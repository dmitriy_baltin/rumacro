<?
session_start();
header('Content-Type: text/html; charset=windows-1251');


include("../config.php");
include("../core/functions.php");
include("../core/database.php");
include("../core/main_functions.php");

$db=new DataBase();


/*
val:3.17
votes:3
product_id:12
score:4 
* 
*/

$ip = GetIp();
$val=stringBeforeDBInputWithStripTags($_REQUEST['val']);
$votes=stringBeforeDBInputWithStripTags($_REQUEST['votes']);
$product_id=stringBeforeDBInputWithStripTags($_REQUEST['product_id']);
$score=stringBeforeDBInputWithStripTags($_REQUEST['score']);

if(!isNum($product_id)){
    echo "ErrProductId";
    exit();
}

$rating_ips_gateway = new rating_ips();
$goods_gateway = new goods();
if($rating_ips_gateway->checkIpForRating($ip,$product_id)){
    
    $good = $goods_gateway->getRecFieldsById($product_id,array("id"));
    if(!$good){
        echo "ErrNoProduct";
        exit();
    }
    $rating_ips_gateway->Add(array("NULL",$product_id,$ip,NowDateTime(),$score));
    $goods_gateway->UpdateFields($product_id,array("rating_votes","rating_val"),array($votes,$val));
    $status="OK";
    $msg="�������. ��� ����� �����";
}
else{
    $status="ERR";
    $msg="�� ��� ���������� �� ���� �����";  
}

echo json_encode(array("status"=>$status,"msg"=>$msg));
?>
