<?php
  $table_gateway=new user_favorites();
  $recs_per_page=20;
  if(isset($_GET['page_num'])&&is_numeric($_GET['page_num'])&&$_GET['page_num']>0)
    $page_num=stringBeforeDBInputWithStripTags($_GET['page_num']);
  else
    $page_num=1;
  list( $goods, $goods_num ) = $table_gateway->getFavorites( getUserID() , $page_num , $recs_per_page );
?>
