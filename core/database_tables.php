<?




class phones extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","code","number","name","otdel"),"phones","id");
$this->setTableRus("������",array("ID","ID ������������","���","�����","���","�����"));

$this->SetAddText("�������� �������");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,20));
$this->AddFieldType(new Text("",STRONG,20));
}

function getPhonesForGroup($ids)
{
if(is_array($ids)&&count($ids)>0)
{
    $ids=array_unique($ids);
    $ids_str=implode(",",$ids);

$sql="
  SELECT 
    *
  FROM 
    `phones`
  WHERE 
    `user_id` IN ({$ids_str})";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
    return $result;
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}
}
else
    return false;

}
}



class texts extends DBTable
{
function __construct()
{
parent::__construct(array("id","label","text"),"texts","id");
$this->setTableRus("������",array("ID","�����","�����"));

$this->NOTAddable();

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,20));
$this->AddFieldType(new RedactorField("",STRONG));
}


function getTextHTML($label)
{
    $rec=$this->getRecFieldsByField("label",$label,array("text"));
    if($rec['text'])
        return $rec['text'];
    else
        return "";
}

}


class prototype_fields_values extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","good_id","prototype_field_id","value"),"prototype_fields_values","id");
        $this->setTableRus("�������� ����� ����������",array("ID","ID ������","ID ���� ���������","�������� ����"));

        $this->SetAddText("�������� ��������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,10));
        $this->AddFieldType(new Text("",STRONG,10));
        $this->AddFieldType(new MultiText("",STRONG,60,4));
    }
     function getGoodsPrototypeFieldsAndValues($good_id)
    {
       $sql="SELECT 
                `user_prototypes_fields`.`name` as `name`,
                `user_prototypes_fields`.`id` as `id`,
                `prototype_fields_values`.`value` as `value`
            FROM 
                `user_prototypes_fields` 
                    LEFT JOIN `prototype_fields_values` ON `prototype_fields_values`.`prototype_field_id`=`user_prototypes_fields`.`id`
            WHERE 
                `prototype_fields_values`.`good_id`={$good_id}
             ORDER BY
                `user_prototypes_fields`.`id` ASC";

            //echo $sql;
            $result=mysql_query($sql);


            if($result)
            {    
            return $result;
            }
            else
            {
                $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
                return false; } 
        
    }
}




class goods_wrong_categories_requests extends DBTable{
    function __construct(){
        parent::__construct(array("id","good_id","datetime"),"goods_wrong_categories_requests","id");
        $this->setTableRus("������ �� ������� ����������� �����",array("ID","ID ������","����/�����"));
        $this->SetAddText("�������� ������");
        $this->AddFieldType(new ID());
        $field = new Text("", STRONG, 10);
        $field->SetOnViewFunction("goods_wrong_categories_requests_good_id");
        $this->AddFieldType($field);
        $this->AddFieldType(new DateTimeField(NowDateTime(), STRONG));
    }
}

function goods_wrong_categories_requests_good_id($value) {
    $goods_gateway = new goods();
    $good = $goods_gateway->getRecFieldsById($value,array("name","id","cat_id3"));
    $result = "<br>�����: <a href='".getGoodUrl($value,$good['name'])."'>".$good['name']."</a>";
    $categories_gateway=new categories();
    $cat_path=$categories_gateway->getCategoryPath($good['cat_id3']);
    for($i=count($cat_path)-1;$i>=0;$i--){
            $print_cat_line.="<a href='".getCatLink($cat_path[$i]['id'],$cat_path[$i]['name'])."' title='".$cat_path[$i]['name']."'>".$cat_path[$i]['name']."</a>";
          if($i)
            $print_cat_line.="  &rarr;  ";        
    }
    $result.="<br><span id='result_".$good['id']."'>������� ���������:<br>".$print_cat_line."</span>";
    EchoPickMacroCategory($good['cat_id3'],$good['id'],"MagCatSelectedInAdmin",true);
    return $result; 
}




class user_categories extends DBTable
{
function __construct()
{
    parent::__construct(array("id","user_id","parent_id","name","disc","prototype_id","manager_id","order"),"user_categories","id");
    $this->setTableRus("������ ������� �������������",array("ID","ID ������������","������������ ������","���","��������","��������","��������","����������"));

    $this->SetAddText("�������� ������");

    $this->AddFieldType(new ID());
    $this->AddFieldType(new UserID("",STRONG));
    $this->AddFieldType(new Text("",STRONG,10));
    $this->AddFieldType(new Text("",STRONG,60));
    $this->AddFieldType(new RedactorField("",STRONG));
    $this->AddFieldType(new Text("",STRONG,10));
    $this->AddFieldType(new Text("",STRONG,10));
    $this->AddFieldType(new Text("",STRONG,10));
}

function GetMainCats($user_id)
{
 $sql="SELECT 
            id,name 
       FROM 
            `".$this->_table_name."` 
       WHERE 
            `parent_id`=0 AND `user_id`={$user_id}
       ORDER BY 
            `order` ASC, `id` ASC";
$result=mysql_query($sql);
if($result)
{
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

}


function GetAllChildCats($user_id)
{
 $sql="SELECT 
            `user_categories`.`id` as 'id',
            `user_categories`.`name` as 'name',
            COUNT(`user_categories`.`id`) as 'count',
            `user_categories`.`parent_id` as 'parent_id'
        FROM 
            `".$this->_table_name."` LEFT JOIN `goods` ON `goods`.`shop_cat_id`=`user_categories`.`id`
        WHERE 
            `user_categories`.`parent_id`<>0 AND `user_categories`.`user_id`={$user_id} AND `goods`.`view`<2 
        GROUP BY
            `user_categories`.`id`
        ORDER BY 
            `user_categories`.`order` ASC, `user_categories`.`id` ASC";
            
$result=mysql_query($sql);
if($result)
{
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

}


function Remove($id)
{
    $goods_gateway=new goods();
    
    $goods=$goods_gateway->getWhereExt("`shop_cat_id`=".$id,array("id"));
    
    
    if(mysql_numrows($goods))
    {
        $remove_cat=$this->getRecFieldsById($id,array("parent_id"));
        $cat_to_set=$remove_cat['parent_id'];    
        $goods_gateway->UpdateGoodsCat($id,$cat_to_set);
    }
    
    $children=$this->GetChildCats($id);
    if($children&&mysql_numrows($children)>0)
    {
        while($child=mysql_fetch_array($children))
        {
            $this->UpdateFields($child['id'],array("parent_id"),array(0));
        }
    }
    
    
    return parent::Remove($id);
}

public $_cats_ids;

function RecurseCatSearch($cur_cat_id)
{
    $children=$this->GetChildCats($cur_cat_id);
    if($children&&mysql_numrows($children)){
        while($child=mysql_fetch_array($children)){
            $this->RecurseCatSearch($child['id']);
        }
    }
    else{
        $this->_cats_ids[]=$cur_cat_id;
    }    
}


function GetChildCats($cat_id)
{
    $sql="SELECT * FROM `".$this->_table_name."` WHERE `parent_id`=".$cat_id." ORDER BY `name` ASC";
    $result=mysql_query($sql);
    if($result){
        return $result;
    }
    else{
        $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
        return false; 
    }
    

}



function getUsersGoodsCategories($user_id,$parent_sign)
    {
$sql="SELECT 
            count(`goods`.`id`) as `num`,
            `user_categories`.`name` as `cat_name`,
            `user_categories`.`id` as `cat_id`,
            `user_categories`.`parent_id` as `parent_id`
FROM 
    `user_categories` 
        LEFT JOIN `goods` ON `goods`.`shop_cat_id`=`user_categories`.`id`
WHERE 
    `user_categories`.`user_id`=".$user_id." AND `user_categories`.`parent_id`{$parent_sign}0
GROUP BY 
    `user_categories`.`id`";

//echo $sql;
$result=mysql_query($sql);


if($result)
{    
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

    
    
    }
    
}



class user_filials extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","user_id","name","city","address","phone"),"user_filials","id");
        $this->setTableRus("�������",array("ID","ID ������������","��������","�����","�����","�������"));

        $this->SetAddText("�������� ������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new UserID("",STRONG));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
    }
}

class user_managers extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","user_id","name","phones","address"),"user_managers","id");
        $this->setTableRus("���������",array("ID","ID ������������","���","�������","�����"));

        $this->SetAddText("�������� ���������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new UserID("",STRONG));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
    }
    
    public static function getManagerInfo($cat_id,$parent_cat_id=0)
    {
        
       /* if($parent_cat_id === false)
        {
            $user_categories_gateway = new user_categories();
            $user_categories_gateway->getRecFieldsById($cat_id,array());  
        }*/
        
        $sql = "
        SELECT
            `user_managers`.*,
            `user_categories`.`id` as 'cat_id'
        FROM
            `user_categories` LEFT JOIN `user_managers` ON `user_categories`.`manager_id` = `user_managers`.`id`  
        WHERE
            `user_categories`.`id` IN (".$cat_id.",".$parent_cat_id.")";
               
        $categories = mysql_query($sql);
        
        if(!$categories)
            return array(false,false,false);
        
        while($category = mysql_fetch_array($categories))
        {
            if($category['cat_id'] == $cat_id)
            {
                $manager_name = $category['name']; 
                $manager_phones = $category['phones'];
                $manager_address = $category['address'];
            }
            
            if($category['cat_id'] == $parent_cat_id && $parent_cat_id)
            {
                $parent_manager_name = $category['name']; 
                $parent_manager_phones = $category['phones'];
                $parent_manager_address = $category['address'];
            }
        }
        
        if($manager_name)
            return array($manager_name, $manager_phones, $manager_address);
        elseif($parent_manager_name)
            return array($parent_manager_name, $parent_manager_phones, $parent_manager_address);
        else
            return array(false,false,false); 
    }
    
    
    
    public static function getManagerPhones($categories)
    {
        $categories_str = implode(", ",$categories);
        
        $sql = "
        SELECT
            `user_managers`.`phones` as 'phone',
            `user_categories`.`id` as 'cat_id'
        FROM
            `user_categories` LEFT JOIN `user_managers` ON `user_categories`.`manager_id` = `user_managers`.`id`  
        WHERE
            `user_categories`.`id` IN (".$categories_str.")";
               
        $categories = mysql_query($sql);
       
        $categories_phones = array();
        if($categories && mysql_num_rows($categories)>0)
        {
            while($category = mysql_fetch_array($categories))
            {
                if($category['phone'])
                    $categories_phones[$category['cat_id']] = $category['phone']; 
            }
        }
        return $categories_phones; 
        
    }
    
    
    
}



class macro_news extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","name","date","on_main","short_text","text"),"macro_news","id");
        $this->setTableRus("������� �����",array("ID","��������","����","�� �������","�������� �����","�����"));

        $this->SetAddText("�������� �������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new DateField(NowDate(),STRONG));
        global $yesnoarray;
        $on_main_field = new ComboArray(0,STRONG,$yesnoarray);
        $on_main_field->SetOnViewFunction("OnViewMacroNewsOnMainFunction");
        $this->AddFieldType($on_main_field);
        
        $this->AddFieldType(new MultiText("",NOT_STRONG,60,3));
        $this->AddFieldType(new RedactorField("",STRONG));
    }
}

class user_prototypes extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","user_id","name"),"user_prototypes","id");
        $this->setTableRus("���������",array("ID","ID ������������","��������"));

        $this->SetAddText("�������� ��������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new UserID("",STRONG));
        $this->AddFieldType(new Text("",STRONG,60));
    }
}

class user_prototypes_fields extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","user_id","prototype_id","name"),"user_prototypes_fields","id");
        $this->setTableRus("���� ����������",array("ID","ID ������������","ID ���������","��������"));

        $this->SetAddText("�������� ���� ���������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new UserID("",STRONG));
        $this->AddFieldType(new Text("",STRONG,10));
        $this->AddFieldType(new Text("",STRONG,60));
    }
    
    function getPrototypeFieldsAndValues($prototype_id,$good_id)
    {
       $sql="SELECT 
                `user_prototypes_fields`.`name` as `name`,
                `user_prototypes_fields`.`id` as `id`,
                `prototype_fields_values`.`value` as `value`
            FROM 
                `user_prototypes_fields` 
                    LEFT JOIN `prototype_fields_values` ON `prototype_fields_values`.`prototype_field_id`=`user_prototypes_fields`.`id`
            WHERE 
                `user_prototypes_fields`.`prototype_id`=".$prototype_id." AND `prototype_fields_values`.`good_id`={$good_id}
            ORDER BY
                `user_prototypes_fields`.`id` ASC
                ";

            //echo $sql;
            $result=mysql_query($sql);
            
            if(!mysql_num_rows($result))
                {
                    return $this->getWhereExt("prototype_id={$prototype_id}",false,"id","ASC");                       
                }


            if($result)
            {    
            return $result;
            }
            else
            {
                $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
                return false; } 
        
    }
}






class user_articles extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","name","photo","date","on_main","anons","text","source_name","source_link"),"user_articles","id");
$this->setTableRus("������",array("ID","ID ������������","��������","����","����","�� �������","�����","�����","�������� ���������","������ �� ��������"));

$this->SetAddText("�������� ������");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/articles/",array(array(300,300),array(100,100)),array("","_small"),30000,true,true));
$this->AddFieldType(new DateField(NowDate(),STRONG));
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));
$this->AddFieldType(new MultiText("",STRONG,60,7));
$this->AddFieldType(new RedactorField("",STRONG));
$this->AddFieldType(new Text("",NOT_STRONG,60));
$this->AddFieldType(new Text("",NOT_STRONG,60));
}



function getArtsForMain($user_id,$num)
{
$sql="
SELECT
    `id`,`name`,`photo`,`date`,`anons`
FROM 
    `".$this->_table_name."` 
WHERE 
    `user_articles`.`on_main`=1 AND `user_articles`.`user_id`={$user_id}
ORDER BY 
    `user_articles`.`id` DESC
LIMIT 0,{$num}";
    
  //echo $sql;
    
$result=mysql_query($sql);


if(!mysql_numrows($result))
{

$sql="
SELECT
    `id`,`name`,`photo`,`date`,`anons`
FROM 
    `".$this->_table_name."` 
WHERE 
    `user_articles`.`user_id`={$user_id}
ORDER BY 
    `user_articles`.`id` DESC
LIMIT 0,{$num}";

$result=mysql_query($sql);
}




if($result)
{
 return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

    
}
}


class user_prices extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","path","name"),"user_prices","id");
$this->setTableRus("������",array("ID","ID ������������","����","��������"));

$this->SetAddText("�������� �����");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new File(NOT_STRONG,30000,"images/prices/",""));
$this->AddFieldType(new Text("",STRONG,60));
}
}


class user_news extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","name","photo","date","on_main","anons","text"),"user_news","id");
$this->setTableRus("�������",array("ID","ID ������������","��������","����","����","�� �������","�����","�����"));

$this->SetAddText("�������� �������");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/articles/",array(array(300,300),array(100,100)),array("","_small"),30000,true,true));
$this->AddFieldType(new DateField(NowDate(),STRONG));
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));
$this->AddFieldType(new MultiText("",STRONG,60,7));
$this->AddFieldType(new RedactorField("",STRONG));
}


function getNewsForMain($user_id,$num)
{
$sql="
SELECT
    `id`,`name`,`photo`,`date`,`anons`
FROM 
    `".$this->_table_name."` 
WHERE 
    `user_news`.`on_main`=1 AND `user_news`.`user_id`={$user_id}
ORDER BY 
    `user_news`.`date` DESC
LIMIT 0,{$num}";
    
  //echo $sql;
    
$result=mysql_query($sql);


if(!mysql_numrows($result))
{

$sql="
SELECT
    `id`,`name`,`photo`,`date`,`anons`
FROM 
    `".$this->_table_name."` 
WHERE 
    `user_news`.`user_id`={$user_id}
ORDER BY 
    `user_news`.`date` DESC
LIMIT 0,{$num}";

$result=mysql_query($sql);
}




if($result)
{
 return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

    
}
}




class spr_company_forms extends Spr
{
    function __construct()
    {
        parent::__construct("spr_company_forms","����� �������������");
    }
}

class spr_company_staff extends Spr
{
    function __construct()
    {
        parent::__construct("spr_company_staff","��������");
    }
}


class spr_company_types extends Spr
{
    function __construct()
    {
        parent::__construct("spr_company_types","���� ��������");
    }
}


class spr_currencies extends Spr
{
    function __construct()
    {
        parent::__construct("spr_currencies","������");
    }
}






class spr_payway_methods_std extends DBTable
{
function __construct()
{
parent::__construct(array("id","name"),"spr_payway_methods_std","id");
$this->setTableRus("���������� ������� ������",array("ID","��������"));

$this->SetAddText("�������� ������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,60));
}
}


class spr_delivery_methods_std extends DBTable
{
function __construct()
{
parent::__construct(array("id","name","is_address"),"spr_delivery_methods_std","id");
$this->setTableRus("���������� ������� ��������",array("ID","��������","���������� �����"));

$this->SetAddText("�������� ������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,60));
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));
}
}


class user_payways extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","name","conditions","active"),"user_payways","id");
$this->setTableRus("���������������� ������� ������",array("ID","ID ������������","��������","������� ������","�������"));

$this->SetAddText("�������� ������ ������");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));

}
}


class user_deliveries extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","name","conditions","is_address","active"),"user_deliveries","id");
$this->setTableRus("���������������� ������� ��������",array("ID","ID ������������","��������","������� ������","���������� �����","�������"));

$this->SetAddText("�������� ������ ��������");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));

}
}


 
class mail_templates extends DBTable
{
function __construct()
{
parent::__construct(array("id","label","text"),"mail_templates","id");
$this->setTableRus("������� ����� ��������",array("ID","�����","�����"));
$this->NOTDeletable();
$this->SetAddText("�������� ������");
        
$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,20));
$this->AddFieldType(new MultiText("",NOT_STRONG,100,20));
    
}
}


class user_orders extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","good_id","num","delivery_id","payway_id","fio","email","phone","city","address","date","comment","status","sender_user_id","view","ip"),"user_orders","id");
$this->setTableRus("������",array("ID","ID ������������","ID ������","�����","��������","������","���","Email","�������","�����","�����","����","�����������","������","ID �����������","�������","IP"));

$this->SetAddText("�������� �����");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG,10));
$this->AddFieldType(new GoodID("",STRONG));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new DateField(NowDate(),STRONG));
$this->AddFieldType(new MultiText("",STRONG,60,7));
global $arOrderStatus;                           
global $yesnoarray;                           
$this->AddFieldType(new ComboArray("0",STRONG,$arOrderStatus));
$this->AddFieldType(new UserID("",STRONG,10));
$this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
$this->AddFieldType(new Text("",STRONG,60));
}


function getNewOrdersNum($user_id)
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `status`=0 AND `user_id`={$user_id} AND `view`=1";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
if(!$res['num'])
return 0;
else
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}


}


function getAllRecsByPage($user_id,$sort_by,$sort_direction,$page_num,$recs_per_page)
{
    $sql="
SELECT 
    SQL_CALC_FOUND_ROWS
    user_orders.*,
    `goods`.`name` as 'good_name',
    `goods`.`price` as 'good_price',
    `spr_currencies`.`name` as 'good_currency',
    `user_payways`.`name` as 'payway_name',
    `user_deliveries`.`name` as 'delivery_name'
FROM 
    `".$this->_table_name."` LEFT JOIN `goods` ON `".$this->_table_name."`.`good_id`=`goods`.`id`
    LEFT JOIN `user_deliveries` ON `".$this->_table_name."`.`delivery_id`=`user_deliveries`.`id`
    LEFT JOIN `user_payways` ON `".$this->_table_name."`.`payway_id`=`user_payways`.`id`
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
WHERE  
    `user_orders`.`user_id`={$user_id} AND `user_orders`.`view`=1";
    
    
    
if($sort_by)
{ $sql.=" ORDER BY `".$sort_by."` ".$sort_direction.", `user_orders`.`id` DESC";}


$sql.=" LIMIT ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


$sql_count="SELECT FOUND_ROWS() as 'num'";

    
 //   echo $sql;


$result=mysql_query($sql);
$result_count=mysql_query($sql_count);

if($result)
{    
$row=mysql_fetch_array($result_count);
return array(0=>$result,1=>$row['num']);
}
else
{
$this->setError(mysql_error());
return false;
}
}



}

 
class notifications extends DBTable
{
    function __construct(){
        parent::__construct(array("id","user_id","date","type","data"),"notifications","id");
        $this->setTableRus("����������� �������������",array("ID","ID ����������","����","���","�������������� ������"));
        $this->NOTAddable();
        $this->NOTEditable();
        $this->NOTDeletable();
        $id_field=new ID();
        $this->AddFieldType($id_field);
        $this->AddFieldType(new UserID("",STRONG));
        $this->AddFieldType(new DateField(NowDate(),STRONG));
        global $arNotificationsTypes;
        $this->AddFieldType(new ComboArray(1,STRONG,$arNotificationsTypes));
        $this->AddFieldType(new Text("",STRONG,60));
    }
} 

class goods_recently_viewed extends DBTable
{
    function __construct()
    {
        parent::__construct(array("id","ip","good_id","datetime"),"goods_recently_viewed","id");
        $this->setTableRus("������������� ������������� ������",array("id","IP","ID ������","����/�����"));
        
        $this->SetAddText("�������� ��������");

        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new GoodID("",STRONG));
        $this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
    }
    
    public function AddData($good_id){
        $this->Add(array("NULL",GetIp(),(int)$good_id, NowDateTime()));
    }
    
    public function getRecentGoods($last){
        
        $ip = getIp();
        $sql="
        SELECT 
            `".$this->_table_name."`.`good_id`,
            `goods`.`name` AS 'good_name',
            `photos`.`path` AS 'good_photo'
        FROM 
            `".$this->_table_name."` 
            INNER JOIN `goods` ON `".$this->_table_name."`.`good_id` = `goods`.`id`  
            LEFT JOIN `photos` ON `goods`.`id`=`photos`.`good_id`
        WHERE 
            `ip` = '{$ip}' AND `photos`.`main`=1
        ORDER BY 
            `".$this->_table_name."`.`id` DESC
        LIMIT 
            0,{$last}";
            
        $result=mysql_query($sql);
        if($result)
        {
            $arr = array();
            while($rec = mysql_fetch_array($result)){
                $rec['good_photo'] = getPic("images/photos/",$rec['good_photo'],2);
                $arr[] = $rec; 
            }
            return $arr;
        }
        else
        {
            $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
            return false;
        }
    }
}

class messages extends DBTable
{
function __construct(){
    parent::__construct(array("id","sender_user_id","reciever_user_id","text","datetime","read","user_view"),"messages","id");
    $this->setTableRus("���������",array("��������","ID ���������� �����������","ID ���������� ����������","�����","�����","��������","������� ��� ������������"));
    $this->SetAddText("�������� ���������");
    global $yesnoarray;
    $id_field=new ID();
    $id_field->SetOnViewFunction("OnViewAdminIDQuery");
    $this->AddFieldType($id_field);
    $this->AddFieldType(new UserID("",STRONG));
    $this->AddFieldType(new UserID("",STRONG));
    $this->AddFieldType(new MultiText("",STRONG,60,7));
    $this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
    $this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
    $this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
}


function setAllRead($user_id)
{
$sql="UPDATE `messages` SET `read`=1 WHERE `reciever_user_id` =".$user_id;

mysql_query($sql);
}

function getUsersNotRead($user_id)
{
$sql="SELECT count(`id`) as 'num' FROM `".$this->_table_name."` WHERE `reciever_user_id` =".$user_id." AND `read`=0 AND `user_view`=1";

$result=mysql_query($sql);
if($result)
{
$rec=mysql_fetch_array($result);
if($rec['num'])
    return $rec['num'];
else
    return 0;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}
}

function getAdminNotRead()
{
$sql="SELECT count(`id`) as 'num' FROM `".$this->_table_name."` WHERE `reciever_user_id` =-1 AND `read`=0";

$result=mysql_query($sql);
if($result)
{
$rec=mysql_fetch_array($result);
if($rec['num'])
    return $rec['num'];
else
    return 0;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}
}


function getMessages($user_id,$page_num,$recs_per_page)
{
   $sql="
 SELECT SQL_CALC_FOUND_ROWS 
*
FROM 
    `messages`
WHERE 
    (`sender_user_id`={$user_id} OR `reciever_user_id`={$user_id}) AND `user_view`=1
ORDER BY 
    `messages`.`id` DESC
LIMIT 
    ".($page_num-1)*$recs_per_page." , ".$recs_per_page;
 


$sql_count="SELECT FOUND_ROWS() as 'num'";


  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);

if($result)
{    
$row=mysql_fetch_array($result_count);
return array(0=>$result,1=>$row['num']);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }



}

function getHistory($user_id,$page_num,$recs_per_page)
{
 $sql="
 SELECT SQL_CALC_FOUND_ROWS 
 *
 FROM 
`messages`
WHERE 
    (`sender_user_id`={$user_id} AND `reciever_user_id`=-1)
    OR 
    (`sender_user_id`=-1 AND `reciever_user_id`={$user_id})   
ORDER BY 
    `messages`.`id` DESC
LIMIT 
    ".($page_num-1)*$recs_per_page." , ".$recs_per_page;
 

 

$sql_count="SELECT FOUND_ROWS() as 'num'";

  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);

if($result)
{    
$row=mysql_fetch_array($result_count);
return array(0=>$result,1=>$row['num']);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }




}





}



class testimonials extends DBTable{
    function __construct(){
        parent::__construct( array("id","hash","name","date","text","mark","owner_comment",
        "email","user_id","product_id","status","verified",
        "param_desc","param_delivery","param_answertime"), "testimonials", "id" );
        $this->setTableRus("������",array("ID","HASH","���","����","�����","������","����������� ���������",
        "Email","ID ���������","ID ������","������","�������������",
        "������ (�������� � ����)","������ (��������)","������ (����� ������)"));
        
        $this->SetAddText("�������� �����");
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new DateField(NowDate(),STRONG));
        $this->AddFieldType(new MultiText("",NOT_STRONG,50,5));
        global $arTestimonialsMarks;
        $this->AddFieldType(new ComboArray(0,STRONG,$arTestimonialsMarks));
        $this->AddFieldType(new MultiText("",NOT_STRONG,50,5));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        global $arTestimonialsStatus;
        $this->AddFieldType(new ComboArray(0,STRONG,$arTestimonialsStatus));
        
        global $yesnoarray;
        $this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
        
        
        
        global $arTestimonialsParamDesc;
        $this->AddFieldType(new ComboArray(0,STRONG,$arTestimonialsParamDesc));
        
        global $arTestimonialsParamDelivery;
        $this->AddFieldType(new ComboArray(0,STRONG,$arTestimonialsParamDelivery));
        
        global $arTestimonialsParamAnswer;
        $this->AddFieldType(new ComboArray(0,STRONG,$arTestimonialsParamAnswer));
    }

function Update($id,$params)
{
    global $portal_email_no_reply;
    global $project_name;
    $i_status=array_search('status',$this->_fields_names);
    $i_user_id=array_search('user_id',$this->_fields_names);
  
  if($params[$i_status]==1){
     $user_id=stringBeforeDBInput($params[$i_user_id]);
     $users_gateway=new users();
     $user=$users_gateway->getRecFieldsById($user_id,array("email","name","id","site_address"));
     $cur_tesimonial = $this->getRecFieldsById($id,array("status"));
     if($user&&$user['email'])
     {
        if($cur_tesimonial['status']==0&&$params[$i_status]==1){
            //�������� ����������� � ����� ������            
            $mail_tamplate=getMailTemplate("new_testimonial",array("%%FIO%%","%%SITE_LINK%%"),array($user['name'],getUserSiteUrl($user['id'],$user['site_address'])));
            $pics=array(PROJECT_URL."/img/macro_logo.jpg");
            MailToMeMagzone(trim($user['email']),'����� ����� � ����� ��������',$mail_tamplate,$portal_email_no_reply,'������ ��������� '.$project_name,$pics);
            
             $res = parent::Update($id,$params);
             if($res){
                $this->updateUserData($id);
             }

          
             return $res;            
        }
        else{
            return parent::Update($id,$params);
        }
     }
     else{
        return parent::Update($id,$params);
     }
     
     
  }
  else{
    return parent::Update($id,$params);    
  }
  
   
}

function getTestimonials($user_id, $page_num, $recs_per_page){
    
     $sql="
     SELECT 
        SQL_CALC_FOUND_ROWS
        ".$this->_table_name.".*,
        `goods`.`name` as 'product_name'
     FROM 
        `".$this->_table_name."`
        LEFT JOIN `goods` ON `".$this->_table_name."`.`product_id`=`goods`.`id`
     WHERE
        ".$this->_table_name.".user_id='".$user_id."' AND ".$this->_table_name.".status='1' AND ".$this->_table_name.".verified = 1
     ORDER BY 
        ".$this->_table_name.".id DESC
     LIMIT 
        ".($page_num-1)*$recs_per_page." , ".$recs_per_page;
      
    $sql_count="SELECT FOUND_ROWS() as 'num'";
     
    $result=mysql_query($sql);
    $result_count=mysql_query($sql_count);

    if($result){    
        $row=mysql_fetch_array($result_count);
        return array(0=>$result,1=>$row['num']);
    }
    else{
        $this->setError(mysql_error());
        return false;
    }    
}

function isMarkPositive($mark){
    if($mark>=TESTIMONIAL_POS_MARK){
        return true;
    }
    return false;
}


function getPosTestimonialPercent($testimonial_num,$positive_testimonial_num){
     return floor((doubleval($positive_testimonial_num)/doubleval($testimonial_num))*100);
}




function verifyTestimonial($testimonial_id){
    $testimonial = $this->getRecFieldsById($testimonial_id,array( "verified" ));
    if($testimonial['verified']==0){
        $this->UpdateFields($testimonial_id,array("verified"),array(1));
        $this->updateUserData($testimonial_id);
    }
}

function updateUserData($testimonial_id) {
    $testimonial = $this->getRecFieldsById($testimonial_id,array( "status", "verified", "user_id", "mark" ));
    if($testimonial['status']==1&&$testimonial['varified']==1){
        $users_gateway = new users();
        $users_gateway->ChangeField($testimonial['user_id'],"testimonials_num",1);
        if($this->isMarkPositive($testimonial['mark'])){
            $users_gateway->ChangeField($testimonial['user_id'],"testimonials_pos_num",1);    
        }    
    }
}

function Add($params){
    global $portal_email_no_reply;
    global $project_name;
    $i_user_id = array_search('user_id', $this->_fields_names);
    $i_mark = array_search('mark', $this->_fields_names);
    $i_verified = array_search('verified', $this->_fields_names);
    $i_email = array_search('email', $this->_fields_names);
    $i_hash = array_search('hash', $this->_fields_names);
    
    $user_id = stringBeforeDBInput($params[$i_user_id]);
    $mark = stringBeforeDBInput($params[$i_mark]);
    
    
    
    if($params[$i_verified]==0){
        //�������� ����������� � ����������� ������ ����� �����
        $mail_tamplate=getDirectMailTemplate("testimonial_verify",
                                            array(
                                                    "%%VERIFY_LINK%%",
                                                    "%%PROJECT_URL%%",
                                                     "%%PROJECT_NAME%%"),
                                            array(          
                                                getTestimonialVerifyLink( $params[$i_hash], true ),          
                                                PROJECT_URL,
                                                $project_name)
                                            );
        $res = MailToMeMagzone(
                        trim($params[$i_email]), '������������� ������', $mail_tamplate,
                        $portal_email_no_reply, $project_name, array()
                        );
    }
    
    parent::Add($params);
    $added_id = mysql_insert_id();
    $this->updateUserData($added_id);
    return $added_id; 
}

function Remove($id){
    $testimonial = $this->getRecFieldsById($id, array("user_id","mark","status","verified"));
    if($testimonial['status']==1&&$testimonial['varified']==1){
        $user_id = $testimonial['user_id'];
        $mark = $testimonial['mark'];
        $users_gateway = new users();
        $users_gateway->ChangeField($user_id,"testimonials_num",-1);
        if($this->isMarkPositive($mark)){
            $users_gateway->ChangeField($user_id,"testimonials_pos_num",-1);    
        }
    }
    return parent::Remove($id);
} 
 
    
    
    function getNotCheckedCount()
{
     $sql="
SELECT
    count(`id`) as 'num'
FROM 
    `testimonials`
WHERE
    `testimonials`.`status`=0 AND `testimonials`.`verified`=1";

    
  
$result=mysql_query($sql);

if($result)
{    
$row=mysql_fetch_array($result);
if($row['num'])
        return $row['num'];
    else
        return 0;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
   
}

    
}



class rating_ips extends DBTable{
    function __construct(){
        parent::__construct( array("id","product_id","ip","datetime","score"), "rating_ips", "id" );
        $this->setTableRus("IP �����������",array("ID","�����","IP","����/�����","������"));
        $this->SetAddText("�������� �����");
        
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new Text("",STRONG,60));
        $this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
        $this->AddFieldType(new Text("",STRONG,60));
    }
    
    function checkIpForRating($ip,$product_id){
        $recs = $this->getWhereExt("`product_id`='{$product_id}' AND `ip`='{$ip}'",array("id"));
        if(mysql_numrows($recs)>0){
            return false;
        }
        return true;
    }
}



class user_callbacks extends DBTable
{
function __construct(){
    parent::__construct(array("id","user_id","item_id","name","phone","datetime","new"),"user_callbacks","id");
    $this->setTableRus("�������� ������ �������������",array("ID","ID ������������","�����","���","�������","���� � �����","�����"));

    $this->SetAddText("�������� �������� �����");

    $this->AddFieldType(new ID());
    $this->AddFieldType(new Text("",STRONG,30));
    $this->AddFieldType(new Text("",STRONG,30));
    $this->AddFieldType(new Text("",STRONG,60));
    $this->AddFieldType(new Text("",STRONG,60));
    $this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
    $this->AddFieldType(new Text("",STRONG,60));
    
    }
    
    function nullNewCallbacks($user_id){
        $this->UpdateFieldsByCond("`new` = 1 AND `user_id`='{$user_id}'",array("new"),array(0));
    }
    
    function getNewNum($user_id)
    {
        $sql="SELECT count(`id`) as 'num' FROM `".$this->_table_name."` WHERE `user_id` =".$user_id." AND `new` = 1";

        $result=mysql_query($sql);
        if($result)
        {
        $rec=mysql_fetch_array($result);
        if($rec['num'])
            return $rec['num'];
        else
            return 0;
        }
        else
        {
            $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
            return false;
        }
    }
}




class goods extends DBTable
{
function __construct()
{
parent::__construct(array("id","name","discription","price","currency_id","on_vetrina","cat_id1","cat_id2","cat_id3","shop_cat_id","user_id","good_added_date","checked","view","avaliable","sale_type","discount","rating_votes","rating_val"),"goods","id");
$this->setTableRus("������",array("ID","��������","������ ��������","����","������","�� �������","��������� 1�� ��-��","��������� 2�� ��-��","��������� 3�� ��-��","��������� � ��������","ID ������������","���� ����������","���������","��������/����������������","������� �� ������","����� ������","������","����� �������","������� ������"));

$this->SetAddText("�������� �����");
global $yesnoarray;
$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new RedactorField("",STRONG));

$price_field=new Text("",STRONG,10);
$price_field->SetOnViewFunction("OnViewPrice");
$this->AddFieldType($price_field);

$this->AddFieldType(new ComboTable("1",STRONG,"spr_currencies","id","name"));
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));

$cat1_field=new Text("",STRONG,10);
$cat1_field->SetOnViewFunction("OnViewCat");
$this->AddFieldType($cat1_field);

$cat2_field=new Text("",STRONG,10);
$cat2_field->SetOnViewFunction("OnViewCat");
$this->AddFieldType($cat2_field);

$cat3_field=new Text("",STRONG,10);
$cat3_field->SetOnViewFunction("OnViewCat");
$this->AddFieldType($cat3_field);


$cat4_field=new Text("",STRONG,10);
$cat4_field->SetOnViewFunction("OnViewSiteCat");
$this->AddFieldType($cat4_field);



$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
global $arUserGoodStatus;
$this->AddFieldType(new ComboArray(0,STRONG,$arUserGoodStatus));


global $arGoodView;
global $arGoodAvaliable;
global $arGoodSaleType;

$this->AddFieldType(new ComboArray(1,STRONG,$arGoodView));
$this->AddFieldType(new ComboArray(0,STRONG,$arGoodAvaliable));
$this->AddFieldType(new ComboArray(0,STRONG,$arGoodSaleType));

$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));

}

function getModerGoodsCount($checked)
{
     $sql="
SELECT
    count(`goods`.`id`) as 'num'
FROM 
    `goods` INNER JOIN `users` ON `goods`.`user_id`=`users`.`id`
WHERE
    `users`.`base_info_set`=1 AND `goods`.`checked`={$checked}";

    
  
$result=mysql_query($sql);

if($result)
{    
$row=mysql_fetch_array($result);
if($row['num'])
        return $row['num'];
    else
        return 0;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
   
}




function getModerGoods($checked,$page_num,$recs_per_page)
{

    
        
 
 $sql="SELECT SQL_CALC_FOUND_ROWS
`goods`.`id` as 'id',
`users`.`id` as 'user_id',
`users`.`site_address` as 'site_address',
`goods`.`name` as 'name'
FROM 
    `goods` INNER JOIN `users` ON `goods`.`user_id`=`users`.`id`
WHERE
    `users`.`base_info_set`=1 AND `goods`.`checked`={$checked}
ORDER BY 
    `goods`.`id` DESC
LIMIT 
    ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


  //echo $sql;


  $sql_count="SELECT FOUND_ROWS() as 'num'";
  
  
  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);


if($result)
{    
$row=mysql_fetch_array($result_count);
if($row['num'])
        $num=$row['num'];
    else
        $num=0;

return array(0=>$result,1=>$num);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }


 




}

function getGoodsPeriodNumber2($start,$end)
{
$sql="SELECT `".$this->_table_name."`.`id` as 'id',`".$this->_table_name."`.`name` as 'name', `users`.`site_address` as 'site_address', `users`.`id` as 'user_id', `users`.`name` as 'user_name'  FROM `".$this->_table_name."` INNER JOIN `users` ON 
`".$this->_table_name."`.`user_id`=`users`.`id`
WHERE `good_added_date` BETWEEN '".$start."' AND '".$end."'";

//echo $sql;

$result=mysql_query($sql);
if($result)
{

return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

}



function getGoodsPeriodNumber($start,$end)
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `good_added_date` BETWEEN '".$start."' AND '".$end."'";
$result=mysql_query($sql);

if($result)
{
$res=mysql_fetch_array($result);
return $res['num'];
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

}




function getGoodsForSite($user_id,$cat_id,$search,$sort_cond,$page_num,$recs_per_page)
{
    $search=trim(mysql_real_escape_string(substr(strip_tags($search),0,200)));
    
    if($search)
        $search_cond=" AND `goods`.`name` LIKE '%{$search}%'";
        
    
    if($cat_id&&is_numeric($cat_id)&&$cat_id>0)
        {
            $cat_id=trim(mysql_real_escape_string(substr(strip_tags($cat_id),0,200)));
            
            $user_categories_gateway=new user_categories();
            $child_cats=$user_categories_gateway->GetChildCats($cat_id);
            
            $cond_cats_arr=array();
            $cond_cats_arr[]=$cat_id;
            if(mysql_num_rows($child_cats))
                while($child_cat=mysql_fetch_array($child_cats))
                    $cond_cats_arr[]=$child_cat['id'];
            
    
            if(count($cond_cats_arr))
            {
                $cond_cats_str=implode(", ",$cond_cats_arr);
                $cat_cond=" AND `goods`.`shop_cat_id` IN ({$cond_cats_str})";
            }
        }
        
        
    
    
 $sql="SELECT  SQL_CALC_FOUND_ROWS
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `goods`.`price` AS 'good_price',
    `goods`.`discount` AS 'good_discount',
    `spr_currencies`.`name` as 'currency'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
WHERE 
    `goods`.`user_id`={$user_id} AND `goods`.`view`=1 AND `goods`.`checked`=1 {$search_cond} {$cat_cond}
ORDER BY 
    {$sort_cond}
LIMIT 
    ".($page_num-1)*$recs_per_page.", ".$recs_per_page;


  //echo $sql;


  $sql_count="SELECT FOUND_ROWS() as 'num'";
  
  
  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);


if($result)
{    
$row=mysql_fetch_array($result_count);
if($row['num'])
        $num=$row['num'];
    else
        $num=0;

return array(0=>$result,1=>$num);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
    
    

}


function getGoodForOrder($good_id)
{
$sql="
SELECT 
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `goods`.`discount` AS 'good_discount',
    `goods`.`price` AS 'good_price',
    `goods`.`user_id` AS 'good_user_id',
    `users`.`name` AS 'good_user_name',
    `users`.`site_address` AS 'good_user_site_address',
    `users`.`shop_name` AS 'good_user_shop_name',
    `spr_currencies`.`name` as 'currency',
    `users`.`delivery_regions` AS 'delivery_regions',
    `users`.`delivery_dop_info` AS 'delivery_dop_info'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
    LEFT JOIN `users` ON `goods`.`user_id`=`users`.`id`
WHERE 
    `goods`.`id`={$good_id}";
    
    
//echo $sql;

    
$result=mysql_query($sql);
if($result)
{
    $row=mysql_fetch_array($result);
    if($row)
        return $row;
    else
        return false;

}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}


function getGoodsForVitrina($user_id)
{
$sql="
SELECT 
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `goods`.`price` AS 'good_price',
    `goods`.`discount` AS 'good_discount',
    `spr_currencies`.`name` as 'currency',
    `photos`.`path` AS 'good_photo'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `photos` ON `goods`.`id`=`photos`.`good_id`
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
WHERE 
    `photos`.`main`=1 AND `goods`.`on_vetrina`=1 AND `goods`.`user_id`={$user_id} AND `goods`.`view`=1 AND `goods`.`checked`=1
ORDER BY 
    `goods`.`id` DESC";
    
  //echo $sql;
    
$result=mysql_query($sql);


if(!mysql_numrows($result))
{

$sql="
SELECT 
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `goods`.`price` AS 'good_price',
    `spr_currencies`.`name` as 'currency',
    `photos`.`path` AS 'good_photo'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `photos` ON `goods`.`id`=`photos`.`good_id`
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
WHERE 
    `photos`.`main`=1 AND `goods`.`user_id`={$user_id}
ORDER BY 
    `goods`.`id` DESC
LIMIT 
    0,12";

$result=mysql_query($sql);
}




if($result)
{
 return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

    
}

function getGoodsInUserCat($user_id,$shop_cat_id,$num)
{

    if($shop_cat_id)
        $cond.="AND `shop_cat_id`={$shop_cat_id}";
        
    
    
    
       $sql="
SELECT 
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `photos`.`path` AS 'good_photo'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `photos` ON `goods`.`id`=`photos`.`good_id`
WHERE 
    `photos`.`main`=1 AND `goods`.`checked`=1 AND `goods`.`user_id`={$user_id} {$cond}
ORDER BY 
    `goods`.`id` DESC
LIMIT
    0,{$num}";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
{
 return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

    
}


function getGoodsInCat($cat_level,$cat_id,$num)
{
       
       $sql="
SELECT 
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `photos`.`path` AS 'good_photo'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `photos` ON `goods`.`id`=`photos`.`good_id`
WHERE 
    `photos`.`main`=1 AND `goods`.`checked`=1 AND `goods`.`cat_id{$cat_level}`={$cat_id}
ORDER BY 
    `goods`.`id` DESC
LIMIT
    0,{$num}";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
{
 return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

    
}


function getGoodInfo($good_id)
{

$sql="SELECT 
`goods`.*,
`users`.`name` as 'user_name',
`users`.`site_address` as 'user_site_address',
`users`.`site_address` as 'user_site_address',
`users`.`person` as 'user_person',
`users`.`address` as 'user_address',
`users`.`shop_name` as 'user_shop_name',
`users`.`skype` as 'user_skype',
`users`.`delivery_regions`,
`users`.`delivery_dop_info`,
`users`.`budni_start_h`,
`users`.`budni_start_m`,
`users`.`budni_end_h`,
`users`.`user_logo`,
`users`.`budni_end_m`,
`users`.`budni_pere_start_h`,
`users`.`profile_level`,
`users`.`budni_pere_start_m`,
`users`.`budni_pere_end_h`,
`users`.`budni_pere_end_m`,
`users`.`has_pere`,
`users`.`vocation_saturday`,
`users`.`testimonials_num`,
`users`.`testimonials_pos_num`,
`users`.`vocation_sunday`,
`user_categories`.`name` as `cat_name`,
`user_categories`.`id` as 'cat_id',
`user_categories`.`manager_id` as 'manager_id',
`user_categories`.`parent_id` as 'cat_parent_id',
`cities`.`name` as 'city_name',
`regions`.`name` as 'region_name',
`spr_currencies`.`name` as `currency`
FROM 
`goods` 
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
    LEFT JOIN `user_categories` ON `goods`.`shop_cat_id`=`user_categories`.`id`
    LEFT JOIN `users` ON `goods`.`user_id`=`users`.`id`
    LEFT JOIN `cities` ON `users`.`city_id`=`cities`.`id`
    LEFT JOIN `regions` ON `users`.`region_id`=`regions`.`id`
WHERE
    `goods`.`id`={$good_id}";
    
 // echo $sql;
    
$result=mysql_query($sql);


if($result)
{    
$row=mysql_fetch_array($result);
if($row)
        return $row;
    else
        return false;
}
else
{   
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}


 




}


function Add($params)
{


  $i_user_id=array_search('user_id',$this->_fields_names);  
  if($params[$i_user_id])
  {
    $users_gateway=new users();
    $users_gateway->ChangeField($params[$i_user_id],"goods_num",1);
  }
    
return parent::Add($params);
}



function Remove($id)
{
$photos_gateway=new photos();
$photos=$photos_gateway->getWhere("good_id",$id);
while($photo=mysql_fetch_array($photos))
    $photos_gateway->Remove($photo['id']);
    
$good=$this->getRecFieldsById($id,array("user_id"));
$users_gateway=new users();
$users_gateway->ChangeField($good['user_id'],"goods_num",-1);


$prototype_fields_values_gateway=new prototype_fields_values();
$prototype_fields_values_gateway->RemoveWhere("good_id","=",$id);
    
    
return parent::Remove($id);
}


function UpdateGoodsCat($old_cat,$new_cat)
{
 $sql="UPDATE `goods` SET `shop_cat_id`={$new_cat} WHERE `shop_cat_id`={$old_cat}";

$result=mysql_query($sql);


if($result)
{    
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}


function getUsersGoodsInfo($user_id,$cat_id,$name,$good_status,$good_avaliable,$good_sale_type,$page_num,$recs_per_page)
{
$condition="WHERE `goods`.`user_id`={$user_id}";
if($cat_id) 
{
    $user_categories_gateway=new user_categories();
    $user_categories_gateway->RecurseCatSearch($cat_id);
    $cats_str=implode(",", $user_categories_gateway->_cats_ids);  
    $condition.=" AND `goods`.`shop_cat_id` IN ({$cats_str})";
}

if($name)
    $condition.=" AND `goods`.`name` LIKE '%".$name."%'";
    
if($good_status!=100)
    $condition.=" AND `goods`.`view` = '{$good_status}'";
    
if($good_avaliable!=100)
    $condition.=" AND `goods`.`avaliable` = '{$good_avaliable}'";

if($good_sale_type!=100)
    $condition.=" AND `goods`.`sale_type` = '{$good_sale_type}'";

    

        
 
 $sql="SELECT SQL_CALC_FOUND_ROWS
`goods`.`id` as 'id',
`user_categories`.`name` as `cat_name`,
`user_categories`.`id` as 'cat_id',
`user_categories`.`parent_id` as 'cat_parent_id',
`goods`.`view` as `view`,
`goods`.`name` as `name`,
`goods`.`discount` as `discount`,
`goods`.`checked` as `checked`,
`goods`.`price` as `price`,
`spr_currencies`.`name` as `currency`
FROM 
`goods` 
    LEFT JOIN `user_categories` ON `goods`.`shop_cat_id`=`user_categories`.`id`
    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
    {$condition}
ORDER BY 
    `goods`.`shop_cat_id` ASC, `goods`.`id` DESC
LIMIT 
    ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


 // echo $sql;


  $sql_count="SELECT FOUND_ROWS() as 'num'";
  
  
  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);


if($result)
{    
$row=mysql_fetch_array($result_count);
if($row['num'])
        $num=$row['num'];
    else
        $num=0;

return array(0=>$result,1=>$num);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }


 




}





function getRegionCompaniesInfo($cat_id,$cat_level,$name,$page_num,$recs_per_page,$cond_city_id,$cond_region_id)
{
$add_condition="";
if($cat_id)   
    $cat_condition="`goods`.`cat_id{$cat_level}`=".$cat_id;

    
$area_condition="";
if($cond_city_id)
    $area_condition="`users`.`city_id`='".$cond_city_id."'";


if($cond_region_id)
    $area_condition="`users`.`region_id`='".$cond_region_id."'";

$condition="`goods`.`checked`=1 AND `users`.`checked`=1 ";//"AND `users`.`profile_level`>".PROFILE_LEVEL_ZOMBIE;

if($name)
    {$condition.=" AND (`users`.`name` LIKE '%".$name."%' OR `users`.`shop_name` LIKE '%".$name."%')";
    $cat_condition="";}



if($area_condition)
    {
        if($condition)
        $condition.=" AND ";
        $condition.=$area_condition;
    }

if($cat_condition)
    {
       if($condition)
        $condition.=" AND ";

       $condition.=$cat_condition; 
    }

    
    
     if($condition)
        $condition="WHERE ".$condition;


 
 $sql="
SELECT SQL_CALC_FOUND_ROWS
    `users`.`id` as `user_id`,
    `users`.`site_address` as `site_address`,
    `users`.`name` as `user_name`,
    `users`.`skype` as `skype`,
    `users`.`user_logo` as `user_logo`,
    `users`.`company` as `company`,
    `users`.`verification` as `verification`,
    `users`.`profile_level` as 'profile_level',
    `cities`.`name` as 'city_name'
FROM 
`users` 
    INNER JOIN `goods` ON `goods`.`user_id`=`users`.`id`
    LEFT JOIN `cities` ON `users`.`city_id`=`cities`.`id`
    {$condition}
GROUP BY 
    `users`.`id`
ORDER BY 
    `users`.`profile_level` DESC
LIMIT 
    ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


    

$sql_count="SELECT FOUND_ROWS() as 'num'";
  
  
  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);


if($result)
{    
$row=mysql_fetch_array($result_count);
if($row['num'])
        $num=$row['num'];
    else
        $num=0;

return array(0=>$result,1=>$num);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}


function getGoodsIdsByName($name){
    require_once(getDocumentRoot() . 'sphinx/api/sphinxapi.php');
    $sphinx = new SphinxClient();
    $serverok=$sphinx->SetServer('localhost', 9312); //9306 9312
    // ���������� �� ������ �����
    //$sphinx->SetMatchMode(SPH_MATCH_ANY); //SPH_MATCH_ANY

    // ���������� ����������� �� �������������
    //$sphinx->SetSortMode(SPH_SORT_RELEVANCE);
    $sphinx->SetMatchMode(SPH_MATCH_EXTENDED2);
    $sphinx->setRankingMode(SPH_RANK_SPH04);
    //SPH_SORT_ATTR_DESC SPH_SORT_EXPR

    $sphinx->setLimits(0,10000);

    // ������ ����� ���� (��� �������� �������������)
    // $sphinx->SetFieldWeights(array ('title' => 20, 'description' => 10));
    // $sphinx->setFilterRange("amount", 1,20);
    //$sphinx->setFilter('@id',array(5));
    $name = preg_replace("/\s{1,}/", " ", $name);
    
    $query_arr = explode(" ", $name);
    $query_array = array();
    foreach($query_arr as $query_piece){
        $query_array[] = "({$query_piece} | *{$query_piece}* | ={$query_piece})";
    }
    $query_str = implode(" & ", $query_array);
    

    // ��������� �� ������� (* - ������������� ���� ��������)
    $result = $sphinx->Query($query_str, SPHINX_INDEX);
    if ($result===false) echo "��������� ������: ".$sphinx->GetLastError();
    $id_list="";
    if ($result && isset($result['matches'])) {
        $ids = array_keys($result['matches']);
        $id_list = implode(',', $ids); 
    }
    //echo $id_list;
    return $id_list;
}


function getRegionGoodsInfo($cat_id,$cat_level,$name,$company,$sort_by,$sort_direction,$page_num,$recs_per_page,$cond_city_id,$cond_region_id)
{
$add_condition="";
if($cat_id && $cat_id!=-1)   
    $cat_condition="`goods`.`cat_id{$cat_level}`=".$cat_id;

    
$area_condition="";
if($cond_city_id)
    $area_condition="`users`.`city_id`='".$cond_city_id."'";


if($cond_region_id)
    $area_condition="`users`.`region_id`='".$cond_region_id."'";


if($name){
    //$search_condition.="(`goods`.`name` LIKE '%".$name."%' OR `categories`.`name` LIKE '%".$name."%')";

    // $search_condition.="(MATCH (`goods`.`name`) AGAINST ('\"{$name}\"' IN BOOLEAN MODE) 
    // OR MATCH (`categories`.`name`) AGAINST ('\"{$name}\"' IN BOOLEAN MODE))";

    if($ids_list=$this->getGoodsIdsByName(iconv('cp1251', 'utf-8', $name))){
        $search_condition.='`goods`.`id` IN ('.$ids_list.')';
    }else  return array(0=>"",1=>0);
}


$condition="`goods`.`checked`=1 AND `goods`.`view`=1 ";//"AND `users`.`profile_level`>".PROFILE_LEVEL_ZOMBIE;

if($area_condition)
    {
        if($condition)
        $condition.=" AND ";
        $condition.=$area_condition;
    }

if($cat_condition)
    {
       if($condition)
        $condition.=" AND ";

       $condition.=$cat_condition; 
    }

if($search_condition)
    {
       if($condition)
            $condition.=" AND ";
        
        $condition.=$search_condition;
    }
    
    
     if($condition)
        $condition="WHERE ".$condition;


if($sort_by!="`price`" && $name)        
    $order_by="ORDER BY FIELD(`goods`.`id`,".$ids_list.")";
else
    $order_by="ORDER BY ".$sort_by." ".$sort_direction.", `users`.`profile_level` DESC";
 
 $sql="SELECT SQL_CALC_FOUND_ROWS
`goods`.`id` as 'id',
`goods`.`user_id` as `user_id`,
`users`.`site_address` as `site_address`,
`users`.`name` as `user_name`,
`users`.`skype` as `skype`,
`users`.`company` as `company`,
`users`.`testimonials_num` as `testimonials_num`,
`users`.`testimonials_pos_num` as `testimonials_pos_num`,
`users`.`verification` as `verification`,
`categories`.`name` as `cat_name`,
`goods`.`cat_id3` as 'cat_id',
`goods`.`name` as `name`,
`goods`.`discount` as `discount`,
`goods`.`shop_cat_id` as `shop_cat_id`,
`user_categories`.`parent_id` as `shop_cat_parent_id`,
`goods`.`price` as `price`,
`spr_currencies`.`name` as `currency`,
`cities`.`name` as 'city_name'
FROM 
`goods` 
    INNER JOIN `users` ON `goods`.`user_id`=`users`.`id`
    LEFT JOIN `cities` ON `users`.`city_id`=`cities`.`id`
    LEFT JOIN `user_categories` ON `goods`.`shop_cat_id`=`user_categories`.`id`
    LEFT JOIN `categories` ON `goods`.`cat_id3`=`categories`.`id`
    INNER JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
    {$condition}
    {$order_by}
LIMIT ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


//echo $sql;


$sql_count="SELECT FOUND_ROWS() as 'num'";
  
  
  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);


if($result)
{    
$row=mysql_fetch_array($result_count);
if($row['num'])
        $num=$row['num'];
    else
        $num=0;

return array(0=>$result,1=>$num);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}



function getUserGoodsNum($user_id)
{
    
$sql="SELECT 
        COUNT(`id`) as 'num'
      FROM
        `goods`
       WHERE
        `user_id`={$user_id}"; 
  
  
  
$result=mysql_query($sql);


if($result)
{    
$row=mysql_fetch_array($result);
if($row['num'])
        return $row['num'];
    else
        return 0;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }


 
}



function getNewGoods($num)
{
   $sql="
SELECT 
    `goods`.`id` AS 'good_id',
    `goods`.`name` AS 'good_name',
    `photos`.`path` AS 'good_photo'
FROM 
    `".$this->_table_name."` INNER JOIN `photos` ON `goods`.`id`=`photos`.`good_id`
WHERE 
    `photos`.`main`=1 AND `goods`.`checked` = 1
ORDER BY 
    `goods`.`id` DESC
LIMIT
    0,{$num}";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
{
 return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}
}


class photos_in_text extends DBTable
{
function __construct()
{
    parent::__construct(array("id","path"),"photos_in_text","id");
    $this->setTableRus("���� ��� ������",array("ID","�����������"));    
    $this->SetAddText("�������� ����");

    $this->AddFieldType(new ID());
    //$photo=new PhotoExt(NOT_STRONG,30,"../photos_in_text/",array(array(1000,1000),array(200,200)),array("","_small"),30000,true);
    $photo=new PhotoExt(NOT_STRONG,30,"images/photos_in_text/",array(array(1000,1000),array(200,200)),array("","_small"),30000,true,true,true);
    $photo->SetOnViewFunction("OnViewPhotosInText");
    $this->AddFieldType($photo);
}

}

function OnViewPhotosInText($value){
    $ar=explode(";",$value);
    if($ar[0]!=""&&$ar[0]!="NOT_SET"){
            $res="";
            $ar1=explode(".",$ar[0]);
            if(file_exists(getDocumentRoot()."images/photos_in_text/{$ar1[0]}_small.{$ar1[1]}"))
            {
             $res.="<a href='/images/photos_in_text/{$ar1[0]}.{$ar1[1]}'><img src='/images/photos_in_text/{$ar1[0]}_small.{$ar1[1]}'></a><br>";
            }
             
            $res.="C����� �� �������� 200x200: ".PROJECT_URL."/images/photos_in_text/{$ar1[0]}_small.{$ar1[1]}<br>";
            $res.="C����� �� ��������-��������: ".PROJECT_URL."/images/photos_in_text/{$ar1[0]}.{$ar1[1]}<br>";   
            return $res;
        }
        else
        {return "";}

}


class photos_temp extends DBTable
{
function __construct()
{
parent::__construct(array("id","path","key","viewed"),"photos_temp","id");
$this->setTableRus("���� ������� (���������)",array("ID","�����������","����","����������"));

$this->SetAddText("�������� ����");

$this->AddFieldType(new ID());
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/photos/",array(array(700,700),array(250,250),array(100,100),array(40,40)),array("","_medium","_small","_smallextra"),30000,true,true,true));
$this->AddFieldType(new Text("",STRONG,30));
global $yesnoarray;
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
}


function getNotViewedByKey($key)
{
     $sql="
  SELECT 
    * 
  FROM 
    `photos_temp`
  WHERE 
    `key`='{$key}' AND `viewed`=0";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
    return $result;
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}

}



class photos extends DBTable
{
function __construct()
{
parent::__construct(array("id","good_id","path","main"),"photos","id");
$this->setTableRus("���� �������",array("ID","ID ������","�����������","�������"));

$this->SetAddText("�������� ����");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,30));
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/photos/",array(array(700,700),array(250,250),array(100,100),array(40,40)),array("","_medium","_small","_smallextra"),30000,true,true,true));
global $yesnoarray;
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
}


function UpdateMainPicForGood($main_pic_in_loader,$good_id)
{

$sql="UPDATE `photos` SET `main`=0 WHERE `good_id`={$good_id}";
mysql_query($sql);
$this->UpdateFields($main_pic_in_loader,array("main"),array(1));

}



function getPhotosForGood($good_id)
{
$good_id=mysql_real_escape_string($good_id);

$sql="
  SELECT 
    * 
  FROM 
    `photos`
  WHERE 
    `good_id`={$good_id}
  ORDER BY
    `main` DESC";
    
$result=mysql_query($sql);
if($result)
    return $result;
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}


function getMainPhotosForGroup($ids)
{
if(is_array($ids)&&count($ids)>0)
{
    $ids_str=implode(",",$ids);

$sql="
  SELECT 
    *
  FROM 
    `photos`
  WHERE 
    `good_id` IN ({$ids_str}) AND `main`=1";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
    return $result;
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}
}
else
    return false;

}

}




class params extends DBTable
{
function __construct()
{
parent::__construct(array("id","premoder_users","premoder_goods","company_name","company_address","rs","edrpoy","mfo","bankname","account_templ",
"admin_login","admin_pass","main_h1","main_text","main_title","main_discription","main_keywords"),"params","id");
$this->setTableRus("���������",array("ID","������������ �������������","������������ ����������","�������� ��������","����� ��������","�/�","������","���","�������� �����","������ (����������) ��� ��. ���","����� ������� � �����. ������","������ ������� � �����. ������","H1 �� �������","����� �� �������","������� TITLE","������� DISCRIPTION","������� KEYWORDS"));

$this->NOTAddable();

$this->AddFieldType(new ID());
global $yesnoarray;         
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new MultiText("",STRONG,60,7));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",STRONG,60));
$this->AddFieldType(new Text("",NOT_STRONG,80));
$this->AddFieldType(new RedactorField("",NOT_STRONG));
$this->AddFieldType(new Text("",STRONG,80));
$this->AddFieldType(new MultiText("",STRONG,60,7));
$this->AddFieldType(new MultiText("",STRONG,60,7));

}
}



class verification extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","path","date","completed"),"verification","id");
$this->setTableRus("�����������",array("ID","ID ������������","����","����","������������"));


$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",NOT_STRONG));
$field=new FileExt(NOT_STRONG,20,"images/verified/",array(1000),array(""),2000);

$this->AddFieldType($field);
$this->AddFieldType(new DateField(NowDate(),STRONG));

global $yesnoarray;
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));
}


function getNew()
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `completed`=0";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
if(!$res['num'])
return 0;
else
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}


function Update($id,$params)
{
  $i_completed_id=array_search('completed',$this->_fields_names);
  $i_user_id=array_search('user_id',$this->_fields_names);
  
  if($params[$i_completed_id]==1)
  {
     $user_id=stringBeforeDBInput($params[$i_user_id]);
     $users_gateway=new users();
     $user=$users_gateway->getRecFieldsById($user_id,array("verification"));
     if($user)
     {
        if($user['verification']==0)
        {
            $users_gateway->UpdateFields($user_id,array("verification"),array(1));
            
        }//else{echo "333";}
     }//else{echo "222";}
  
  
  }
  //else{echo "111";}
    
    
return parent::Update($id,$params);
}



}





class photos_folder extends DBTable
{
function __construct()
{
parent::__construct(array("id","label","cur_folder"),"photos_folder","id");
$this->setTableRus("������",array("ID","�����","������� �����"));

$this->SetAddText("��������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,30));
$this->AddFieldType(new Text("",STRONG,30));
}
}



class adminmessages extends DBTable
{
function __construct()
{
parent::__construct(array("id","text","theme_id","user_id","datetime","done"),"adminmessages","id");

$this->setTableRus("������� � �������������",array("ID","�����","����","ID ������������","����/�����","���������"));

$this->SetAddText("�������� ������ � �������������");

$id_field=new ID();
$id_field->SetOnViewFunction("OnViewAdminIDQuery");
$this->AddFieldType($id_field);
//$text_field=new MultiText("",STRONG,40,5);
//$text_field->SetOnViewFunction("OnViewAdminTextQuery");
$this->AddFieldType(new MultiText("",STRONG,40,5));
$this->AddFieldType(new ComboTable(1,STRONG,"admmessages_themes","id","theme"));

$user_field=new Text("",NOT_STRONG,20);
$user_field->SetOnViewFunction("OnViewAdminUserIdQuery");
$this->AddFieldType($user_field);

$this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
global $yesnoarray;
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));

}

function getNotDoneRequests()
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `done`=0";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}

function getUsersQueries($user_id,$page_num,$recs_per_page)
{
$sql="SELECT * FROM `".$this->_table_name."` WHERE `user_id` =".$user_id." ORDER BY `datetime` DESC LIMIT ".($page_num-1)*$recs_per_page." , ".$recs_per_page;

$result=mysql_query($sql);
if($result)
{
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}
}


function Add($params)
{

    global $portal_email; 
    global $project_name; 
    global $project_url;  
    
$text="������������,<br>
  ����� ������ �� {$project_name}<br>";
          $text.="<a href='".$project_url."/mac_a/index.php?page=showtabExt&table=adminmessages' target='_blank'>� ������ ��������</a><br>";
          $text.="� ���������,<br>";
          $text.="������ {$project_name}<br>";
          
          
        
  MailToMeMagzone($portal_email,'����� ������ �� ������� '.$project_name, $text, $portal_email,$project_name,array());  
    
    
    
return parent::Add($params);
}


         function getAllRecsByPageExt($sort_by,$sort_direction,$page_num,$recs_per_page,$searchFields)//,$s_field,$s_string,$s_field_sign=0
{
    $sql="SELECT SQL_CALC_FOUND_ROWS * FROM `".$this->_table_name."`";

    $where_cond=$this->getSearchCond($searchFields);
    
    if($this->_filter)
            {
                if($where_cond)
                {$where_cond.=" AND {$this->_filter}";}
                else
                {$where_cond=" WHERE {$this->_filter}";}
            }
                
    
$sql.=$where_cond;
    
if($sort_by)
{ $sql.=" ORDER BY `".$sort_by."` ".$sort_direction.", `id` DESC ";}


$sql.=" LIMIT ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


$sql_count="SELECT FOUND_ROWS() as 'num'";

    
    //echo $sql;


$result=mysql_query($sql);
$result_count=mysql_query($sql_count);

if($result)
{    
$row=mysql_fetch_array($result_count);
return array(0=>$result,1=>$row['num']);
}
else
{
$this->setError(mysql_error());
return false;
}

}




}


class cities extends DBTable
{
function __construct()
{
parent::__construct(array("id","region_id","name","on_main"),"cities","id");
$this->setTableRus("������",array("ID","������","��������","�� �������"));

$this->SetAddText("�������� �����");

$this->AddFieldType(new ID());
$this->AddFieldType(new ComboTable("1",STRONG,"regions","id","name"));
$this->AddFieldType(new Text("",STRONG,30));
global $yesnoarray;
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));

}
function getCityInfo($city_id)
{
  $sql="
  SELECT 
    `cities`.`name` AS 'city_name',
    `regions`.`name` AS 'reg_name',
    `regions`.`id` AS 'reg_id'
  FROM 
    `".$this->_table_name."` LEFT JOIN `regions` ON `cities`.`region_id`=`regions`.`id`
  WHERE 
    `cities`.`id`=".$city_id;
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
{
    if(mysql_numrows($result)>0)
        return mysql_fetch_array($result);
    else
        return false;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}


function getCityInfoByName($city_name)
{
  $sql="
  SELECT 
    `cities`.`id` AS 'city_id',
    `regions`.`id` AS 'reg_id',
  FROM 
    `".$this->_table_name."` LEFT JOIN `regions` ON `cities`.`region_id`=`regions`.`id`
  WHERE 
    `cities`.`name`='".$city_name."'";
    
  //echo $sql;
    
$result=mysql_query($sql);
if($result)
{
    return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}

}


class themes extends DBTable
{
function __construct()
{
parent::__construct(array("id","name"),"themes","id");
$this->setTableRus("�������",array("ID","��������"));

$this->SetAddText("�������� ������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,30));
}
}

//id    name        sort    on_main
class regions extends DBTable
{
function __construct()
{
parent::__construct(array("id","name","sort","on_main"),"regions","id");
$this->setTableRus("�������",array("ID","��������","����������","�� �������"));

$this->SetAddText("�������� ������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,30));
$this->AddFieldType(new Text("",STRONG,10));
global $yesnoarray;
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));

}

function getRegionInfo($region_id)
{
  $sql="
  SELECT 
    `regions`.`name` AS 'reg_name'
  FROM 
    `".$this->_table_name."`
  WHERE 
    `regions`.`id`=".$region_id;
    
$result=mysql_query($sql);
if($result)
{
    if(mysql_numrows($result)>0)
        return mysql_fetch_array($result);
    else
        return false;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}
}






class money_transactions extends DBTable
{
function __construct()
{
parent::__construct(array("id","bill_id","sum","datetime","comment","payway_id"),"money_transactions","id");
$this->setTableRus("�������� ����������",array("ID","ID �����","�����","����/�����","�����������","������ ������"));

$this->SetAddText("�������� ����������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new DateTimeField(NowDateTime(),STRONG));
$this->AddFieldType(new MultiText("",NOT_STRONG,60,5));
$this->AddFieldType(new ComboTable("1",STRONG,"payways_type","id","name"));
}

function getSumForPeriod($start,$end,$payway=false)
{
    if($payway)
        $payway_cond=" AND `payway_id`={$payway}";
    
    
$sql="SELECT SUM(`sum`) AS 'sum' FROM `".$this->_table_name."` WHERE `datetime` BETWEEN '".$start."' AND '".$end."' {$payway_cond}";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
if($res['sum']=="")
return 0;
else
return $res['sum'];
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false;
}

}



function Add($params)
{
$i_bill_id=array_search('bill_id',$this->_fields_names);
$i_sum=array_search('sum',$this->_fields_names);
$bill_id=strip_tags($params[$i_bill_id]);
$sum=strip_tags($params[$i_sum]);

    if($bill_id>0&&is_numeric($bill_id))
    {
        if($sum>0&&is_numeric($sum))
        {
        
        
        $money_bills_gateway=new money_bills();
        $bill=$money_bills_gateway->getRecFieldsById($bill_id,array("sum","paid_status"));
        
        if($bill)
        {
            if($bill['paid_status']==0)
            {
                if($bill['sum']==$sum)
                {
                  $money_bills_gateway->UpdateFields($bill_id,array("paid_status"),array(1));//�������
                  return parent::Add($params);
                }
                else
                {
                    $this->setError("����� ���������� � ������������� ����� �� ���������. ��������� ������������ ��������� ����.");
                    return false;
                }
            }
            else 
            {
                $this->setError("������ ���� ��� ��� �������.");
                return false;
            }  
            
        }
        else
        {
            $this->setError("����, ������� �� ������� �� ������. ��������� ������������ �������� ID �����");
            return false;
        }
        
        }
        else
        {
            $this->setError("�������� ������ ����� ����������");
            return false;
        }   
        
    }
    else
    {
        $this->setError("�������� ������ �������������� �����");
        return false;
    }
}

}






class money_bills_paid extends money_bills
{
function __construct()
{
    parent::__construct();
    $this->SetFilter("`money_bills`.`paid_status`=1");
    $this->_table_rus_name="���������� �����";
}
}


class money_bills_not_paid extends money_bills
{
function __construct()
{
    parent::__construct();
    $this->SetFilter("`money_bills`.`paid_status`=0");
    $this->_table_rus_name="������������ �����";
}
}


class money_bills extends DBTable
{
function __construct()
{
parent::__construct(array("id","uid","user_id","sum","transactions_type","data_id","comment","payer","payway_id","paid_status","datetime",
"fio","phone","email","company_name","ur_address","contact_person"),"money_bills","id");
$this->setTableRus("������������ �����",array("ID","UID","ID ������������*","�����*","��� ������*","ID ������ ��� ID ���. ��������*",
"����������� (���� ��� ������-������)*","����������*","������ ������*","������ �����","����/�����",
"���(�)","�������(�/�)","����������� �����(�/�)","������������ �����������(�)","����������� �����(�)","���������� ����(�)"));



$this->AddFieldType(new ID()); 

$uid_field=new Text(uniqid(),NOT_STRONG,20);
$uid_field->SetOnViewFunction("OnViewUid");
$this->AddFieldType($uid_field);

$this->AddFieldType(new UserID("",STRONG)); 
$this->AddFieldType(new Text("",STRONG,10));

$this->AddFieldType(new ComboTable("1",STRONG,"transactions_type","id","name"));
$this->AddFieldType(new Text("",NOT_STRONG,10));//��� ID ��������� �������� ���� ��� ���� �� ��������� �������� ��� ID ������ ����� ���� ��� ����� �����
$this->AddFieldType(new MultiText("",NOT_STRONG,60,3));
global $bill_payer;
$this->AddFieldType(new ComboArray(1,STRONG,$bill_payer));

$this->AddFieldType(new ComboTable("1",STRONG,"payways_type","id","name"));

global $bill_paid_status;
$this->AddFieldType(new ComboArray(0,STRONG,$bill_paid_status));

$this->AddFieldType(new DateTimeField(NowDateTime(),STRONG)); 

$this->AddFieldType(new Text("",NOT_STRONG,40));
$this->AddFieldType(new Text("",NOT_STRONG,40));
$this->AddFieldType(new Text("",NOT_STRONG,40));
$this->AddFieldType(new Text("",NOT_STRONG,40));
$this->AddFieldType(new Text("",NOT_STRONG,40));
$this->AddFieldType(new Text("",NOT_STRONG,40));
}


function Add($params)
{
    
    return parent::Add($params);
}



function getBillsForContext($context_id)
{
    $sql="
SELECT 
    `uid`,`payway_id`
FROM 
    `".$this->_table_name."`
WHERE 
    `".$this->_table_name."`.`transactions_type`=5 AND `".$this->_table_name."`.`data_id`={$context_id}
ORDER BY 
    `".$this->_table_name."`.`id` DESC
LIMIT 
    0,1";
    
$result=mysql_query($sql);
if($result)
{
    if(mysql_numrows($result))
        return mysql_fetch_array($result);
    else
        return false;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}

function getLastUserBill($user_id,$payer)
{
     $sql="
SELECT 
    *
FROM 
    `".$this->_table_name."`
WHERE 
    `".$this->_table_name."`.`user_id`={$user_id} AND `".$this->_table_name."`.`payer`={$payer}
ORDER BY 
    `".$this->_table_name."`.`id` DESC
LIMIT 
    0,1";
    
    
    
$result=mysql_query($sql);
if($result)
{
    if(mysql_numrows($result))
        return mysql_fetch_array($result);
    else
        return false;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}

}



class context extends DBTable
{
function __construct()
{
parent::__construct(array("id","user_id","date_create","goods","regions","sum","sum_to_account","search_engine","date_paid","date_start","date_end","cur_status_id","comment"),"context","id");
$this->setTableRus("��������� �������� (��������)",array("ID","ID ������������","���� ��������","������ ��� ������ �������","������� �������","����� � ������",
"����� ����������","��������� �������","���� ������",
"���� ������ ��������","���� ����� ��������","������� ������ ��������","�����������"));    
$this->SetAddText("�������� ��������");

$this->AddFieldType(new ID());
$this->AddFieldType(new UserID("",STRONG));
$this->AddFieldType(new DateField(NowDate(),NOT_STRONG));
$this->AddFieldType(new MultiText("",STRONG,60,10));
$this->AddFieldType(new MultiText("",STRONG,60,10));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
global $search_engines_arr;
$this->AddFieldType(new ComboArray(0,STRONG,$search_engines_arr));
$this->AddFieldType(new DateField("0000-00-00",NOT_STRONG));
$this->AddFieldType(new DateField("0000-00-00",NOT_STRONG));
$this->AddFieldType(new DateField("0000-00-00",NOT_STRONG));

$this->AddFieldType(new ComboTable("1",STRONG,"context_status","id","name"));
$this->AddFieldType(new MultiText("",NOT_STRONG,60,10));


}


function getUsersContext($user_id)
{
 $sql="
SELECT 
    `".$this->_table_name."`.*,
    `context_status`.`name` as 'status'
FROM 
    `".$this->_table_name."`  LEFT JOIN `context_status` ON `".$this->_table_name."`.`cur_status_id`=`context_status`.`id` 
WHERE 
    `".$this->_table_name."`.`user_id`={$user_id}
ORDER BY 
    `cur_status_id` ASC, `".$this->_table_name."`.`id` DESC";
    
$result=mysql_query($sql);
if($result)
{
    return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}

}


class context_not_paid extends context
{
function __construct()
{
    parent::__construct();
    $this->SetFilter("`context`.`cur_status_id`=1");
    $this->_table_rus_name="��������� ��������, ������ ������";
}
}

class context_paid_not_active extends context
{
function __construct()
{
    parent::__construct();
    $this->SetFilter("`context`.`cur_status_id`=4");
    $this->_table_rus_name="������ ��������� �������� (��������)";
}
}


class context_active extends context
{
function __construct()
{
    parent::__construct();
    $this->SetFilter("`context`.`cur_status_id`=2");
    $this->_table_rus_name="�������� ��������� ��������";
}
}

class context_ended extends context
{
function __construct()
{
    parent::__construct();
    $this->SetFilter("`context`.`cur_status_id`=3");
    $this->_table_rus_name="����������� ��������� ��������";
}
}


class payways_type extends Spr
{
  function __construct()
{
    parent::__construct("payways_type","������� ������");
}

}



class transactions_type extends Spr
{
  function __construct()
{
    parent::__construct("transactions_type","���� �����");
}

}


class context_status extends Spr
{
  function __construct()
{
    parent::__construct("context_status","������� ��������� ��������");
}

}



class members_features extends DBTable
{
function __construct()
{
parent::__construct(array("id","member_name","price","price_no_adv","price_adv_google","price_adv_yandex","adv_goods","goods_num","order"),"members_features","id");
$this->setTableRus("������",array("ID","�������� �����a","���� (���.)","���� (��� ���������)","������ �������� Google (���.)","������ �������� Yandex (���.)","����� ������������� �������","����� �������","������� ������"));    
$this->SetAddText("�������� �����");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,40));
}
}




class admin_groups extends DBTable{
    function __construct(){
        parent::__construct(array(
         "id",
         "name",
         "admins",
         "admin_groups",
         "categories",
         "cities",
         "context",
         "context_status",
         "goods",
         "goods_recently_viewed",
         "params",
         "members_features",
         "money_transactions",
         "money_bills",
         "buy_member",
         "money_bills_not_paid",
         "money_bills_paid",
         "transactions_type",
         "payways_type",
         "goods_wrong_categories_requests",
         "all_users_mess_form",
         "advice",
         "messages",
         "verification",
         "macro_news",
         "premoderusers",
         "premodergoods",
         "context_paid_not_active",
         "context_not_paid",
         "context_active",
         "context_ended",
         "users",
         "phones",
         "user_filials",
         "users_entries",
         "user_articles",
         "user_categories",
         "user_deliveries",
         "user_news",
         "user_orders",
         "user_payways",
         "user_prices",
         "mail_templates",
         "texts",
         "spr_currencies",
         "spr_company_forms",
         "spr_company_staff",
         "spr_company_types",
         "spr_delivery_methods_std",
         "spr_payway_methods_std",
         "rassilka",
         "regions",
         "themes","testimonials","stat"),"admin_groups","id");
        $this->setTableRus("������ �������",array(
          "ID",
         "��������",
         "��������������",
         "������ ���������������",
         "��������� ������� � �����",
         "�������",
         "��������",
         "������� ����������� �������",
         "������",
         "������� ������������� ������",
         "����� ���������",
         "������ �����",
         "������ �����",
         "�����",
         "��������� ������",
         "������������ �����",
         "���������� �����",
         "���� ����� (���.)",
         "������� ������ (���.)",
         "������� � �������� ����������",
         "�������� �������������",
         "��������� ������������",
         "���������",
         "�����������",
         "����� �������",
         "��������� �������������",
         "��������� �������",
         "��������: ����� (����������)",
         "��������: �� ����������",
         "��������: ��������",
         "��������: �����������",
         "������������",
         "��������",
         "�������",
         "�����������",
         "������",
         "������ �������",
         "������� ��������",
         "�������",
         "������",
         "������� ������",
         "������",
         "������� ����� ��������",
         "������",
         "����������: ������",
         "����������: ����� �������������",
         "����������: ������ �����",
         "����������: ���� ��������",
         "����������: ���������� ������ ��������",
         "����������: ���������� ������ ������",
         "������� ��������",
         "�������",
         "������� ������","������","����������"));    
        $this->SetAddText("�������� ������");

        global $yesnoarray;
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,40));                           
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
        $this->AddFieldType(new ComboArray("1",STRONG,$yesnoarray));
    }
    public static function hasRights ( $slug, $user_admin_rights_obj ) {
        $sinonims = array(
            "photos_in_text"=>"macro_news"  
        );
        if(isset($sinonims[$slug])){
            $slug = $sinonims[$slug]; 
        }
        if(!isset($user_admin_rights_obj)){
            return false;
        }
        if(isset($user_admin_rights_obj[$slug])&&$user_admin_rights_obj[$slug]==1){
            return true;
        }
        return false;
    }
}


class user_messages extends DBTable{
    function __construct() {        
        parent::__construct(array("id","user_id","subject","name","email","phone","text","datetime","datetime_read","new","product_id"),"user_messages","id");
        $this->setTableRus("������",array("ID","ID ������������","����","���","Email","�������","�����","����/�����","����/����� ���������","�����","�����"));    
        $this->SetAddText("�������� ���������");
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new MultiText("",STRONG,40,4));
        $this->AddFieldType(new DateTimeField("",STRONG));
        $this->AddFieldType(new DateTimeField("",STRONG));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
    }
    
    function getUsersNotRead($user_id)
    {
        $sql="SELECT count(`id`) as 'num' FROM `".$this->_table_name."` WHERE `user_id` =".$user_id." AND `new`=1";

        $result=mysql_query($sql);
        if($result)
        {
        $rec=mysql_fetch_array($result);
        if($rec['num'])
            return $rec['num'];
        else
            return 0;
        }
        else
        {
            $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
            return false;
        }
    }
}

class user_messages_replies extends DBTable{
    function __construct() {        
        parent::__construct(array("id","message_id","name","email","text","datetime"),"user_messages_replies","id");
        $this->setTableRus("������",array("ID","ID ���������","���","Email","�����","����/�����"));    
        $this->SetAddText("�������� ���������");
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new MultiText("",STRONG,40,4));
        $this->AddFieldType(new DateTimeField("",STRONG));
    }
}



class user_favorites extends DBTable{
    function __construct() {        
        parent::__construct(array("id","user_id","product_id"),"user_favorites","id");
        $this->setTableRus("������",array("ID","������������","������"));    
        $this->SetAddText("�������� ����� � ���������");
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
    }
    
    public function isInFavorites($user_id, $product_id){
        $recs = $this->getWhereExt("user_id='{$user_id}' AND product_id='{$product_id}'",array("id"));
        if(mysql_num_rows($recs)){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function getFavorites($user_id, $page_num, $recs_per_page) {
        
        $sql="SELECT SQL_CALC_FOUND_ROWS
                `".$this->_table_name."`.`id` as 'data_id', 
                `goods`.`id` as 'id',
                `goods`.`name` as `name`,
                `goods`.`discount` as `discount`,
                `goods`.`price` as `price`,
                `spr_currencies`.`name` as `currency` 
              FROM `".$this->_table_name."` LEFT JOIN `goods`
                    ON `".$this->_table_name."`.`product_id` = `goods`.`id`
                    LEFT JOIN `spr_currencies` ON `goods`.`currency_id`=`spr_currencies`.`id`
              WHERE
                    `".$this->_table_name."`.`user_id` = '{$user_id}'
              ORDER BY
                    `".$this->_table_name."`.`id` DESC
              LIMIT 
                    ".($page_num-1) * $recs_per_page . " , " . $recs_per_page;
                    
        $sql_count="SELECT FOUND_ROWS() as 'num'";

        $result=mysql_query($sql);
        $result_count=mysql_query($sql_count);

        if($result){    
            $row=mysql_fetch_array($result_count);
            return array(0=>$result,1=>$row['num']);
        }
        else{
            $this->setError(mysql_error());
            return false;
        }

    } 
}        
        
        
class admins extends DBTable{
    function __construct() {        
        parent::__construct(array("id","name","pass","group"),"admins","id");
        $this->setTableRus("������",array("ID","���","������","������"));    
        $this->SetAddText("�������� ������");
        $this->AddFieldType(new ID());
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new Text("",STRONG,40));
        $this->AddFieldType(new ComboTable("1",STRONG,"admin_groups","id","name"));
    }
    function Login($login,$pass){
        $sql="SELECT * FROM `admins` WHERE `name`='".$login."' AND `pass`='".$pass."'";
        $result=mysql_query($sql);
        if($result){
            if(mysql_numrows($result)>0){
                return mysql_fetch_array($result);
            }
            else{
                return false;
            }
            return $result;
        }
        else{
            $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
            return false; 
        }
    }
}


class users extends DBTable
{
function __construct()
{
parent::__construct(array("id","name","person","address","email","skype","icq","pass",
"city_id","region_id","reg_date","ban_date","discription","verification","company",
"website","year_founded","company_type","company_form","company_staff","profile_level",
"paid_to","req_name","req_egrpoy","req_phone","req_address","req_account",
"req_bankname","req_mfo","services_give","services_take","pic_shapka","pic_fon",
"theme_id","map","user_logo","shop_name","keywords","menu_main","menu_catalog","menu_about",
"menu_contacts", "menu_pay_delivery","show_menu_pay_delivery","vitrina_name",
"delivery_regions","delivery_dop_info","budni_start_h","budni_start_m","budni_end_h",
"budni_end_m","budni_pere_start_h","budni_pere_start_m","budni_pere_end_h","budni_pere_end_m",
"has_pere","vocation_saturday","vocation_sunday","site_address","goods_num","base_info_set",
"checked","stat_code","testimonials_num","testimonials_pos_num"),"users","id");
$this->setTableRus("������������",array("ID","�������� (���)","���������� ����","�����","Email","skype","icq","������","�����","������","���� �����������","���� ����","��������","�����������","��������/������� ����","����","��� ���������","��� ��������","�������������� �������� �����","����� ���������","������� �������","������� ��","���������: ��������","���������: ������","���������: �������","���������: ��. �����","���������: ����","���������: �������� �����","���������: ���","������, ������� ������������","������ ������� ����������","������: �����","������: ���","������: ���� ����������","�� �����","�������","�������� ��������","�������� �����","����: ������� ����","����: ������ � ������","����: � ���","����: ��������","����: ������ � ��������","����: ���������� �������� ������ � ��������","��������� �������","������� ��������","��������: ���. ����","��: ����� ������ ����","��: ����� ������ ���","��: ����� ����� ����","��: ����� ����� ���","��: ����� ���. ������ ����","��: ����� ���. ������ ���","��: ����� ���. ����� ����","��: ����� ���. ����� ���","���� �������?","������� ��������?","����������� ��������?","����� �����","����� �������","�������� ���������� ���������","������������ ��������","��� ����������","����� �������","����� ���������� �������"));    
$this->SetAddText("�������� ������������");

$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,40));//name
$this->AddFieldType(new Text("",NOT_STRONG,40));//person
$this->AddFieldType(new Text("",NOT_STRONG,40));//address
$this->AddFieldType(new Text("",STRONG,40));//email
$this->AddFieldType(new Text("",NOT_STRONG,40));//skype
$this->AddFieldType(new Text("",NOT_STRONG,40));//icq
$this->AddFieldType(new Text("",STRONG,40));//pass
$this->AddFieldType(new ComboTable("1",STRONG,"cities","id","name"));//city_id
$this->AddFieldType(new ComboTable("1",STRONG,"regions","id","name"));//region_id
$this->AddFieldType(new DateField(NowDate(),STRONG));//reg_date
$this->AddFieldType(new DateField("0000-00-00",STRONG));//ban_date
$this->AddFieldType(new RedactorField("",STRONG));//discription
global $yesnoarray;                           
global $arUserType;
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));//verification
$this->AddFieldType(new ComboArray("1",STRONG,$arUserType));//company
$this->AddFieldType(new Text("",NOT_STRONG,40));//website
$this->AddFieldType(new Text("",NOT_STRONG,10));//year_founded                   
$this->AddFieldType(new ComboTable("1",STRONG,"spr_company_types","id","name"));//company_type
$this->AddFieldType(new ComboTable("1",STRONG,"spr_company_forms","id","name"));//company_form
$this->AddFieldType(new ComboTable("1",STRONG,"spr_company_staff","id","name"));//company_staff
$this->AddFieldType(new ComboTable("1",STRONG,"members_features","id","member_name"));//profile_level
$this->AddFieldType(new DateField(NowDateDay(30),STRONG));//paid_date
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_name
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_egrpoy
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_phone
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_address
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_account
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_bankname
$this->AddFieldType(new Text("",NOT_STRONG,40));//req_mfo
$this->AddFieldType(new MultiText("",NOT_STRONG,50,5));//services_give
$this->AddFieldType(new MultiText("",NOT_STRONG,50,5));//services_take
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/shop/",array(array(1000,300)),array(""),30000,false,true));//pic_shapka
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/shop/",array(array(2000,2000)),array(""),30000,false,true));//pic_fon
$this->AddFieldType(new ComboTable("1",STRONG,"themes","id","name"));//theme_id
$this->AddFieldType(new MultiText("",STRONG,60,7));//map
$this->AddFieldType(new PhotoExt(NOT_STRONG,30,"images/logos/",array(array(200,200),array(100,100)),array("","_small"),30000,false,true));//user_logo
$this->AddFieldType(new Text("",NOT_STRONG,40));//shop_name
$this->AddFieldType(new MultiText("",STRONG,60,7));//keywords
$this->AddFieldType(new Text("",NOT_STRONG,40));//menu_main
$this->AddFieldType(new Text("",NOT_STRONG,40));//menu_catalog
$this->AddFieldType(new Text("",NOT_STRONG,40));//menu_about    
$this->AddFieldType(new Text("",NOT_STRONG,40));//menu_contacts    
$this->AddFieldType(new Text("",NOT_STRONG,40));//menu_pay_delivery   
$this->AddFieldType(new ComboArray(1,STRONG,$yesnoarray));//show_menu_pay_delivery
$this->AddFieldType(new Text("",NOT_STRONG,40));//vitrina_name
$this->AddFieldType(new MultiText("",STRONG,60,3));//delivery_regions
$this->AddFieldType(new MultiText("",STRONG,60,3));//delivery_dop_info

global $grHours;
global $grMins;
global $arUserGoodStatus;
$this->AddFieldType(new ComboArray("0",STRONG,$grHours));//budni_start_h
$this->AddFieldType(new ComboArray("0",STRONG,$grMins));//budni_start_m
$this->AddFieldType(new ComboArray("0",STRONG,$grHours));//budni_end_h
$this->AddFieldType(new ComboArray("0",STRONG,$grMins));//budni_end_m
$this->AddFieldType(new ComboArray("0",STRONG,$grHours));//budni_pere_start_h
$this->AddFieldType(new ComboArray("0",STRONG,$grMins));//budni_pere_start_m
$this->AddFieldType(new ComboArray("0",STRONG,$grHours));//budni_pere_end_h
$this->AddFieldType(new ComboArray("0",STRONG,$grMins));//budni_pere_end_m
$this->AddFieldType(new ComboArray("0",STRONG,$yesnoarray));//has_pere
$this->AddFieldType(new ComboArray(1,STRONG,$yesnoarray));//vocation_saturday
$this->AddFieldType(new ComboArray(1,STRONG,$yesnoarray));//vocation_sunday
$this->AddFieldType(new Text("",NOT_STRONG,40));//site_address
$this->AddFieldType(new Text("",NOT_STRONG,10));//goods_num
$this->AddFieldType(new ComboArray(0,STRONG,$yesnoarray));//base_info_set
$this->AddFieldType(new ComboArray(0,STRONG,$arUserGoodStatus));//checked
$this->AddFieldType(new MultiText("",NOT_STRONG,50,5));//stat_code
$this->AddFieldType(new Text("",NOT_STRONG,40));//testimonials_num
$this->AddFieldType(new Text("",NOT_STRONG,40));//testimonials_pos_num
}


function getUsersForConfirmNotification()
{
    $notify_type=NOTIFICATION_TYPES_CONFIRM_ITEMS;
    global $need_to_confirm_items_time;
    $date_need_to_confirm_items_time=DateDay(NowDate(),-1*$need_to_confirm_items_time);
     $sql="
SELECT
    `users`.`id` as 'user_id',
    `users`.`email` as 'email',
    `users`.`name` as 'name',
    MIN(`notifications`.`date`) as 'last_time_notify'
FROM 
    `users` LEFT JOIN `notifications` ON `users`.`id`=`notifications`.`user_id`
WHERE
    `notifications`.`type`={$notify_type} OR `notifications`.`type` IS NULL
GROUP BY
    `users`.`id`
HAVING
    MAX(`notifications`.`date`)<'{$date_need_to_confirm_items_time}' OR MAX(`notifications`.`date`) IS NULL";

   //echo $sql; 
  
$result=mysql_query($sql);

if($result)
{    
    return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
}

function getModerUsersCount($checked)
{
     $sql="SELECT
count(`id`) as 'num'
FROM 
    `users` 
WHERE
    `base_info_set`=1 AND `checked`={$checked}";

    
  
$result=mysql_query($sql);

if($result)
{    
$row=mysql_fetch_array($result);
if($row['num'])
        return $row['num'];
    else
        return 0;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }
   
}

function getModerUsers($checked,$page_num,$recs_per_page)
{

    
        
 
 $sql="SELECT SQL_CALC_FOUND_ROWS
`users`.`id` as 'id',
`users`.`site_address` as 'site_address',
`users`.`name` as 'name',
`users`.`shop_name` as 'shop_name'
FROM 
    `users` 
WHERE
    `base_info_set`=1 AND `checked`={$checked}
ORDER BY 
    `users`.`id` DESC
LIMIT 
    ".($page_num-1)*$recs_per_page." , ".$recs_per_page;


 // echo $sql;


  $sql_count="SELECT FOUND_ROWS() as 'num'";
  
  
  
$result=mysql_query($sql);
$result_count=mysql_query($sql_count);


if($result)
{    
$row=mysql_fetch_array($result_count);
if($row['num'])
        $num=$row['num'];
    else
        $num=0;

return array(0=>$result,1=>$num);
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }


 




}



function getRegisteredUsersNumberBeforeDate($date)
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `reg_date`<='".$date."'";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}


function getCompanyNumber()
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `company`=1";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}


function getRegisteredUsersNumber($start,$end)
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `reg_date` BETWEEN '".$start."' AND '".$end."'";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}


function getInfo($user_id)
{
    
    $sql="
SELECT 
    users.*,
    `spr_company_types`.`name` as 'company_type',
    `spr_company_forms`.`name` as 'company_form',
    `cities`.`name` as 'city_name',
    `regions`.`name` as 'region_name',
    `spr_company_staff`.`name` as 'company_staff'
FROM 
    `".$this->_table_name."` 
    LEFT JOIN `spr_company_types` ON `users`.`company_type`=`spr_company_types`.`id`
    LEFT JOIN `spr_company_forms` ON `users`.`company_form`=`spr_company_forms`.`id`
    LEFT JOIN `spr_company_staff` ON `users`.`company_staff`=`spr_company_staff`.`id`
    LEFT JOIN `cities` ON `users`.`city_id`=`cities`.`id`
    LEFT JOIN `regions` ON `users`.`region_id`=`regions`.`id`
WHERE
    `users`.`id`='{$user_id}'";

  //echo $sql;

$result=mysql_query($sql);
if($result)
   { 
       if(mysql_num_rows($result))
        return mysql_fetch_array($result);
       else
        return false;
   }
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}



}

function getRecommendedUsers($num)
{
$sql="
SELECT 
    `user_logo`,
    `id` as 'user_id',
    `site_address`
FROM 
    `".$this->_table_name."`
ORDER BY 
    `goods_num` DESC
LIMIT
    0,{$num}";


$result=mysql_query($sql);
if($result)
    return $result;
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}

}


function getNewUsers($num)
{
$sql="
SELECT 
    `user_logo`,
    `id` as 'user_id',
    `site_address`,
    `discription` as 'disc',
    `name`,
    `shop_name`
FROM 
    `".$this->_table_name."`
WHERE
    `user_logo`<>'NOT_SET'  AND `goods_num`>10 AND `discription`<>''
ORDER BY 
    `id` DESC
LIMIT
    0,{$num}";


$result=mysql_query($sql);
if($result)
    return $result;
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}

}



function Login($login, $pass)
{
    $login=trim(stringBeforeDBInputWithStripTags($login));
    if(strlen($login)==0)
    {return false;}
    
    $user=$this->getRecFieldsByField("email",$login,array("id","email","name","site_address","pass","ban_date"));
    if(!$user)
        return false;    
    
    
    if($user['pass']==$pass)
    {    
    $ar=explode("-",$user['ban_date']);
    
    $now = time();
    $ban=mktime(0, 0, 0, $ar[1], $ar[2], $ar[0]);
    
    if($now-$ban>=0)
    {return array('login'=>$user['email'],'id'=>$user['id'],'name'=>$user['name'],'site_address'=>$user['site_address']);}
    else
    {return array('login'=>FormatDateForView($user['ban_date']),'id'=>"ban");}

    }
    else
    {
    return false;
    }
}


}



class users_entries extends DBTable
{
function __construct()
{
parent::__construct(array("id","datetime","user_id"),"users_entries","id");
$this->setTableRus("����� �������������",array("ID","����/�����","ID ������������"));
$this->NOTAddable();

$this->AddFieldType(new ID()); 
$this->AddFieldType(new DateTimeField(NowDateTime(),STRONG)); 
$this->AddFieldType(new UserID("",STRONG));
}


function getLoginsPeriodNumber($start,$end)
{
$sql="SELECT `users`.`site_address` as 'site_address', `users`.`name` as 'name', `user_id`, COUNT(`".$this->_table_name."`.`id`) AS 'num' FROM `".$this->_table_name."` INNER JOIN `users` ON `".$this->_table_name."`.`user_id`=`users`.`id` WHERE `datetime` BETWEEN '".$start."' AND '".$end."' GROUP BY `user_id`";
//echo $sql;

$result=mysql_query($sql);
if($result)
{

return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

}



function getActiveUsersPers($start,$end)
{

$sql="SELECT `user_id`, COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `datetime` BETWEEN '".$start."' AND '".$end."' GROUP BY `user_id`";
$result=mysql_query($sql);
if($result)
{
$users_num_logged_in_period=mysql_numrows($result);


$users_gateway=new users();
$all_users_reg_before_date=$users_gateway->getRegisteredUsersNumberBeforeDate($end);

return array(($users_num_logged_in_period/$all_users_reg_before_date)*100,$users_num_logged_in_period);


}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}




function getUsersEntriesNumber($start,$end)
{
$sql="SELECT COUNT(`id`) AS 'num' FROM `".$this->_table_name."` WHERE `datetime` BETWEEN '".$start."' AND '".$end."'";
$result=mysql_query($sql);
if($result)
{
$res=mysql_fetch_array($result);
return $res['num'];
}
else
{return "������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error();}

}



}






class categories2 extends DBTable
{

    
    
function __construct()
{
parent::__construct(array("ID","ParentID","Title"),"sample_offercategory","ID");

}
}


class categories extends DBTable
{
private $_cat_path;
    
    
function __construct()
{
parent::__construct(array("id","name","parent_id","discription","level"),"categories","id");
$this->setTableRus("��������� �������� ��������",array("ID","��������","ID ������������� ���������","��������","������� ���������"));

$this->SetAddText("�������� ���������");


$this->AddFieldType(new ID()); 
$this->AddFieldType(new Text("",STRONG,40));
$this->AddFieldType(new Text("",STRONG,10));
$this->AddFieldType(new RedactorField("",NOT_STRONG)); 
$this->AddFieldType(new Text("",STRONG,10));



}

function getCatPath()
{
return $this->_cat_path;
}

function RecurseCatSearch($cur_cat_id,&$result)
{

$children=$this->GetChildCats($cur_cat_id);

if($children&&mysql_numrows($children)>0)
{
    while($child=mysql_fetch_array($children))
    {
        $this->RecurseCatSearch($child['id'],$result);
    }
}
else
    $result[]=$cur_cat_id;
}


function getTopCategoryType($cur_cat_id)
{
$cur_cat_info=$this->getParentCat($cur_cat_id);
$this->_cat_path[]=array("name"=>$cur_cat_info['name'],"id"=>$cur_cat_info['id'],"level"=>$cur_cat_info['level']);
if($cur_cat_info['parent_id']==0)
{
    return $cur_cat_info['level'];
}
else
{
    return $this->getTopCategoryType($cur_cat_info['parent_id']);
}

}


function getCategoryPath($cur_cat_id)
{
unset($this->_cat_path);
$this->getCategoryPathRecursive($cur_cat_id);
return $this->_cat_path;
}

function getCategoryPathRecursive($cur_cat_id)
{
$cur_cat_info=$this->getParentCat($cur_cat_id);
if($cur_cat_info!=-1)
{
$this->_cat_path[]=array("name"=>$cur_cat_info['name'],"id"=>$cur_cat_info['id'],"level"=>$cur_cat_info['level'],"discription"=>$cur_cat_info['discription']);
if($cur_cat_info['parent_id']!=0)
{
   $this->getTopCategoryType($cur_cat_info['parent_id']);
}
}

}



function getParentCat($cat_id)
{
 $sql="SELECT id,name,parent_id,level,discription FROM `".$this->_table_name."` WHERE `id`=".$cat_id;
$result=mysql_query($sql);
if($result)
{
if(mysql_numrows($result)>0)
{
$res=mysql_fetch_array($result);
return $res;
}
else
{
return -1;
}
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}

}




function GetChildCats($cat_id)
{
 $sql="SELECT * FROM `".$this->_table_name."` WHERE `parent_id`=".$cat_id." ORDER BY `name` ASC";
$result=mysql_query($sql);
if($result)
{
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}
    

}

function GetMainCats()
{
 $sql="SELECT * FROM `".$this->_table_name."` WHERE `parent_id`=0 ORDER BY `id` ASC";
$result=mysql_query($sql);
if($result)
{
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; }

}



}











              ////////////////////////////////////////////////////////////////////////////










class admin_users extends DBTable
{
function __construct()
{
parent::__construct(array("id","name","pass","status"),"admin_users","id");
$this->setTableRus("���������� ����������������",array("ID","�����","������","������"));    
$this->SetAddText("�������� ��������������");


$this->AddFieldType(new ID());
$this->AddFieldType(new Text("",STRONG,30));
$this->AddFieldType(new Text("",STRONG,30));





global $admin_status;
$admin_status_field=new ComboArray("1",STRONG,$admin_status);
$admin_status_field->SetOnViewFunction("OnViewFuncAdminStatus");
$this->AddFieldType($admin_status_field);


//$this->SetPermissions(array("edit"=>0,"delete"=>0,"add"=>0,"view"=>0));

}

function Login($login,$pass)
{
$sql="SELECT * FROM `admin_users` WHERE `name`='".$login."' AND `pass`='".$pass."'";

$result=mysql_query($sql);
if($result)
{
    if(mysql_numrows($result)>0)
    {
        return mysql_fetch_array($result);
    }
    else
    {
        return false;
    }


    
return $result;
}
else
{
    $this->setError("������ � ������� � ���� ������! �������� �������� ������ ����������! ������:".mysql_error());
    return false; 
}

}

}



    






?>