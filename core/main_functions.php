<?

function Out($str)
{
    return trim(hsc(strip_tags($str),null,"cp1251"));
}

function getData($str)
{
    return trim(strip_tags($str));
}

function hsc($str)
{
    return htmlspecialchars($str,null,"cp1251");
}


//deprecated?
function IsStrictString($string)
{
    return preg_match("/^[a-zA-Z0-9\s�-��-�]+$/",$string);
}

function IsCharsName($string)
{
    return preg_match("/^[a-zA-Z�-��-�\s]+$/",$string);
}

function IsStrictName($string)
{
    return preg_match("/^[-a-zA-Z0-9\s�-��-�\"\.]+$/",$string);
}

function IsStrictPhone($string)
{
    return preg_match("/^[-\s0-9\(\)\+]+$/",$string);
}


function IsDigits($string)
{
    return preg_match("/^[0-9]+$/",$string);
}


function IsUsualString($string)
{
    return preg_match("/^[-a-zA-Z0-9\s�-��-�,\.:;\?\(\)%=+@\"\/]+$/",$string);
}

function IsNum($string)
{
    return is_numeric($string)&&preg_match("/^[0-9]{1,13}$/",$string);
}

function checkEmail($email)
{
return preg_match("|^([-a-z0-9_\.]{1,20})@([-a-z0-9\.]{1,20})\.([a-z]{2,4})|is", strtolower($email));
}

function getSite($website)
{
if(!strstr($website,"http://"))
    return "http://".$website;
else
    return $website;
}



function getPermission()
{
if(isset($_SESSION['user_id_macro']))
{return true;}
else
{return false;}
}

function getUserLogin()
{
return $_SESSION['user_login_macro'];
}


function getUserName()
{
return $_SESSION['user_name_macro'];
}


function getUserSiteAddress()
{
return $_SESSION['user_site_macro'];
}

function setUserSiteAddress($data)
{
    $_SESSION['user_site_macro']=$data;
}





function getUserID()
{
return $_SESSION['user_id_macro'];
}


function getUserPermission($user_id)
{
if(($_SESSION['user_id_macro']==$user_id)&&isset($_SESSION['user_id_macro']))
{return true;}
else
{return false;}

}





function unsetPastUrl()
{
unset($_SESSION['past_url']);
}



function setPastUrl($url)
{
$_SESSION['past_url']=$url;
}

function getPastUrl()
{
return $_SESSION['past_url'];
}

function isPastUrl()
{
if(isset($_SESSION['past_url']))
{return true;}
else
{return false;}

}


function LoginFunc($login,$pass,$id,$name,$site_address,$cookies_set)
{
$_SESSION['user_login_macro']=$login;
$_SESSION['user_id_macro']=$id;
$_SESSION['user_name_macro']=$name;
$_SESSION['user_site_macro']=$site_address;


if(!$cookies_set)//���� �� ����������� ����� �� ����� ���������� ��
{
$time=mktime(date("H"), date("i"), date("s"), date("m")+12, date("j"), date("Y"));
setcookie("macro_user_login", $login, $time);
setcookie("macro_user_pass", $pass, $time);
}


$users_entries_gateway=new users_entries();
$users_entries_gateway->Add(array("NULL",NowDateTime(),$id));


//$page_name="main";
//if(!isset($_POST['user_login_access']))
//{$page_name="main";}
//else
//{//$page_name="user_admin";


if($_SERVER['QUERY_STRING']!="page=login"&&$_SERVER['QUERY_STRING']&&$_SERVER['QUERY_STRING']!="page=register")
    header ("location: index.php?{$_SERVER['QUERY_STRING']}");
/*elseif(isset)
    header ("location: /index.php?page=set_main_info&");*/
else
    header ("location: /p_set_main_info");


flush();
exit();

}



function getHttpHostMain()
{
    if($_SERVER['HTTP_HOST'])
    {
     $ar=explode(".",$_SERVER['HTTP_HOST']);
     if(count($ar)==1)
        return $ar[0];
     else if(count($ar)==2)
        {
            if($ar[0]=="rumacro")
                return $ar[0].".".$ar[1];
            else
                return $ar[1];
        }
     else if(count($ar)==3)
        return $ar[1].".".$ar[2];
     else
        return $ar[count($ar)-2].".".$ar[count($ar)-1];
    }
    else
        return "rumacro.ru";
}


function getHttpHost()
{
 return "http://".getHttpHostMain();
}



         
function replace_links($str)
{
    return preg_replace('/<a(.+?)href=("|\')(.+?)("|\')(.*?)>(.+?)<\/a>/i', '<a$1href="http://www.rumacro.ru/out.php?link=$3"$5>$6</a>', $str);
}
         
         



function getUserSiteUrl($user_id=false,$site_address=false)
{
    $server=getHttpHostMain();
    
    if(!$user_id)
    {
        if(!getUserSiteAddress())
            return "http://".$server."/site-".getUserID();
        else
            return "http://".getUserSiteAddress().".{$server}";
    }
    else
    {
        if(!$site_address)
            return "http://".$server."/site-".$user_id;
        else
            return "http://{$site_address}.{$server}";
    }    
}

function getUserCatUrl($user_id,$site_address,$cat_id,$cat_name)
{
    if($site_address)
    {
        $server=getHttpHostMain();    
        return "http://".$site_address.".".$server."/cat-".$cat_id."-".translite($cat_name); 
    }
        
    else
        return "usercat-".$user_id."-".$cat_id."-".translite($cat_name); 
}

function getUserArtUrl($user_id,$site_address,$art_id,$art_name)
{
    if($site_address)
    {
        $server=getHttpHostMain();    
        return "http://".$site_address.".".$server."/art-".$art_id."-".translite($art_name); 
    }
    else
        return "userart-".$user_id."-".$art_id."-".translite($art_name); 
}


function getUserNewsUrl($user_id,$site_address,$new_id,$new_name)
{
    if($site_address)
    {
        $server=getHttpHostMain();    
        return "http://".$site_address.".".$server."/news-".$new_id."-".translite($new_name); 
    }
    else
        return "usernews-".$user_id."-".$new_id."-".translite($new_name); 
}


function getSitePageUrl($label,$user_id,$site_address)
{
    
    if($site_address)
        {
            $server=getHttpHostMain();    
            return "http://".$site_address.".".$server."/~".$label; 
        }
    else
        return "spu-".$user_id."-".$label; 
}


function getGoodUrl($good_id,$good_name,$host=true)
{
    if($host)
        return getHttpHost()."/item-{$good_id}-".translite($good_name);
    else
        return "/item-{$good_id}-".translite($good_name);
}


function getMacronewsItem($news_id,$host=true)
{
    if($host)
        return getHttpHost()."/macronews-{$news_id}";
    else
        return "/macronews-{$news_id}";
}


function getTestimonialVerifyLink($hash,$host=true)
{
    $host_str = "";
    if($host) {
        $host_str = getHttpHost();
    }
    
    return $host_str."/index.php?page=verifytestimonial&hash={$hash}";
}


function getViewMessageLink($mess_id,$host=true)
{
    if($host)
        return getHttpHost()."/index.php?page=view_message&id={$mess_id}";
    else
        return "/index.php?page=view_message&id={$mess_id}";
}





function getCatLink($cat_id,$cat_name,$area_cond="",$name_cond="")
{
    if(!($area_cond.$name_cond))
        //return getHttpHost()."/category-{$cat_id}-".TextHelper::translite($cat_name);
        return "/category-{$cat_id}-".TextHelper::translite($cat_name);
    else
        return "index.php?page=items&cat_id=".$sub_cat['id'].$area_cond.$name_cond;
}




function getPageLink($page_label,$host=true)
{
    if($host)
    {
        $server=getHttpHostMain();
        if($page_label)
            return "http://".$server."/p_".$page_label;  
        else
            return "http://".$server;
    }  
    else
    {
        return "/p_".$page_label;
    }
}






function getServicePic($profile_level,$size="")
{       
    if(in_array($profile_level,array(2,3,4,5)))
        return "<img src='img/service{$profile_level}{$size}.png'>";
    else 
        return "";
}

function getServiceStarsPic($profile_level)
{       
    if(in_array($profile_level,array(2,3,4,5)))
        return "<img src='img/service{$profile_level}_star.png'>";
    else 
        return "";
}




function getMailTemplate($label,$vars=false,$data_vars=false)
{
    $mail_templates_gateway=new mail_templates();
    $main_text_templ=$mail_templates_gateway->getRecByField("label","main");
    $data_text_templ=$mail_templates_gateway->getRecByField("label",$label);
    $text_templ=str_replace(array("%%EMAIL_DATA%%"),array($data_text_templ['text']),$main_text_templ['text']);
    if($vars&&$data_vars)
        return str_replace($vars,$data_vars,$text_templ);
    else
        return $text_templ;
}

function getDirectMailTemplate($label,$vars=false,$data_vars=false)
{
    $mail_templates_gateway=new mail_templates();
    $data_text_templ=$mail_templates_gateway->getRecByField("label",$label);
    $text_templ=$data_text_templ['text'];
    if($vars&&$data_vars)
        return str_replace($vars,$data_vars,$text_templ);
    else
        return $text_templ;
}



function Redirect($url)
{
    header ("location: ".$url);
    flush();
    exit();
}




function getVerific()
{
    return "<div class='verific'>�������� ��������������</div>";
}



function getUserLevel($user_id)
{
    $users_gateway=new users();
    $user=$users_gateway->getRecFieldsById($user_id,array("profile_level"));
    return $user['profile_level'];
}

function getMemberInfo($profile_level)
{
    if(in_array($profile_level,array(0,1,2,3,4,5)))
    {
        $members_features_gateway=new members_features();
        $member=$members_features_gateway->getRecById($profile_level);
        return $member;
    }
    else
        return false;
}

function getMember($profile_level,$data)
{
    if(in_array($profile_level,array(0,1,2,3,4,5)))
    {
        $members_features_gateway=new members_features();
        $member=$members_features_gateway->getRecFieldsById($profile_level,array($data));
        return $member[$data];
    }
    else
        return false;
}


function Cut($str,$num)
{
if(strlen($str)<=$num)
    return $str;
else
    return substr($str,0,$num)."...";
}



function BuyPocket($user_id,$pocket_id)
{
$members_features_gateway=new members_features();
$pocket=$members_features_gateway->getRecById($pocket_id);

    
//�������� �� ������� ������
$users_gateway=new users();   
$user=$users_gateway->getRecFieldsById($user_id,array("paid_to","profile_level"));
if($user)
{

    if(in_array($user['profile_level'],array(PROFILE_LEVEL_1,PROFILE_LEVEL_2,PROFILE_LEVEL_3,PROFILE_LEVEL_4)))
    {
        if(DateSmallerOrEqual($user['paid_to'],NowDate()))
            $paid_to=NowDateDay(365);
        else
            $paid_to=DatePlusDay($user['paid_to'],365);
    }
    else
        $paid_to=NowDateDay(365);
        
    $users_gateway->UpdateFields($user_id,array("profile_level","paid_to"),array($pocket_id,$paid_to));
                    
    //��������� ��������� �������� ���������� � ������������ � �������
                                            
                    $params=array(
                                            "NULL",
                                            $user_id,
                                            FormatDateForView(NowDate()),
                                            "��������� �������� � ������������ � ������� ".$pocket['member_name'],
                                            $pocket['adv_goods']." �������",
                                            $pocket['price_adv_yandex'],
                                            $pocket['price_adv_yandex'],
                                            0,//google
                                            FormatDateForView(NowDate()),
                                            "0000-00-00",
                                            "0000-00-00",
                                            4//��������
                                            );
                                            
                                            
                    $context_gateway=new context();
                    $context_gateway->Add($params);
                    

}

}


function CreateBillForUser($params,$send_email=false,$email="",$name_of_item="",$fio="",$sum_to_pay="")
{
                $money_bills_gateway=new money_bills();
               if($money_bills_gateway->Add($params))
                {
                    $success=true;
                    $new_bill_id=mysql_insert_id();
                    $new_bill=$money_bills_gateway->getRecFieldsById($new_bill_id,array("uid","user_id"));
                    $new_bill_uid=$new_bill['uid'];
                    $new_bill_user_id=$new_bill['user_id'];
                    $users_gateway=new users();
                    $user=$users_gateway->getRecFieldsById($new_bill_user_id,array("site_address"));
                    
                       
                    //���������� ����� � ������������ � ������
                    if($send_email&&$email)
                    {
                        $mail_tamplate=getMailTemplate("bill_created",array("%%LINK_BILL%%","%%SERVICE_NAME%%","%%FIO%%","%%SERVICE_PRICE%%","%%SITE_LINK%%"),
                        array("http://www.rumacro.ru/account.php?account=".$new_bill_uid,$name_of_item,$fio,$sum_to_pay,getUserSiteUrl($new_bill_user_id,$user['site_address'])));
                        $pics=array("img/macro_logo.jpg");
                              
                        MailToMeMagzone($email,'����� ����� �� rumacro.ru',$mail_tamplate,'do-not-reply@rumacro.ru','������ ��������� rumacro.ru',$pics);  
                    }
                    
                    return array($new_bill_uid,$new_bill_id);

                }
               else
                return array(false,false);
}



function DateEqual($date1,$date2)
{
list($year1,$month1,$day1)=split("-",$date1);
list($year2,$month2,$day2)=split("-",$date2);
    
    $date1_=mktime(0,0,0,$month1,$day1,$year1);
    $date2_=mktime(0,0,0,$month2,$day2,$year2);
    
    if($date1_==$date2_)
    {return true;}
    else
    {return false;}
}




///////////////////////////////////////////////////////////////

function getArray($data)
{
    if(is_array($data))
        return $data;
    else
        return array($data);
}



class TextHelper
{
    public static function Out($str,$nl2br=false,$strip_tags=true,$exeption_keys=false)
    {
        if(is_array($str))
        {
            $str_temp = array();
            $exeption_keys_arr=getArray($exeption_keys);
            foreach($str as $key=>$value)
                if($exeption_keys&&in_array($key,$exeption_keys_arr))
                    $str_temp[$key]=$value;
                else
                    $str_temp[$key] = TextHelper::Out($value,$nl2br,$strip_tags);        
                
            return $str_temp;
        }
        else
        {
            if($strip_tags)
                $str=strip_tags($str);
                
            $res=hsc($str,null,"cp1251");
            if($nl2br)
                $res=nl2br($res);
            return $res;
        }
    }

   
    
    
    public static function Cut($str,$num)
    {
        if(strlen($str)<=$num)
            return $str;
        else
            return substr($str,0,$num)."...";
    }
    
    
    public static function translite($string)
    {
    $string=strtolower($string);
      $trans = array("�"=>"a","�"=>"b","�"=>"v","�"=>"g","�"=>"d","�"=>"e", "�"=>"yo","�"=>"j","�"=>"z","�"=>"i","�"=>"i","�"=>"k","�"=>"l", "�"=>"m",
      "�"=>"n","�"=>"o","�"=>"p","�"=>"r","�"=>"s","�"=>"t", "�"=>"y","�"=>"f","�"=>"h","�"=>"c","�"=>"ch", "�"=>"sh","�"=>"sh","�"=>"i","�"=>"e","�"=>"u",
      "�"=>"ya"," "=>"-",
      "�"=>"a","�"=>"b","�"=>"v","�"=>"g","�"=>"d","�"=>"e", "�"=>"yo","�"=>"j","�"=>"z","�"=>"i","�"=>"i","�"=>"k", "�"=>"l","�"=>"m","�"=>"n","�"=>"o","�"=>"p", 
      "�"=>"r","�"=>"s","�"=>"t","�"=>"y","�"=>"f", "�"=>"h","�"=>"c","�"=>"ch","�"=>"sh","�"=>"sh", "�"=>"i","�"=>"e","�"=>"u","�"=>"ya",
      "�"=>"","�"=>"","�"=>"","�"=>"","-"=>"-","."=>"",":"=>"","\""=>"",","=>"",";"=>"","?"=>"","%"=>"");
      return strtr($string, $trans);
    }


    public static function InsertAtags($text1)
    {
    $text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)', 
       '<noindex><a href="\\1" rel="nofollow">\\1</a></noindex>', $text1); 
    $text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)', 
       '<noindex>\\1<a rel="nofollow" href="http://\\2">\\2</a></noindex>', $text); 
    $text = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})', 
       '<a href="mailto:\\1">\\1</a>', $text);
     return $text;
    }

    
    public static function replace_links($str)
    {
        return preg_replace('/<a(.+?)href=("|\')(.+?)("|\')(.*?)>(.+?)<\/a>/i', 
        "<a$1href=\"/out.php?link=$3\"$5>$6</a>", $str);
    }

    public static function replace_linksHref($text)
    {
         $text= preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"/out.php?link=$3\" >$3</a>", $text);
         $text= preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http:///out.php?link=$3\" >$3</a>", $text);
         return $text;
    }
}



function include_partial($label,$params=null)
{
    if($params)
        foreach($params as $key=>$value)
            $$key=$value;
            
    if(file_exists(getDocumentRoot()."templates/partial/".$label.".php"))
        include(getDocumentRoot()."templates/partial/".$label.".php");
    if(file_exists(getDocumentRoot()."templates/partial/".$label.".html"))
        include(getDocumentRoot()."templates/partial/".$label.".html");
}



function collection_partial($collection,$label,$params=null)
{
    if(!$collection)
        return false;
        
    if($params)
        foreach($params as $key=>$value)
            $$key=$value;
            
    if(!$htmlspecialchars)
        $htmlspecialchars=false;
        
    if(!$nl2br)
        $nl2br=false;
        
    if(!$striptags)
        $striptags=false;
            
            
    
                if(mysql_num_rows($collection))
                    {
                        $counter = 1;
                        while($item=mysql_fetch_array($collection))
                        {
                            if($htmlspecialchars)
                                $item = TextHelper::Out($item,$nl2br,$striptags);
                                
                            if(file_exists(getDocumentRoot()."templates/partial/".$label.".php"))
                                include(getDocumentRoot()."templates/partial/".$label.".php");
                            if(file_exists(getDocumentRoot()."templates/partial/".$label.".html"))
                                include(getDocumentRoot()."templates/partial/".$label.".html");
                                
                            $counter++;
                        }
                    }
            
}


class REQUEST
{
    
    public static function GetIp()
    {
     if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
     {
       $ip=$_SERVER['HTTP_CLIENT_IP'];
     }
     elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
     {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
     }
     elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
     {
      $ip=$_SERVER['HTTP_X_Real_IP'];
     }
     elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
     {
      $ip=$_SERVER['HTTP_X_Real_IP'];
     }
     else
     {
       $ip=$_SERVER['REMOTE_ADDR'];
     }
     return $ip;
    }
    
    
    public static function isMethodPost()
    {
         if(count($_POST)||count($_FILES))
            return true;
         else
            return false;
    }
    
    public static function getParam($param,$def_value=false,$strip_tags=false)
    {
         if(isset($_POST[$param]))
        {
            if(!$strip_tags)
                return $_POST[$param];
            else
                return strip_tags($_POST[$param]);
        }
         elseif(isset($_GET[$param]))
         {
             if(!$strip_tags)
                    return $_GET[$param];
                else
                    return strip_tags($_GET[$param]);
         }
         elseif($def_value)
            return $def_value;
         else
            return null;
    }
    
    public static function Cookies($param,$value=false,$time=false)
    {
         if($value===false)
            return $_COOKIE[$param];
         else
         {
             if(!$time)
                $time=mktime(date("H"), date("i"), date("s"), date("m")+12, date("j"), date("Y"));
                
             if($value==="")
                setcookie($param);
             else
                setcookie($param,$value,$time);
         }
             
    }
}







 



?>