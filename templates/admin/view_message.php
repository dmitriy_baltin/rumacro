<?php
if(!isset($_GET['id'])||!is_numeric($_GET['id'])){
    Redirect(getPageLink("404"));
}
$item_id = (int)$_GET['id'];
$table_gateway=new user_messages(); 
$item = $table_gateway->getRecById($item_id);
$product_name = "";
if($item['product_id']){
    $goods_gateway = new goods();
    $product = $goods_gateway->getRecFieldsById($item['product_id'],array("name"));
    $product_link = getGoodUrl($item['product_id'],$product['name']);
    $product_name = Out($product['name']);    
}
if(!$item){
    Redirect(getPageLink("404"));
}
if($item['user_id']!=getUserID()){
    Redirect(getPageLink("404"));
}
if($item['new']==1){
    $now_date_time = NowDateTime(); 
    $table_gateway->UpdateFields($item_id,array("new","datetime_read"),array(0,$now_date_time));
    $item['datetime_read'] = $now_date_time;
}

$cur_page_header="�������� ���������";
if($item['subject']){
    $cur_page_header=Out($item['subject']);
}
$seo_title = $cur_page_header; 

$user_messages_replies_gateway = new user_messages_replies();

$item_name = Out($item['name']);
$item_email = Out($item['email']);
$item_phone = Out($item['phone']);
$item_text = nl2br(Out($item['text']));
$item_datetime_read = FormatDateTimeForView2($item['datetime_read']);
$item_datetime = FormatDateTimeForView2($item['datetime']);

$is_post = false;
$error = "";
if(Form::isPost()){
    $is_post = true;
    $users_gateway = new users();
    $user = $users_gateway->getRecFieldsById(getUserID(),array("email","site_address"));
    $reply_text = $_POST['text'];
    if(!$reply_text){
        $error.="������� ����� ���������<br>";  
    }
    if(!$error){
        $user_messages_replies_gateway->Add(array("NULL",$item_id,getUserName(),$user["email"],$reply_text,NowDateTime()));
        
        //�������� ������������ ��������� �� ����� getUserName(),$user["email"]
        $mail_tamplate=getDirectMailTemplate("new_message_reply",
                                            array("%%FIO%%","%%SITE_LINK%%","%%SENDER_NAME%%","%%PROJECT_URL%%","%%TEXT%%"),
                                            array(
                                                $item['name'],getUserSiteUrl(getUserID(),$user['site_address']),
                                                getUserName(),PROJECT_URL,nl2br($reply_text)
                                            )
                                            );
        $res = MailToMeMagzone(
                        trim($item['email']),'��������� �� '.getUserName(),$mail_tamplate,
                        $user['email'],getUserName(),array()
                        );
    } 
}
$replies = $user_messages_replies_gateway->getWhereExt("message_id={$item_id}",false,"id","DESC");  
?>
