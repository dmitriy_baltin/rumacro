<?php
	//user_sitemap_gen
	include("config.php");
	include("core/functions.php");
	include("core/main_functions.php");
	include("core/database.php");
	include("core/sitemapgen_extended.php");

	$db=new DataBase();
	
	$sm=new SiteMapWriter(false); //false = output_to screen
	$sm->BeginWrite();

	$url=$_GET['host'];

	$gateway=new users();
	$user=$gateway->getRecByField("site_address",$url);

	if(!isset($user)) return;

	$user_id=$user['id'];

	$sm->Write(getUserSiteUrl($user_id,$url),"Daily","1");
	$sm->Write(getSitePageUrl("items",$user_id,$url),"Monthly","1");
	$sm->Write(getSitePageUrl("paydelivery",$user_id,$url),"Monthly","1");
    $sm->Write(getSitePageUrl("testimonials",$user_id,$url),"Monthly","1");
	$sm->Write(getSitePageUrl("about",$user_id,$url),"Monthly","1");
	$sm->Write(getSitePageUrl("contacts",$user_id,$url),"Monthly","1");
	$sm->Write(getSitePageUrl("news",$user_id,$url),"Monthly","1");
	$sm->Write(getSitePageUrl("articles",$user_id,$url),"Monthly","1");

	$gateway=new user_categories();
	$items = $gateway->getWhere("user_id",$user_id);
	while($item = mysql_fetch_array($items)){
	    $sm->Write(getUserCatUrl($user_id,$url,$item['id'],$item['name']),"Monthly","0.8");
	}


	$gateway=new user_news();
	$items = $gateway->getWhere("user_id",$user_id);
	while($item = mysql_fetch_array($items))
	{
	    $sm->Write(getUserNewsUrl($user_id,$url,$item['id'],$item['name']),"Monthly","0.8");
	}


	$gateway=new user_articles();
	$items = $gateway->getWhere("user_id",$user_id);
	while($item = mysql_fetch_array($items))
	{
	    $sm->Write(getUserArtUrl($user_id,$url,$item['id'],$item['name']),"Monthly","0.8");
	}

	$sm->EndWrite();    
?>