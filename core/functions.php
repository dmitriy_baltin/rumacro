<?                           

class Form{
    
    public static function isPost(){                              
        if((count($_POST)||count($_FILES))&&Form::checkCSRFToken()){
            return true;
        }
        else{
            return false;
        }
    }
    
    public static function checkCSRFToken(){
        
        if(!isset($_POST[Form::getCSRFFieldName()])){
            return false;
        }
        $csrf_value = $_POST[Form::getCSRFFieldName()];
        if(!isset($_SESSION[Form::getCSRFSessionName()])){
            $_SESSION[Form::getCSRFSessionName()] = array($csrf_value);
            return true;
        }
        else{
            if(in_array($csrf_value,$_SESSION[Form::getCSRFSessionName()])){
                return false;
            }
            else{
                $_SESSION[Form::getCSRFSessionName()][]=$csrf_value;
                return true;
            }
        }
        
        
    }
    public static function renderCSRFToken(){
        printHidden(Form::getCSRFFieldName(),uniqid(),10);
    }
    public static function getCSRFFieldName(){
        return "_csrftoken";
    }
    public static function getCSRFSessionName(){
        return "session_csrftoken";
    }
}
  
//////////////////////////////////////////////////Project FUNC///////////////////////////////////

function OnViewMacroNewsOnMainFunction($value,$id){
    $result = "<input data-id='{$id}' class='macro_on_main' type='checkbox' ";
    if( $value==1 ){
        $result.= "checked";    
    }
    $result.= ">";
    return $result;
}

function includeSimpleZoom()
{
echo "<script type='text/javascript' src='js/zoom/zoom.js'></script>
<link rel='stylesheet' type='text/css' href='js/zoom/zoom.css'>";
}

function includeTips()
{
//echo "<script type='text/javascript' src='js/jquery.poshytip.js'></script>";
//echo "<link rel=STYLESHEET href='js/tip-green/tip-green.css' type='text/css'>";
}
   
   
   
   
function EchoOrderForm()
{
   echo "
<script src='js/JsHttpRequest/lib/JsHttpRequest/JsHttpRequest.js' type='text/javascript'></script>
<div id='PopUpOrderForm' class='popup_block'>
                <div id='popupheader'></div>
                <div id='popupcontent'>";
echo "</div></div>";

}

function EchoPickCity($id_tag,$name_tag,$params_tag,$selected_city_id,$added_cities=false,$add_invite_option=true,$AddOptionFunc="AddOption",$CityTagChangeFunc="CityTagChange")
{

echo "<select id='{$id_tag}' name='{$name_tag}' {$params_tag} onchange=\"{$CityTagChangeFunc}('{$id_tag}');\">";

if($add_invite_option)
        echo "<option value='0'> ** �������� ����� ** </option>";
        
if($added_cities&&is_array($added_cities))
{
    
    foreach ($added_cities as $key => $value)
    {                                     
            echo "<option value='{$key}'";
            if($key==$selected_city_id)
                 echo " selected";
            echo ">{$value}</option>";
    }
        
}

 

    echo "<option value='-1'> ** ������� ������ ����� ** </option>";
                                                                 
    echo "</select>";                                            

echo "
<div id='PopUpWindowCity' class='popup_block'>
                <div id='popupheader'></div>
                <div id='popupcontent'>";
                echo "<span>����� ������</span>
                <div class='select_cities_list'>";
    

$pics_per_row=6; $counter=0;     
$cities_gateway=new cities();
$cities=$cities_gateway->getWhere("on_main",1,"name","ASC");

while($city=mysql_fetch_array($cities))
{

if(!$counter)
    echo "<div class='reg_cont2'>";

    echo "<div><a href='#' onclick=\"return {$AddOptionFunc}('{$city['id']}','{$city['name']}','{$id_tag}')\">{$city['name']}</a></div>";

if(($counter+1)%$pics_per_row==0)
{
    $counter=-1;
    echo "</div>";
}

 $counter++;
}

if($counter)
    echo "</div>";

    
echo "<div class='clr'></div>
</div>"; 


echo "<div class='city_select'>";

echo "<div id='id_region_id_cont'><select id='id_region_id' onchange=\"LoadCities('id_region_id');\">";

$regions_gateway=new regions();
$regions=$regions_gateway->getAllRecs("sort","ASC");


echo "<option value='0' selected>** �������� ������ **</option>";
    while($region=mysql_fetch_array($regions))    
    {
        echo "<option value='{$region['id']}'>{$region['name']}</option>";
    }
echo "</select></div>";

echo "<div id='id_city_id_cont'><select id='id_city_id' onchange=\"CheckCity('id_city_id');\" disabled>";
//echo "<option value='0' selected></option>";
echo "</select></div>";
echo "<div><button id='select_city_but' onclick=\"{$AddOptionFunc}(\$('#id_city_id :selected').val(),\$('#id_city_id :selected').text(),'{$id_tag}');\" type='button' disabled>�������</button></div>";


echo "<div class='clr'></div></div>";



 echo "</div>";
 echo "</div>";
}


function EchoPayWay($item_id,$item_name,$item_active)
{
    if($item_active) $checked="checked";
    
                return "
                <div class='pd_item' id='pitem_{$item_id}'>
                    <div class='pd_item_name'>
                        <input type='checkbox' id='pck{$item_id}' onchange=\"ActiveChanged({$item_id},'p')\" {$checked}>
                        <span id='pitem_name{$item_id}'>{$item_name}</span>
                    </div>
                    <div class='pd_item_action'>
                        <span id='pedit_but_{$item_id}'><a href='#' onclick='return EditPayWay({$item_id});'><img src='img/edit.png'></a></span>
                        <span id='prem_but_{$item_id}'><a href='#' onclick=\"return RemovePD({$item_id},'p');\"><img src='img/close_red.png'></a></span>
                    </div>
                    <div class='clr'></div>
                </div>";
}


function EchoDelivery($item_id,$item_name,$item_active)
{
    if($item_active) $checked="checked";
    
                return "
                <div class='pd_item' id='ditem_{$item_id}'>
                    <div class='pd_item_name'>
                        <input type='checkbox' id='dck{$item_id}' onchange=\"ActiveChanged({$item_id},'d')\" {$checked}>
                        <span id='ditem_name{$item_id}'>{$item_name}</span>
                    </div>
                    <div class='pd_item_action'>
                        <span id='dedit_but_{$item_id}'><a href='#' onclick='return EditDelivery({$item_id});'><img src='img/edit.png'></a></span>
                        <span id='drem_but_{$item_id}'><a href='#' onclick=\"return RemovePD({$item_id},'d');\"><img src='img/close_red.png'></a></span>
                    </div>
                    <div class='clr'></div>
                </div>";
}


function EchoAddCatForm($id_tag,$is_post,$dest_url="")
{
    
    $user_categories_gateway=new user_categories();
    $main_cats=$user_categories_gateway->GetMainCats(getUserID());
    
    
    $user_prototypes_gateway=new user_prototypes();
    $prototypes=$user_prototypes_gateway->getWhereExt("user_id=".getUserID(),array("id","name"));
    
    $user_managers_gateway=new user_managers();
    $managers=$user_managers_gateway->getWhereExt("user_id=".getUserID(),array("id","name"));
    
    
    echo "
    <script>
    function AddCatForm{$id_tag}()
    {
        OpenExt('AddCatForm{$id_tag}','#?w=800','ClosePopUp()');
    }
    </script>
    
    <div id='AddCatForm{$id_tag}' class='popup_block'>
                <div id='popupheader'></div>
                <div id='popupcontent'>
                <span>����� ������</span>";
                
                if($is_post)
                    echo "<form id='add_group_form' action='{$dest_url}' method='post'  enctype='multipart/form-data'>";
                    
                echo "<div id='add_group_form_cont'>
                        <div class='d_punkt'>
                            <div class='label'>������������ ���������<span>*</span></div>
                            <div class='input'>";
                                             
                            echo "<select id='new_cat_parent_id' style='width:350px;' name='group_parent_id'>";
                            echo "<option value='0' selected>�������� ������</option>";
                            while($main_cat=mysql_fetch_array($main_cats))
                                {
                                    $main_cat_name=hsc(strip_tags($main_cat['name']));
                                    echo "<option value='{$main_cat['id']}'>{$main_cat_name}</option>";
                                }
                            echo "</select>";
                            
                            echo "</div>
                            <div class='clr'></div>
                        </div>
                        
                        <div class='d_punkt'>
                            <div class='label'>�������� ������<span>*</span></div>
                            <div class='input'>";
                            
                            printTextLine("group_name","",55,"style='width:350px;'");
                            
                            echo "<div class='input_text'>��������: �������������� ��� ����������</div>";
                            echo "</div>
                            <div class='clr'></div>
                        </div>
                        
                        <div class='d_punkt'>
                            <div class='label'>�������� ������</div>
                            <div class='input'>";
                            printTextExtParams("group_disc","",10,45,"style='width:350px;'");
                            echo "<div class='input_text'>�� 3000 ��������</div>";
                            echo "</div>
                            <div class='clr'></div>
                        </div>
                        
                        
                        <div class='d_punkt'>
                            <div class='label'>�������� ������</div>
                            <div class='input'>";
                                             
                            echo "<select style='width:350px;' class='good_text' id='id_group_prototype_id' name='group_prototype_id'>";
                                echo "<option value='0' selected>�� ������</option>";
                                while($prototype=mysql_fetch_array($prototypes))
                                {
                                    $prototype_name=hsc(strip_tags($prototype['name']));
                                    echo "<option value='{$prototype['id']}'>{$prototype_name}</option>";
                                }
                            echo "</select>";
                            
                            echo "</div>
                            <div class='clr'></div>
                        </div>
                        
                        
                        <div class='d_punkt'>
                            <div class='label'>�������� ������</div>
                            <div class='input'>";
                                             
                            echo "<select style='width:350px;' class='good_text' id='id_group_manager_id' name='group_manager_id'>";
                                echo "<option value='0' selected>�� ������</option>";
                                while($manager=mysql_fetch_array($managers))
                                {
                                    $manager_name=hsc(strip_tags($manager['name']));
                                    echo "<option value='{$manager['id']}'>{$manager_name}</option>";
                                }
                            
                            echo "</select>";
                            
                            echo "</div>
                            <div class='clr'></div>
                        </div>
                        
                        

                <div class='subm_but_cont'>";
                
                    if($is_post)
                        echo "<input class='submit' type='submit' value='�������� ������'>";
                    else
                        echo "<button class='submit' id='add_group_but'>�������� ������</button>";
                        
                        echo "<div id='add_process_begin'></div>";
                        
                echo "</div>

                        
                </div>";
                


                if($is_post) echo "</form>";
                echo "</div>
    </div>";
    
    
}

function EchoPickRegion($id_tag,$params_tag,$area_type,$area_id,$area_name,$select_func,$cat_sort_cond,$name_cond,$delete_return_page="items")
{

printHidden($id_tag."area_type",$selected_type,10);
printHidden($id_tag."area_id",$selected_id,10);

if(!$area_name)
    $area_name="������� ������";
    
echo "<a id='{$id_tag}' onclick='return SelectRegion();' href='#' {$params_tag}>{$area_name}</a>";

if($area_name!="������� ������")
    echo " <a href='index.php?page={$delete_return_page}{$cat_sort_cond}{$name_cond}'><img src='img/small_cancel.png'></a>";


echo "
<div id='PopUpWindow' class='popup_block'>
                <div id='popupheader'></div>
                <div id='popupcontent'>";
                echo "<span>����� �������</span>
                <div class='select_cities_list'>";

                
echo "<div class='city_select'>";

echo "<div id='id_region_id_cont'><select id='id_region_id' onchange=\"LoadCities2('id_region_id');\">";

$regions_gateway=new regions();
$regions=$regions_gateway->getAllRecs("sort","ASC");


echo "<option value='0' selected>��� ������</option>";
    while($region=mysql_fetch_array($regions))    
    {
        echo "<option value='{$region['id']}'>{$region['name']}</option>";
    }
echo "</select></div>";

echo "<div id='id_city_id_cont'><select id='id_city_id' onchange=\"CheckCity('id_city_id');\" disabled>";
//echo "<option value='0' selected></option>";
echo "</select></div>";

echo "<div><button id='select_city_but' onclick=\"SelectRegionButFunc('{$id_tag}','{$select_func}');\" type='button'>�������</button></div>";


echo "<div class='clr'></div></div>";


echo "
<div class='clr'></div>
</div>"; 


  
  









 echo "</div>";
 echo "</div>";
}




function echoPhoneInAdmin($counter,$phone_code,$phone_num,$phone_name,$phone_otdel,$id)
{
echo "<div class='phone'>
            <input type='text' name='ph_code{$counter}' class='good_text' size='6' value='{$phone_code}' title='������� ��� ���������� ��� ���������� ���������.'> 
            <input class='good_text' name='ph_number{$counter}' type='text' size='10' value='{$phone_num}' title='������� ����� ��������. ��������: 456 43 67'> 
            ��� <input type='text' name='ph_name{$counter}' class='good_text' size='9' value='{$phone_name}' title='�� ������ ������� ��� �������������� ����������. ��������: ������'> 
            ����� <input type='text' name='ph_otdel{$counter}' class='good_text' size='13' value='{$phone_otdel}' title='�� ������ ������� �������� ������ ��� ���������. ��������: ������������ ����� ��� ���������.'>";
            echo "<input type='hidden' value='{$id}' name='ph_id{$counter}'>";
echo "</div>";
}




function OnViewAdminIDQuery($value)
{
$messages_gateway=new messages();
$query=$messages_gateway->getRecFieldsById($value,array("sender_user_id","user_view","read"));

if($query['sender_user_id']==-1)
{
    return "";
}
else
{

if($query['read']==0)
$b="<font class='red'>�� ���������</font>";
else
$b="���������";

if($query['user_view']==0)
    $add_str="<b>������� �������������</b>";

   return "{$b}
   <br>
   <a href='index.php?page=advice&user_id={$query['sender_user_id']}&query_id={$value}'>&rarr; ��������</a><br>
   <a href='index.php?page=history&user_id={$query['sender_user_id']}'>&rarr; ���������</a><br>
   {$add_str}";
}
}


function OnViewCat($value)
{
    if(isNum($value))
    {
    $categories_gateway=new categories();
    $cat=$categories_gateway->getRecFieldsById($value,array("name"));
    return $cat['name']." (".$value.")";
    }

}


function OnViewSiteCat($value)
{
    if(isNum($value))
    {
    $user_categories_gateway=new user_categories();
    $cat=$user_categories_gateway->getRecFieldsById($value,array("name"));
    return substr(hsc($cat['name']),0,30)." (".$value.")";
    }

}


function OnViewPrice($value)
{
    return getPriceFormat($value,"");
}





function getParam($str)
{
    if(isset($_GET[$str]))
    {
        return mysql_real_escape_string(stripslashes(strip_tags(hsc(trim($_GET[$str])))));
    }
    if(isset($_POST[$str]))
    {
        return mysql_real_escape_string(stripslashes(strip_tags(hsc(trim($_POST[$str])))));
    }
    return false;
    
    
}

function Param($label)
{
    $params_gateway=new params();
    if(is_array($label))
        {$rec=$params_gateway->getRecFieldsById(1,$label);
        return $rec;}
    else
        {$rec=$params_gateway->getRecFieldsById(1,array($label));
        return $rec[$label];}
}


function getAdvices($user_id,$num)
{
    $users_gateway=new users();
    $advices=array();
    
    if(count($advices)<$num)
    {
        $user_info=$users_gateway->getRecFieldsById($user_id,array("goods_num","base_info_set","verification"));
        if(!$user_info['base_info_set'])
            $advices[]="��� ���������� ������� <a href='index.php?page=set_main_info'>�������� ���������� � ����� ��������</a>";        
    }
    
    if(count($advices)<$num)
    {
        if(!$user_info['goods_num'])
            $advices[]="��� ���������� <a href='index.php?page=set_verific'>�������� ������ ��� ������</a>";        
    }
    
    if(count($advices)<$num)
    {
        if(!$user_info['verification'])
            $advices[]="��� ���������� <a href='index.php?page=set_verific'>������ �����������</a>";        
    }
    
    return $advices;
}




function ToHex($text)
{
    
$text = str_replace(chr(208),chr(208).chr(160),$text); # �
$text = str_replace(chr(192),chr(208).chr(144),$text); # �
$text = str_replace(chr(193),chr(208).chr(145),$text); # �
$text = str_replace(chr(194),chr(208).chr(146),$text); # �
$text = str_replace(chr(195),chr(208).chr(147),$text); # �
$text = str_replace(chr(196),chr(208).chr(148),$text); # �
$text = str_replace(chr(197),chr(208).chr(149),$text); # �
$text = str_replace(chr(168),chr(208).chr(129),$text); # �
$text = str_replace(chr(198),chr(208).chr(150),$text); # �
$text = str_replace(chr(199),chr(208).chr(151),$text); # �
$text = str_replace(chr(200),chr(208).chr(152),$text); # �
$text = str_replace(chr(201),chr(208).chr(153),$text); # �
$text = str_replace(chr(202),chr(208).chr(154),$text); # �
$text = str_replace(chr(203),chr(208).chr(155),$text); # �
$text = str_replace(chr(204),chr(208).chr(156),$text); # �
$text = str_replace(chr(205),chr(208).chr(157),$text); # �
$text = str_replace(chr(206),chr(208).chr(158),$text); # �
$text = str_replace(chr(207),chr(208).chr(159),$text); # �
$text = str_replace(chr(209),chr(208).chr(161),$text); # �
$text = str_replace(chr(210),chr(208).chr(162),$text); # �
$text = str_replace(chr(211),chr(208).chr(163),$text); # �
$text = str_replace(chr(212),chr(208).chr(164),$text); # �
$text = str_replace(chr(213),chr(208).chr(165),$text); # �
$text = str_replace(chr(214),chr(208).chr(166),$text); # �
$text = str_replace(chr(215),chr(208).chr(167),$text); # �
$text = str_replace(chr(216),chr(208).chr(168),$text); # �
$text = str_replace(chr(217),chr(208).chr(169),$text); # �
$text = str_replace(chr(218),chr(208).chr(170),$text); # �
$text = str_replace(chr(219),chr(208).chr(171),$text); # �
$text = str_replace(chr(220),chr(208).chr(172),$text); # �
$text = str_replace(chr(221),chr(208).chr(173),$text); # �
$text = str_replace(chr(222),chr(208).chr(174),$text); # �
$text = str_replace(chr(223),chr(208).chr(175),$text); # �
$text = str_replace(chr(224),chr(208).chr(176),$text); # �
$text = str_replace(chr(225),chr(208).chr(177),$text); # �
$text = str_replace(chr(226),chr(208).chr(178),$text); # �
$text = str_replace(chr(227),chr(208).chr(179),$text); # �
$text = str_replace(chr(228),chr(208).chr(180),$text); # �
$text = str_replace(chr(229),chr(208).chr(181),$text); # �
$text = str_replace(chr(184),chr(209).chr(145),$text); # �
$text = str_replace(chr(230),chr(208).chr(182),$text); # �
$text = str_replace(chr(231),chr(208).chr(183),$text); # �
$text = str_replace(chr(232),chr(208).chr(184),$text); # �
$text = str_replace(chr(233),chr(208).chr(185),$text); # �
$text = str_replace(chr(234),chr(208).chr(186),$text); # �
$text = str_replace(chr(235),chr(208).chr(187),$text); # �
$text = str_replace(chr(236),chr(208).chr(188),$text); # �
$text = str_replace(chr(237),chr(208).chr(189),$text); # �
$text = str_replace(chr(238),chr(208).chr(190),$text); # �
$text = str_replace(chr(239),chr(208).chr(191),$text); # �
$text = str_replace(chr(240),chr(209).chr(128),$text); # �
$text = str_replace(chr(241),chr(209).chr(129),$text); # �
$text = str_replace(chr(242),chr(209).chr(130),$text); # �
$text = str_replace(chr(243),chr(209).chr(131),$text); # �
$text = str_replace(chr(244),chr(209).chr(132),$text); # �
$text = str_replace(chr(245),chr(209).chr(133),$text); # �
$text = str_replace(chr(246),chr(209).chr(134),$text); # �
$text = str_replace(chr(247),chr(209).chr(135),$text); # �
$text = str_replace(chr(248),chr(209).chr(136),$text); # �
$text = str_replace(chr(249),chr(209).chr(137),$text); # �
$text = str_replace(chr(250),chr(209).chr(138),$text); # �
$text = str_replace(chr(251),chr(209).chr(139),$text); # �
$text = str_replace(chr(252),chr(209).chr(140),$text); # �
$text = str_replace(chr(253),chr(209).chr(141),$text); # �
$text = str_replace(chr(254),chr(209).chr(142),$text); # �
$text = str_replace(chr(255),chr(209).chr(143),$text); # �
    
    

return $text;
}
                
function getMinHourFormat($time)
{
if($time==0)
    return "00";
else
    return $time;
}


  
function echoYandexMap($id,$coords,$zoom,$width,$height,$is_draggable,$name,$disc,$tag_name_id=false,$tag_disc_id=false,$mini_map=true)
{     
        global $yandex_maps_lib;
        if(!$yandex_maps_lib){
            echo " <script src=\"http://api-maps.yandex.ru/2.0/?load=package.standard&mode=debug&lang=ru-RU\" type=\"text/javascript\"></script>";
            $yandex_maps_lib = true;
        }
            
       echo "
        <div id='YMapsID{$id}' style='width:{$width}px;height:{$height}px'></div>
        
        <!--
            <input type='hidden' size='30' name='{$id}' id='id_{$id}' value='{$coords};{$zoom}'>
        -->
        
        <input type='hidden' size='30' name='xy_{$id}' id='id_xy_{$id}' value='{$coords}'>
        <input type='hidden' size='30' name='zoom_{$id}' id='id_zoom_{$id}' value='{$zoom}'>

        <script type='text/javascript'>
                
               var myMap{$id}=0;
               var myPlacemark{$id} = 0;
                ymaps.ready(function () { 
                   
                   myMap{$id} = new ymaps.Map('YMapsID{$id}', {
                            // ����� �����
                            center: [{$coords}],
                            // ����������� ���������������
                            zoom: {$zoom}
                            // ��� �����
                            //type: 'yandex#satellite'
                        }
                    );
                    
                    // ���������� ������������ ������ ������
                    myMap{$id}.controls.add('mapTools')
                        // ���������� ������ ��������� �������� 
                        .add('zoomControl')
                        // ���������� ������ ����� �����
                        .add('typeSelector');
                    
                    
                    
                    myPlacemark{$id} = new ymaps.Placemark(
                            // ���������� �����
                            [{$coords}], {
                                iconContent: '{$name}',
                                // - ������� ������ �����
                                balloonContent: '{$disc}'
                            }, {";
                                if($is_draggable)
                                    echo "draggable: true,";
                                echo "
                                hideIconOnBalloonOpen: false
                            }
                        );

                        
                    // ���������� ����� �� �����
                    myMap{$id}.geoObjects.add(myPlacemark{$id});
                    
                    myPlacemark{$id}.events.add('dragend',
                        function(e) {
                                        $('#id_xy_{$id}').val(myPlacemark{$id}.geometry.getCoordinates());
                                        $('#id_zoom_{$id}').val(myMap{$id}.getZoom());
                                        
                                    }
                    );       
                   myMap{$id}.events.add('boundschange',
                        function(e) {
                                        //$('#id_{$id}').val(myPlacemark{$id}.geometry.getCoordinates()+';'+myMap{$id}.getZoom());
                                        $('#id_xy_{$id}').val(myPlacemark{$id}.geometry.getCoordinates());
                                        $('#id_zoom_{$id}').val(myMap{$id}.getZoom());
                                    }
                    ); 
                });
                    
    function MapGoTo{$id}(name,flying_flag)
    {
        ymaps.geocode(name).then(function (res) {
        myMap{$id}.panTo(
                                // ���������� ������ ������ �����
                                res.geoObjects.get(0).geometry.getCoordinates(), 
                                {
                                    flying: flying_flag
                                }
                        );

            myPlacemark{$id}.geometry.setCoordinates(res.geoObjects.get(0).geometry.getCoordinates());
        }); 
    }              
    </script>";
}







function echoYandexMap_old($id,$coords,$zoom,$width,$height,$is_draggable,$name,$disc,$tag_name_id=false,$tag_disc_id=false,$mini_map=true)
{
    
    global $yandex_apikey;
    
    
    
    echo "<script src='http://api-maps.yandex.ru/1.1/index.xml?key={$yandex_apikey}'
    type='text/javascript'></script>

    
    <div id='YMapsID{$id}' style='width:{$width}px;height:{$height}px'></div>
    <input type='hidden' size='30' name='xy_{$id}' id='id_xy_{$id}' value='0'>
    <input type='hidden' size='30' name='zoom_{$id}' id='id_zoom_{$id}' value='0'>
    
    
    
    
<script type='text/javascript'>
    var map{$id};
    var placemark{$id};
      
            
map{$id} = new YMaps.Map(document.getElementById('YMapsID{$id}'));
map{$id}.setCenter(new YMaps.GeoPoint({$coords}), {$zoom});

placemark{$id} = new YMaps.Placemark(new YMaps.GeoPoint({$coords})";
    if($is_draggable)
        echo ", {draggable: true}";
echo ");";

if($name)
    echo "placemark{$id}.name = '{$name}';
    placemark{$id}.description = '{$disc}';";
else
    echo "placemark{$id}.name = \$('#{$tag_name_id}').val();
    placemark{$id}.description = \$('#{$tag_disc_id}').val();";
    
    
echo 
"map{$id}.addOverlay(placemark{$id}); 

map{$id}.addControl(new YMaps.ToolBar());
map{$id}.addControl(new YMaps.Zoom());";

if($mini_map)
    echo "map{$id}.addControl(new YMaps.MiniMap());";

echo "map{$id}.addControl(new YMaps.ScaleLine());



YMaps.Events.observe(placemark{$id}, placemark{$id}.Events.PositionChange, function (placemark) {
  $('#id_xy_{$id}').val(placemark{$id}.getCoordPoint());
  $('#id_zoom_{$id}').val(map{$id}.getZoom());
  
  alert($('#id_xy_{$id}').val());
  
});

YMaps.Events.observe(map{$id}, map{$id}.Events.Update, function (map) {
  $('#id_zoom_{$id}').val(map{$id}.getZoom());
  $('#id_xy_{$id}').val(placemark{$id}.getCoordPoint());
  
  alert($('#id_xy_{$id}').val());
  
});
   
function MapGoTo{$id}(name,flying_flag)
{
    
    
    //alert(placemark.getCoordPoint());
    //alert(map.getZoom());
     //placemark.setCoordPoint(new YMaps.GeoPoint(37.95,55.53));
    //map.setCenter(new YMaps.GeoPoint(37.95,55.53), map.getZoom());
    
    
    var geocoder = new YMaps.Geocoder(name, { prefLang : 'ru' } );
 
    YMaps.Events.observe(geocoder, geocoder.Events.Load, function (geocoder) {    
        placemark{$id}.setCoordPoint(geocoder.get(0).getGeoPoint());
        map{$id}.panTo(geocoder.get(0).getGeoPoint(),{flying:flying_flag});
    });
    
   
    
    
}        
        
    </script>";

}



function getName($shop_name,$name) {
    if(trim($shop_name)!=""){
        return $shop_name;
    }
    else{   
        return $name;
    }
}



function getPic($base_path,$pic_field,$size,$no_pic="")
{
if($pic_field=="NOT_SET"||!$pic_field)
{
    if($no_pic){
        return "img/no_pic_".$no_pic.".jpg"; 
    }
    else{
        if($size==0)
            return "img/no_pic.jpg";
        else if($size==3)
            return "img/no_pic_extrasmall.jpg";
        else if($size==1)
            return "img/no_pic_medium.jpg";
        else
            return "img/no_pic_small.jpg";
    }
}
else
{
    $ar=explode(";",$pic_field);
    if(file_exists(getDocumentRoot()."/".$base_path.$ar[$size]))
        return $base_path.$ar[$size];
    else
        {
        if($size==0)
            return "img/no_pic.jpg";
        else if($size==3)
            return "img/no_pic_extrasmall.jpg";
        else
            return "img/no_pic_small.jpg";
        }
}



} 

function getPriceFormat($good_price,$good_currency,$good_discount=0)
{
    if($good_price==0)
        return "����������";
    elseif($good_price==-1)
        return "�� �������";
    elseif($good_price==-2)
        return "���������";
    else
    {
        if($good_discount)
        {
            $good_price = ceil($good_price - $good_price * $good_discount/100.0);   
        }
               
        return "<span title='�������� ����'>" . $good_price . "</span> ".$good_currency;
    }    
}




function getFormattedPhoneNum($phone_code,$phone_number,$name="",$otdel="")
{
    $name_str="";
    if($name){
        $name_str=" - {$name}";    
    }
    $otdel_str="";
    if($otdel){
        $otdel_str=" ({$otdel})";    
    }
    return (string)$phone_code." {$phone_number}{$name_str}{$otdel_str}";
}

function getUserPhoneNumbers($user_id,$ar_users_phones)
{
    $ar_res=array();
    foreach ($ar_users_phones as $value)
        if($value['user_id']==$user_id)
            $ar_res[]=$value['number'];
            
    return $ar_res;
}



function OnViewVerificPhoto($value)
{    
    return "<a href='..images/verified/{$value}'>{$value}</a>";    
}

function NowDate2()
{
    return date("d/m/Y");
}


function getPhotosForGroup($ids)
{

if(count($ids))
{
    $photos_gateway=new photos();
    $photos=$photos_gateway->getMainPhotosForGroup($ids);
}
    
$goods_photos=array();
if($photos)
{
while($photo=mysql_fetch_array($photos))
    $goods_photos[$photo['good_id']]=$photo['path'];
}
return  $goods_photos;

}

//////////////////////////////////////////////////MAIN FUNC///////////////////////////////////

function checkbox_verify($_name)
{
$result=0;// ����������� �����������, ����� ������� ������ ���������� ��������� 
// ���������, � ���� �� ������ ����� checkbox �� html �����, � �� ����� �������������
if (isset($_REQUEST[$_name]))
{ if ($_REQUEST[$_name]=='on') { $result=1; } else { $result=0; }
}
return $result;
}

function getPhotoDir($root_path,$label)
{
    $photos_folder_gateway=new photos_folder();
    $folder_obj=$photos_folder_gateway->getRecFieldsByField("label",trim($label),array("cur_folder","max_files"));
    if($folder_obj)
    {
        $cur_folder_name=$folder_obj['cur_folder'];
        $max_files=$folder_obj['max_files'];
        
        
        if(!$cur_folder_name)
        {
        //���������� ��� ���, ������� ��:
        $cur_folder_name=1;
        mkdir(getDocumentRoot()."/".$root_path.$cur_folder_name,0777);
        $photos_folder_gateway->UpdateFieldsByField("label",$label,array("cur_folder"),array($cur_folder_name));
        }
        else
        {
            if(file_exists(getDocumentRoot()."/".$root_path.$cur_folder_name))
            {
            //��������� ������� ������ � �����
             $num=getFilesNumInDir(getDocumentRoot()."/".$root_path.$cur_folder_name);
             if($num>$max_files)
             {
                 while(file_exists(getDocumentRoot()."/".$root_path.$cur_folder_name))
                    $cur_folder_name++;
                    
                 mkdir(getDocumentRoot()."/".$root_path.$cur_folder_name,0777);
                 $photos_folder_gateway->UpdateFieldsByField("label",$label,array("cur_folder"),array($cur_folder_name));
             }
            }
            else
                mkdir(getDocumentRoot()."/".$root_path.$cur_folder_name,0777);
        }
        
        
        return $cur_folder_name;
        
        
    }
    else
       return "";
}


function getFilesNumInDir($dir)
{
if ($handle = opendir($dir)) {
    $num=0;
    while (false !== ($entry = readdir($handle)))
        if ($entry != "." && $entry != "..")
            $num++;
            
            
    closedir($handle);
    return $num;
}
return false;
}


function DateSmallerOrEqual($date1,$date2)
{
list($year1,$month1,$day1)=explode("-",$date1);
list($year2,$month2,$day2)=explode("-",$date2);
    
    $date1_=mktime(0,0,0,$month1,$day1,$year1);
    $date2_=mktime(0,0,0,$month2,$day2,$year2);
    
    if($date1_<=$date2_)
    {return true;}
    else
    {return false;}
}



function stringBeforeDBInputWithStripTags($str)
{
return strip_tags($str);
}

function stringBeforeDBInputWithStripTags2($str)
{
    return strip_tags($str);
}


function stringBefore($str)
{
return mysql_real_escape_string(strip_tags($str));
}




function OnViewFuncAdminStatus($value)
{
    global $admin_status;
    return $admin_status[$value];
}


function getPhpFile($file)
{
$ar=split("[.]",$file);
return $ar[0].".php";
}

function setCOOKIEmy($name,$value)
{
    $time=mktime(date("H"), date("i"), date("s"), date("m")+12, date("j"), date("Y"));
    setcookie($name, $value, $time);
}
function getCOOKIEmy($name)
{
     return $_COOKIE[$name];
}
function deleteCOOKIEmy($name)
{
    setcookie($name);
}


  
                                
                            


function getSearchParam($param_name)
{
if(isset($_POST[$param_name]))
    {
        return StringBeforeDB($_POST[$param_name],true);
    }
    else if(isset($_GET[$param_name]))
    {return StringBeforeDB($_GET[$param_name],true);}
   else
    {return false;}
}


function getSearchMultiParamFromPost($post_base,$table_src)
{
$result="";
      $gateway=new $table_src();
      $recs=$gateway->getAllRecFields(array("id"));
      while($rec=mysql_fetch_array($recs))
      {
      if($_POST[$post_base.$rec['id']])
      {
          if($result!="")
            {$result.=";";}
          $result.=$rec['id'];
      }
      }
return $result;
}


function getSearchMultiParam($from_search,$post_base,$table_src,$get_param_name)
{
    if($from_search)
    {return getSearchMultiParamFromPost($post_base,$table_src);}
    else
    {
    if(isset($_GET[$get_param_name]))
    {
     return StringBeforeDB($_GET[$param_name],true);
    }
    else
    {return false;}
    }
}











function getLang()
{
$lang=false;
if(isset($_GET['lang'])&&($_GET['lang']=="en"||$_GET['lang']=="rus"))
{
    //� ������
    setSessionLang($_GET['lang']);
    //� �����
    setCOOKIELang($_GET['lang']);
    //
    $lang=$_GET['lang'];
}
else if(getSessionLang()&&(getSessionLang()=="en"||getSessionLang()=="rus"))
{
//�� ������
    $lang=getSessionLang();
}
else if(getCOOKIELang())//���� ����������� ����� �� ��������� �� ���
{
//� ������
    setSessionLang(getCOOKIELang());
    $lang=getCOOKIELang();
}
else
{
    $def_lang="rus";
    //� ������
    setSessionLang($def_lang);
    //� �����
    setCOOKIELang($def_lang);
    //
    $lang=$def_lang;
}
return $lang;
}        
 









    


function StringBeforeDB($text,$strip_tags=true)
{
    if($strip_tags)
    {$text1=strip_tags($text);}
    else
    {$text1=$text;}
return mysql_real_escape_string(stripslashes(trim($text1)));
}


function printVioletButton($text,$type="submit",$onclick="")
{
    
    echo "<p style='padding:10px 0;'><input class='button button5' type='{$type}' onclick=\"{$onclick}\" value='".hsc($text)."'/></p>";
}





function IsAfterDateSec($date,$sec)
{
    $ar=explode(" ",$date);
    $date1=explode("-",$ar[0]);
    $time1=explode(":",$ar[1]);
    
    if(mktime()-mktime($time1[0], $time1[1], $time1[2], $date1[1], $date1[2], $date1[0])>$sec)
        {return true;}
    else
        {return false;}

}




function StripTagsAndNL($text)
{
$text=hsc(strip_tags($text));
$new_text=str_replace("\n","<br>",$text);
return $new_text;
}


function NL($text)
{
$new_text=hsc(str_replace("\n","<br>",$text));
return $new_text;
}

function NL2($text)
{
$new_text=str_replace("\n","<br>",$text);
return $new_text;
}


function stringBeforeDBInput($str)
{
return mysql_real_escape_string(stripslashes($str));
}


function GetIp()
{
 if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
 {
   $ip=$_SERVER['HTTP_CLIENT_IP'];
 }
 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
 {
  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
 }
 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
 {
  $ip=$_SERVER['HTTP_X_Real_IP'];
 }
 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
 {
  $ip=$_SERVER['HTTP_X_Real_IP'];
 }
 else
 {
   $ip=$_SERVER['REMOTE_ADDR'];
 }
 
 
 return $ip;
}









function PrintSearchFieldInForm($table_gateway,$s_field,$s_string,$s_field_sign,$counter)
{
            echo "
            <div style='padding-bottom:8px;'>
            <font>�����:</font>
                <input type='text' name='s_string{$counter}'  size='30' class='good_text' value='{$s_string}'/>&nbsp;&nbsp;&nbsp;
            <font>�� ����:</font>
            <select name='s_field{$counter}' class='good_text'>";
            
                
            $rus_fields=$table_gateway->getFieldsRus();
            $fields=$table_gateway->getFields();
             $fields_types=$table_gateway->getFieldsTypes(); 
            
             
            for($i=0;$i<count($rus_fields);$i++)
            {
                if($rus_fields[$i]!=""&&$fields_types[$i]->IsInListing()&&$fields_types[$i]->IsInSearch())
                {
                    echo "<option value='".$fields[$i]."'";
                    
                    if($fields[$i]==$s_field)
                    {
                        echo "selected";
                    }
                    
                    echo ">".$rus_fields[$i]."</option>";
                }
            }
                
            echo "</select>&nbsp;&nbsp;&nbsp;
            
            <font>���������:</font>
            <select name='s_field_sign{$counter}' class='good_text'>
                <option value='0'";
                if($s_field_sign===null||$s_field_sign==0) echo "selected";
                echo ">������ ������������</option>
                <option value='1'";
                if($s_field_sign==1) echo "selected";
                echo ">�������� ������������</option>
                <option value='2'";
                if($s_field_sign==2) echo "selected";
                echo ">�� �����</option>
            </select>
            
            </div>"; 
}

class SearchField
{
public $_s_field;
public $_s_string;
public $_s_sign;

function __construct($field,$string,$sign)
{
    $this->_s_field=$field;
    $this->_s_string=$string;
    $this->_s_sign=$sign;
}

}

  








function Print2Page($beg,$end,$page_num,$params)
{
    
for($i=$beg;$i<=$end;$i++)
{
if($i==$page_num)
{
    
    
                  
                                    
    echo " ".$i." ";
}
else
{
    
    $put=str_replace("%param%",$i,$params);
    echo " <a {$put}>".$i."</a> ";
}
}
    
}


function PrintPagesList2($page_all_number,$page_num,$params)
{

    if($page_all_number>10)
    {
$max_printed;
if($page_num<=3||$page_num>=$page_all_number-3)
{

    Print2Page(1,4,$page_num,$params);
    echo "&nbsp;<font>...</font>&nbsp;";
    Print2Page($page_all_number-4,$page_all_number,$page_num,$params);

}
else
{
    Print2Page(1,3,$page_num,$params);
    if($page_num>5&&$page_num<$page_all_number-5)
    {
    echo "&nbsp;<font>...</font>&nbsp;";
    Print2Page($page_num-1,$page_num+1,$page_num,$params);
    echo "&nbsp;<font>...</font>&nbsp;";
    }
    else
    {
        if($page_num<=5)
        {
        Print2Page(4,$page_num+1,$page_num,$params);
        echo "&nbsp;<font>...</font>&nbsp;";
        }
    }
    
    
    
    
    
    if($page_num>=$page_all_number-5)
    {
        echo "&nbsp;<font>...</font>&nbsp;";
        Print2Page($page_num-1,$page_all_number-4,$page_num,$params);
    }
    
    
    
    Print2Page($page_all_number-3,$page_all_number,$page_num,$params);
}




    }
    else
    {
    
        Print2Page(1,$page_all_number,$page_num,$params);
    
    }
    
    

}


function PrintPagesBlockAjax($page_num,$goods_num,$recs_per_page,$params)
 {

$page_all_number=getPageNumber($goods_num,$recs_per_page);
    
if($page_all_number!=0)
{
//echo "<div style='float:right; padding-top:5px; padding-bottom:5px;'><font>�������� ��������: ".$page_num." �� ".$page_all_number."</font></div>";

//echo "<div class='pagination'>";
         
if($page_num!=1)                     
{   $put=str_replace("%param%",$page_num-1,$params);
    echo "<a {$put} class='prev'><< ����������</a>";}

PrintPagesList2($page_all_number,$page_num,$params);


if($page_num!=$page_all_number)                     
{   $put=str_replace("%param%",$page_num+1,$params);
    echo "<a {$put} class='next'>��������� >></a>";}



//echo "</div>";
}

}


function printButtonLink($path,$value)
{
echo "<input type='button' value='{$value}' class='mysubmit' onclick=\"GoTo('{$path}');\">";
}

function printButton($onclick,$value)
{
echo "<input type='button' value='{$value}' class='mysubmit' onclick=\"{$onclick}\">";
}



function EchoAdminPic()
{
echo "<img src='users_pic/admin_small.jpg'>";
}

function getUserLang($time_zone)
{
  if($time_zone>=2&&$time_zone<=12)
  {return "rus";}
  else
  {return "en";}
}



function getDateFormated($date1)
{
global $monthes_array;

if(getPermission()&&getUserTZ()!=0)
{$delta_hours=getUserTZ();

$ar1=explode(" ",$date1);
$ar2=explode("-",$ar1[0]);
$ar3=explode(":",$ar1[1]);

$date=date("Y-m-j H:i:s", mktime($ar3[0]+$delta_hours, $ar3[1], $ar3[2], $ar2[1], $ar2[2], $ar2[0]));;
}
else
{$delta_hours=0;
$date=$date1;}





$ar1=explode(" ",$date);
$ar2=explode("-",$ar1[0]);
$ar3=explode(":",$ar1[1]);

if($ar2[1]<10)
{$month=substr($ar2[1],1,1);}
else
{$month=$ar2[1];}

return "{$ar2[2]} {$monthes_array[$month]} {$ar2[0]}, {$ar3[0]}:{$ar3[1]}";
}

function PasteInImg($img_path,$string_to_paste)
{
$ar=split("[.]",$img_path);

return $ar[0]."{$string_to_paste}.".$ar[1];
}


function getDateOnlyFormated($date)
{
global $monthes_array;


$ar2=explode("-",$date);


if($ar2[1]<10)
{$month=substr($ar2[1],1,1);}
else
{$month=$ar2[1];}

return "{$ar2[2]} {$monthes_array[$month]} {$ar2[0]}";
}



function echoSubmit($text,$params)
{
    echo "<input type='submit' {$params} class='mysubmit' value='{$text}'>";
}







function printUserPhoto($user_id,$photo_id=null)
{
        ///////////////////////////������ ����///////////////////////////////
global $l_pf_nophoto;

        
$photos_gateway=new photos();
if($photo_id)
    {$photo=$photos_gateway->getRecById($photo_id);}
else
    {$photo=$photos_gateway->getUserFirstPhoto($user_id);}

    
if($photo)
{
$photo_next=$photos_gateway->getNextPhoto($photo_id,$photo['user_id']);
          if($photo_next===false)
          {
            $photo_next=$photos_gateway->getFirstPhoto($photo['user_id']);
            //echo "PREV: {$photo_next['id']}";
          }
          
          
          $photo_prev=$photos_gateway->getPrevPhoto($photo_id,$photo['user_id']);
          if($photo_prev===false)
          {
            $photo_prev=$photos_gateway->getLastPhoto($photo['user_id']);
            //echo "NEXT: {$photo_next['id']}";
          }

          
          echo "<div class='navLeft'><a href='#' onclick='return ChangePhoto({$photo_prev['id']});'></a></div>
            <div class='fotoImg'>
                <span class='fotoImgContainer'>
                <a onclick='return ChangePhoto({$photo_next['id']});' href='#'>
                    <img src='users_pic/{$photo['path']}' alt=''/>
                </a>
                </span>
            </div>
            <div class='navRight'><a href='#' onclick='return ChangePhoto({$photo_next['id']});'></a></div>
            <div class='clr'></div>
            <div class='fotoDesc'>";
             if($photo['disc']!="")
                {$disc=StripTagsAndNL($photo['disc']);}
             echo $disc;
            echo "</div>";
}
else
{
echo $l_pf_nophoto;
}
}





function PrintImgChanged($img,$img_changed,$alt)
{
    echo "<img alt='{$alt}' src='{$img}' onmouseover=\"this.src='{$img_changed}';\" onmouseout=\"this.src='{$img}';\">";
}

function FormatDateForDB($str_date)
{
$sep="[/.-]";
list ($day, $month, $year)=split ($sep,$str_date);
if(strlen($day)==4&&strlen($month)==2&&strlen($year)==2)
{return $str_date;}
else
{
    return $year."-".$month."-".$day;
}
}


function OnAttack($idsphpresult)
{
echo "ONEWAYSHOW.COM - ������� ������! ������� �������������������� ������� � �����. ������������� ����� ���� ���������!";
                            
                            flush();
                            MailToMeMagzone('dmitriy_invest@mail.ru','����� ONEWAY',
                            '����� �� ONEWAY. �����:'.date("G").':'.date("i").' ����:'.date("d").'/'.date("m").'/'.date("Y").' '.GetRealIp().' ���������: '.$idsphpresult." \n��������: ".$_GET['page'],
                            'info@onewayshow.com','������� ������ ONEWAY',array());                            
}


function FormatDateTimeForDB($str_date)
{
list ($date, $time)=split(" ",$str_date);
$sep="[/.-]";
list ($day, $month, $year)=split($sep,$date);
return $year."-".$month."-".$day." ".$time;
}




function FormatDateForView($str_date)
{
$sep="[/.-]";
list ($year, $month, $day)=split ($sep,$str_date);
return $day."/".$month."/".$year;
}

function FormatDateForView2($str_date)
{
$sep="-";
list ($year, $month, $day)=explode ($sep,$str_date);
return $day.".".$month.".".$year;
}


function FormatDateTimeForView($str_date)
{
list ($date, $time)=split(" ",$str_date);
$sep="[/.-]";
list ($year, $month, $day)=split($sep,$date);


list ($h, $m,$s)=split(":",$time);

return $day."/".$month."/".$year." ".$h.":".$m;
}

function FormatDateTimeForView2($str_date)
{
list ($date, $time)=explode(" ",$str_date);
$sep="-";
list ($year, $month, $day)=explode($sep,$date);


list ($h, $m,$s)=explode(":",$time);

return $day.".".$month.".".$year." ".$h.":".$m;
}

function GetDateTimeForEnded()
{
return date("d.m.Y");
}




class DataBaseSingleton
{
    
    static $_db=null;
    static $_db_name=DB_NAME;
    
    static function GetDB()
    {
        if(DataBaseSingleton::$_db)
        {return DataBaseSingleton::$_db;}
        else
        {DataBaseSingleton::$_db=new DataBase();
        return DataBaseSingleton::$_db;}
    }
    
    
    static function SetDBName($db_name)
    {
    if(DataBaseSingleton::$_db_name!=$db_name)
    {
      DataBaseSingleton::$_db_name=$db_name;
      DataBaseSingleton::$_db->SelectDB($db_name);
    }
    }
    
    
    
   

}



 







function PrintPage($beg,$end,$page_num,$href)
{
    
for($i=$beg;$i<=$end;$i++)
{
if($i==$page_num)
{
    echo "<font>".$i."&nbsp;</font>";
}
else
{
    echo "<a href='{$href}&page_num=".$i."'>".$i."</a>&nbsp;";
}
}
    
}


function PrintPages($page_all_number,$page_num,$href)
{

    if($page_all_number>10)
    {
$max_printed;
if($page_num<=3||$page_num>=$page_all_number-3)
{

    PrintPage(1,4,$page_num,$href);
    echo "&nbsp;<font>...</font>&nbsp;";
    PrintPage($page_all_number-4,$page_all_number,$page_num,$href);

}
else
{
    PrintPage(1,3,$page_num,$href);
    if($page_num>5&&$page_num<$page_all_number-5)
    {
    echo "&nbsp;<font>...</font>&nbsp;";
    PrintPage($page_num-1,$page_num+1,$page_num,$href);
    echo "&nbsp;<font>...</font>&nbsp;";
    }
    else
    {
        if($page_num<=5)
        {
        PrintPage(4,$page_num+1,$page_num,$href);
        echo "&nbsp;<font>...</font>&nbsp;";
        }
    }
    
    
    
    
    
    if($page_num>=$page_all_number-5)
    {
        echo "&nbsp;<font>...</font>&nbsp;";
        PrintPage($page_num-1,$page_all_number-4,$page_num,$href);
    }
    
    
    
    PrintPage($page_all_number-3,$page_all_number,$page_num,$href);
}




    }
    else
    {
    
        PrintPage(1,$page_all_number,$page_num,$href);
    
    }
    
    

}

 
 


function getPageNumber($recs_number,$recs_per_page)
{
if((int)($recs_number/$recs_per_page)==$recs_number/$recs_per_page)
{
    $page_number=(int)($recs_number/$recs_per_page);
}
else
{
$page_number=(int)($recs_number/$recs_per_page)+1;
}
return $page_number;
}


function IsLoginCorrect($login)
{
$chars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
"0","1","2","3","4","5","6","7","8","9",
"_","-");

$arr1 = str_split($login);
$wrong=false;
for($i=0;$i<count($arr1);$i++)
{
if(!in_array($arr1[$i],$chars))
{
$wrong=true;
break;
}
}

return $wrong;

}


function DatePlusDay($start_date,$delta_day)
{
    list($year,$month,$day)=explode("-",$start_date);
    return date("Y-m-d",mktime(0, 0,0, $month, $day+$delta_day, $year));
}



function echoImgLoader($params,$path="")
{
     echo "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' width='660' height='595' id='imgloader'>
                <param name='movie' value='{$path}imgloader/imgloader.swf' />
                <param name='quality' value='high' />
                <param name='bgcolor' value='#ffffff' />
                <param name='allowScriptAccess' value='sameDomain' />
                <param name='allowFullScreen' value='true' />
                <param name=FlashVars value=\"{$params}\" />
                <!--[if !IE]>-->
                <object type='application/x-shockwave-flash' data='{$path}imgloader/imgloader.swf' width='660' height='595'>
                    <param name='quality' value='high' />
                    <param name='bgcolor' value='#ffffff' />
                    <param name='allowScriptAccess' value='sameDomain' />
                    <param name='allowFullScreen' value='true' />
                    <param name=FlashVars value=\"{$params}\" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                    <p> 
                        Either scripts and active content are not permitted to run or Adobe Flash Player version
                        10.2.0 or greater is not installed.
                    </p>
                <!--<![endif]-->
                    <a href='http://www.adobe.com/go/getflashplayer'>
                        <img src='http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash Player' />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>";


}


function printCombo($name,$table,$key,$field,$selected_key,$params="",$first_rec=null)
{
global $db;
$result=$db->ExecuteQuery("SELECT ".$key.",".$field." FROM ".$table." ORDER BY ".$key." ASC");

	echo "<select id='id_".$name."' name='".$name."' class='good_text' {$params}>";
    
    if($first_rec)
        {
            echo "<option value='".$first_rec[0]."' ";
            if($first_rec[0]==$selected_key)
                echo "selected";
            echo ">".$first_rec[1]."</option>";
        
        }
	
while($rec=mysql_fetch_array($result))
{	
if($selected_key==$rec[$key])
    echo "<option value='".$rec[$key]."' selected>".$rec[$field]."</option>";
else
    echo "<option value='".$rec[$key]."'>".$rec[$field]."</option>";
}				
echo "</select>";
}


function printComboExt($name,$table,$key,$field,$selected_key,$onchange)
{
global $db;
$result=$db->ExecuteQuery("SELECT ".$key.",".$field." FROM ".$table);

    echo "<select id='id_".$name."' name='".$name."' class='good_text' onchange='".$onchange."'>";
    
while($rec=mysql_fetch_array($result))
{    
if($selected_key==$rec[$key])
{
            echo "<option value='".$rec[$key]."' selected>".$rec[$field]."</option>";
}
else
        {
            echo "<option value='".$rec[$key]."'>".$rec[$field]."</option>";
        }
}                
    echo "</select>";
}



function PrintComboArrayExt($name,$selected,$ar,$onchange)
{
echo "<select id='id_".$name."' name='".$name."' class='good_text' onchange=\"".$onchange."\">";
    
    for($i=0;$i<count($ar);$i++)
    {
        if($i==$selected)
        {
            echo "<option value='".$i."' selected>".$ar[$i]."</option>";
        }
        else
        {
            echo "<option value='".$i."'>".$ar[$i]."</option>";
        }
    }
    echo "</select>";
}



function PrintComboArray($name,$selected,$ar,$params="",$first_rec=null)
{
echo "<select id='id_".$name."' name='".$name."' class='good_text' {$params}>";
	
    if($first_rec)
        {
            echo "<option value='".$first_rec[0]."' ";
            if($first_rec[0]==$selected_key)
                echo "selected";
            echo ">".$first_rec[1]."</option>";
        
        }
    
    while (list ($key, $val) = each ($ar) )
    {
        if($key==$selected)
        {
            echo "<option value='".$key."' selected>".$val."</option>";
        }
        else
        {
            echo "<option value='".$key."'>".$val."</option>";
        }
    }
	echo "</select>";
}


function PrintComboArrayValue($name,$selected,$ar,$val)
{
echo "<select id='id_".$name."' name='".$name."' class='good_text'>";
    
    for($i=0;$i<count($ar);$i++)
    {
        if($i==$selected)
        {
            echo "<option value='".$val[$i]."' selected>".$ar[$i]."</option>";
        }
        else
        {
            echo "<option value='".$val[$i]."'>".$ar[$i]."</option>";
        }
    }
    echo "</select>";
}





function printComboYesNo($name,$selected)
{
global $yesnoarray;
PrintComboArray($name,$selected,$yesnoarray);
}

function LeaveFirst($str,$count)
{
if(strlen($str)<=$count)
{
return $str;
}

return substr($str,0,$count);

}




    

 function ParsePost(&$_post)
{
foreach($_post as $key=>$value)
{
        $_post[$key]=strip_tags($value);
}
}

  



function FormParams($table_gateway,$_post)
{
//������ ������������ $params
global $_FILES;
$fields=$table_gateway->getFields();

for($i=0;$i<count($fields);$i++)
{
if($fields[$i]!=$table_gateway->getKey())
{
if(isset($_post[$table_gateway->getName()."_".$fields[$i]]))
{
if($_post[$table_gateway->getName()."_".$fields[$i]]=="files_".$table_gateway->getName()."_".$fields[$i])
    {
    //��� �����������/����, ������ ������� �����������
    $params[]="files_".$table_gateway->getName()."_".$fields[$i];
    }
    else
    {
    //��� ������� ����!
    $params[]=$_post[$table_gateway->getName()."_".$fields[$i]];
    }
}
else
{
    if($_FILES[$table_gateway->getName()."_".$fields[$i]]['name'])
    {
    $params[]="value_".$table_gateway->getName()."_".$fields[$i];
    }
    else
    {$params[]="NOT_SET";}
}
}
else
{$params[]="NULL";}
}

//����� ������ ������������ $params
return $params;
}



   


function EchoTextLimit($text,$name,$value,$size,$limit)
{


echo "
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='right' valign='top'>";
if($text!="")
{
echo "<font>".$text."</font>&nbsp;&nbsp;";
}
echo "</td>
<td align='left' valign='top'>
<input type='text'  id='id_".$name."' value='".$value."' name='".$name."' size='".$size."' class='good_text' onkeyup='OnChange_id_".$name."();'>&nbsp;&nbsp;
</td>";



echo "
<td align='left' valign='top'>
<div id='".$name."_length'>

</div>    
    </td>
  </tr>
</table>";

echo "
<script language='Javascript'>
function OnChange_id_".$name."()
{
if(document.getElementById('id_".$name."').value.length>".$limit.")
{
	document.getElementById('id_".$name."').value=document.getElementById('id_".$name."').value.substring(0,".$limit.");
}

var sym=".$limit."-document.getElementById('id_".$name."').value.length;
document.getElementById('".$name."_length').innerHTML='<font>�������� ��������: '+sym+'</font>';

}
var sym1=".$limit."-document.getElementById('id_".$name."').value.length;
document.getElementById('".$name."_length').innerHTML='<font>�������� ��������: '+sym1+'</font>';
</script>
";

}





function EchoTextAreaLimit($text,$name,$value,$cols,$rows,$limit)
{


echo "
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='right' valign='top'>";
if($text!="")
{
echo "<font>".$text."</font>&nbsp;&nbsp;";
}
echo "</td>
<td align='left' valign='top'>
<textarea  id='id_".$name."' cols='".$cols."' rows='".$rows."' name='".$name."' class='good_text' onkeyup='OnChange_id_".$name."();'>".$value."</textarea>&nbsp;&nbsp;
</td>";



echo "
<td align='left' valign='top'>
<div id='".$name."_length'>

</div>    
    </td>
  </tr>
</table>";

echo "
<script language='Javascript'>
function OnChange_id_".$name."()
{
if(document.getElementById('id_".$name."').value.length>".$limit.")
{
    document.getElementById('id_".$name."').value=document.getElementById('id_".$name."').value.substring(0,".$limit.");
}

var sym=".$limit."-document.getElementById('id_".$name."').value.length;
document.getElementById('".$name."_length').innerHTML='<font>�������� ��������: '+sym+'</font>';

}
var sym1=".$limit."-document.getElementById('id_".$name."').value.length;
document.getElementById('".$name."_length').innerHTML='<font>�������� ��������: '+sym1+'</font>';
</script>
";

}




function AddLimitToTextArea($textarea_id,$left_symb_id,$limit)
{

echo "
<script language='Javascript'>

document.getElementById('{$textarea_id}').onkeyup=function(){

if(document.getElementById('".$textarea_id."').value.length>".$limit.")
{
    document.getElementById('".$textarea_id."').value=document.getElementById('".$textarea_id."').value.substring(0,".$limit.");
}

var sym=".$limit."-document.getElementById('".$textarea_id."').value.length;
document.getElementById('".$left_symb_id."').innerHTML=sym;

}

var sym1=".$limit."-document.getElementById('".$textarea_id."').value.length;
document.getElementById('".$left_symb_id."').innerHTML=sym1;

</script>";

}







function EchoTextLimitExt($text,$name,$value,$size,$limit,$params)
{


echo "
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='right' valign='top'>";
if($text!="")
{
echo "<font>".$text."</font>&nbsp;&nbsp;";
}
echo "</td>
<td align='left' valign='top'>
<input type='text'  id='id_".$name."' value='".$value."' name='".$name."' size='".$size."' class='good_text' onkeyup='OnChange_id_".$name."();' ".$params.">&nbsp;&nbsp;
</td>";



echo "
<td align='left' valign='top'>
<div id='".$name."_length'>

</div>    
    </td>
  </tr>
</table>";

echo "
<script language='Javascript'>
function OnChange_id_".$name."()
{
if(document.getElementById('id_".$name."').value.length>".$limit.")
{
    document.getElementById('id_".$name."').value=document.getElementById('id_".$name."').value.substring(0,".$limit.");
}

var sym=".$limit."-document.getElementById('id_".$name."').value.length;
document.getElementById('".$name."_length').innerHTML='<font>�������� ��������: '+sym+'</font>';

}
var sym1=".$limit."-document.getElementById('id_".$name."').value.length;
document.getElementById('".$name."_length').innerHTML='<font>�������� ��������: '+sym1+'</font>';
</script>
";

}



    

    
function printTextLineBig($name,$value,$size)
{
    echo "<input type='text' name='".$name."' id='id_".$name."' size='".$size."' value='".$value."' class='good_text_big'/>";
}


    
function printTextLine($name,$value,$size,$params="")
{
	echo "<input type='text' name='".$name."' id='id_".$name."' size='".$size."' value=\"{$value}\" class='good_text' {$params}>";
}


function printTextLineExt($name,$value,$size,$onkeyup)
{
    echo "<input type='text' name='".$name."' id='id_".$name."' size='".$size."' value='".$value."' class='good_text' onkeyup='".$onkeyup."'/>";
}



function printPassLine($name,$value,$size,$params="")
{
    echo "<input type='password' name='".$name."' id='id_".$name."' size='".$size."' value='".$value."' class='good_text' {$params}>";
}




function printHidden($name,$value,$size)
{
    echo "<input type='hidden' name='".$name."' id='id_".$name."' size='".$size."' value='".$value."' class='good_text'/>";
}

 
   




function printTextLineDisabled($name,$value,$size)
{
	echo "<input type='text' name='".$name."' id='id_".$name."' size='".$size."' value='".$value."' class='good_text'/ disabled>";
}

function printTextLineClassParams($name,$value,$size,$class,$params)
{
    echo "<input type='text' name='".$name."' id='id_".$name."' size='".$size."' value='".$value."' class='{$class}' {$params}>";
}




function printText($name,$value,$rows,$cols)
{
	echo "<textarea name='".$name."' id='id_".$name."' rows='".$rows."' cols='".$cols."' class='good_text'>".$value."</textarea>";
}

function printTextExt($name,$value,$rows,$cols,$onkeyup)
{
    echo "<textarea name='".$name."' id='id_".$name."' onkeyup=\"".$onkeyup."\" rows='".$rows."' cols='".$cols."' class='good_text'>".$value."</textarea>";
}

function printTextExtParams($name,$value,$rows,$cols,$params)
{
    echo "<textarea name='".$name."' id='id_".$name."' {$params} rows='".$rows."' cols='".$cols."' class='good_text'>".$value."</textarea>";
}







function printTextClass($name,$value,$rows,$cols)
{
    echo "<textarea name='".$name."' id='id_".$name."' rows='".$rows."' cols='".$cols."'>".$value."</textarea>";
}



function printStdText($name,$value)
{
	echo "<textarea name='".$name."' id='id_".$name."' rows='7' cols='30' class='good_text'>".$value."</textarea>";
}



function fEcho($text)
{
echo "<font>".$text."</font>";
}



//����
/*function EchoDate($id,$name,$value)
{
global $used_calendar;

$value=FormatDateForView2($value);
      
      
echo "
<span class='inputbox'>
<input  class='good_text' name='".$name."' id='".$id."' value='".$value."' size='11'></span>
<img src='images/calendar.gif' onclick=\"gfPop.fPopCalendar(document.getElementById('".$id."'));\">";


if(!$used_calendar)
{
echo "
<iframe width=174 height=189 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src=\"calendar/calendar_src.htm\" scrolling=\"no\" frameborder=\"0\" style=\"visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;\">
</iframe>";
$used_calendar=true;
}
} */

function EchoDate($id,$name,$value)
{
global $used_calendar;
$value=FormatDateForView($value);
echo "
<input  class='good_text' name='".$name."' id='".$id."' value='".$value."' size='11'>&nbsp;&nbsp;<input type='button' onclick=\"gfPop.fPopCalendar(document.getElementById('".$id."'));\" value='...' class='good_text'>";

if(!$used_calendar)
{
echo "
<iframe width=174 height=189 name='gToday:normal:agenda.js' id='gToday:normal:agenda.js' src=\"calendar/calendar_src.htm\" scrolling=\"no\" frameborder=\"0\" style=\"visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;\">
</iframe>";
$used_calendar=true;
}
}




//���� � �����
function EchoDateTime($text,$id,$name,$valuedate,$valuetime)
{
global $used_calendar;

$valuedate=FormatDateForView($valuedate);
echo "<script language='Javascript'>
function OnSubmit_".$id."()
{
document.getElementById('".$id."').value=document.getElementById('temp_".$id."').value+\" \"+document.getElementById('temp2_".$id."').value;
}
</script>";

echo "<font>".$text."</font> <input  class='good_text' type='text' name='temp_".$name."' id='temp_".$id."' value='".$valuedate."' size='11'>&nbsp;&nbsp;<input type='button' onclick=\"gfPop.fPopCalendar(document.getElementById('temp_".$id."'));\" value='...' class='good_text'>
&nbsp;&nbsp;
<font>�����</font>: <input type='text'  class='good_text' name='temp2_".$name."' id='temp2_".$id."' value='".$valuetime."' size='11'>

<input type='hidden' name='".$name."' id='".$id."' value='".$valuedate." ".$valuetime."' size='30'>";

if(!$used_calendar)
{
echo "<iframe width=174 height=189 name=\"gToday:normal:agenda.js\" id=\"gToday:normal:agenda.js\" src=\"calendar/calendar_src.htm\" scrolling=\"no\" frameborder=\"0\" style=\"visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;\">
</iframe>";
$used_calendar=true;
}
}


function EchoDateTime2($text,$id,$name,$valuedate,$valuetime)
{
global $used_calendar;

$valuedate=FormatDateForView($valuedate);
echo "<script language='Javascript'>
function OnSubmit_".$id."()
{
document.getElementById('".$id."').value=document.getElementById('temp_".$id."').value+\" \"+document.getElementById('temp2_".$id."').value;
}
</script>";

echo "<div style='padding-bottom:4px;'><font>".$text."</font> <input  class='good_text' type='text' name='temp_".$name."' id='temp_".$id."' value='".$valuedate."' size='11'>&nbsp;&nbsp;<input type='button' onclick=\"gfPop.fPopCalendar(document.getElementById('temp_".$id."'));\" value='...' class='good_text'>
</div>
<div style='padding-bottom:0px;'>
<font>�����</font>: <input type='text'  class='good_text' name='temp2_".$name."' id='temp2_".$id."' value='".$valuetime."' size='11'>

<input type='hidden' name='".$name."' id='".$id."' value='".$valuedate." ".$valuetime."' size='30'>
</div>
";


if(!$used_calendar)
{
echo "<iframe width=174 height=189 name=\"gToday:normal:agenda.js\" id=\"gToday:normal:agenda.js\" src=\"calendar/calendar_src.htm\" scrolling=\"no\" frameborder=\"0\" style=\"visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;\">
</iframe>";
$used_calendar=true;
}
}


function ProcessImageOnly($field_name,&$params,$shop_login,$table)
{
$index=array_search("value_".$table->getName()."_".$field_name, $params);
    
    if($_FILES[$table->getName()."_".$field_name]['name'])
            {
                  //��, ����������� ���������
                  $ext=getImgExt($_FILES[$table->getName()."_".$field_name]['type']);
                  if($ext==false)
                  {
                  $table->setError("�������� ��� �����! ����� ����������� ������ ����� ���������� .jpg, .png, .gif");
                  return false;
                  }
                  
                  if(file_exists("../shops/".$shop_login."/".$field_name.".".$ext))
                  {
                    unlink ("../shops/".$shop_login."/".$field_name.".".$ext);
                  }
                  
                  move_uploaded_file($_FILES[$table->getName()."_".$field_name]['tmp_name'], "../shops/".$shop_login."/".$field_name.".".$ext);
                  
                  $params[$index]="shops/".$shop_login."/".$field_name.".".$ext;
                  
                  
            }
            else
            {$params[$index]="NOT_SET";}
    return true;   

}


function getDefSort($tab_name)
{
 switch($tab_name)
 {
 
 case 'testimonials':
    return array("status","ASC");
 
 
 default:
    return array("id","DESC");
 
 }

}


function ScaleImage($filename,$temp_path,$param,$path_result,$ext)
{
$w = $param; // ������������ ������ (������) ��������� �����
$q = 100; // �������� jpeg �����������

$file = $filename; // ���������� ���������� �������
                                               // $_FILES � $file
switch($ext)
{
case "jpg":
$src = imagecreatefromjpeg($temp_path); // ������� ���������
break;
case "png":
$src = imagecreatefrompng($temp_path); // ������� ���������
break;
case "gif":
$src = imagecreatefromgif($temp_path); // ������� ���������
break;
}


                                                           
$w_src = imagesx($src); // ���������� ��� �������� ������ �� ����������� (������)
$h_src = imagesy($src); // ���������� ��� �������� ������ �� ��������� (������)

$dir = $path_result; // �����, ���� ����� �������
                                                 // ��� ��������������� ����

$path = $dir.$file; // ������ ���� � ����� (������� � ��� ���)

if (($w_src > $w)||($h_src > $w))  // ��������� �� ����� �� ���
                         //�������� ������ ����������� ���
  {
   if ($w_src > $h_src) // ���� ����������� ��������������
     {
      $ratio = $w_src/$w; // ������� ����������� ���������
      $w_dest = $w; // �������� ������ ����� ����� ������������
      $h_dest = round($h_src/$ratio); // ������� �������� ������
     }
   elseif ($h_src > $w_src) // ���� ����������� ������������
     {
      $ratio = $h_src/$w; // ������� ����������� ���������
      $h_dest = $w; // �������� ������ ����� ����� ������������
      $w_dest = round($w_src/$ratio); // ������� �������� ������
     }
   else // ���� ����������� ����������
     {
      $w_dest = $w; // ����������� ������������ ��������
      $h_dest = $w;
     }
   $dest = imagecreatetruecolor($w_dest,$h_dest); // ������� ������ �����������
   imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
   // ����������� �������� ����������� � �������� (� ������ ���������).
   // ������� ���������� ����������, ������� ����������� ����� �������
   // ��������, ��� ���� ������������ ������ imagecopyresampled �������
   // imagecopyresized
  }
else
  {
      
      switch($ext)
{
case "jpg":
 $dest = imagecreatefromjpeg($temp_path);   
break;
case "png":
 $dest = imagecreatefrompng($temp_path);   
break;
case "gif":
 $dest = imagecreatefromgif($temp_path);   
break;
}

      
      
      
  
   
   // ���� �������� ������ ����� ������ ��� �� ������, �� ������
   // ������� ��������� ���� ��� ��������������
  }
 
   
     switch($ext)
{
case "jpg":
   $res = imagejpeg($dest,$path,$q); // �������� ���� � �����
break;
case "png":
   $res = imagepng($dest,$path,9); // �������� ���� � �����
break;
case "gif":
   $res = imagegif($dest,$path,$q); // �������� ���� � �����
break;
}
   
   
   
   
   if (!$res)
     {
     
     
      imagedestroy($dest); // ����������� ������
   imagedestroy($src);
   
      return false;
     }
     
    imagedestroy($dest);
   imagedestroy($src);
   
    
   return true;
}


function watermark( $main_img_obj, $watermark_img_obj, $alpha_level = 100) {
    

    
 $watermark_width = imagesx($watermark_img_obj);
 $watermark_height = imagesy($watermark_img_obj);
 $dest_x = imagesx($main_img_obj) - $watermark_width - 10; // ������
 $dest_y = imagesy($main_img_obj) - $watermark_height - 10; // ������ ���� �����������
 //imagecopymerge($main_img_obj, $watermark_img_obj, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, $alpha_level);
  imagecopy($main_img_obj, $watermark_img_obj, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
 return $main_img_obj;
 }


function ScaleImageExt($filename,$temp_path,$w_param,$h_param,$path_result,$ext,$watermark=false)
{
ini_set("gd.jpeg_ignore_warning", 1);
$q = 100; // �������� jpeg �����������

$file = $filename; // ���������� ���������� �������
                                               // $_FILES � $file
switch($ext)
{
case "jpg":
$src = imagecreatefromjpeg($temp_path); // ������� ���������
break;
case "png":
$src = imagecreatefrompng($temp_path); // ������� ���������
break;
case "gif":
$src = imagecreatefromgif($temp_path); // ������� ���������
break;
}


                                                           
$w_src = imagesx($src); // ���������� ��� �������� ������ �� ����������� (������)
$h_src = imagesy($src); // ���������� ��� �������� ������ �� ��������� (������)

/*if((float)$w_src/(float)$h_src<(float)$w_param/(float)$h_param)
    {$w = $h_param; }
    else
    {$w = $w_param;}*/
    
    $w = $w_param;



$dir = $path_result; // �����, ���� ����� �������
                                                 // ��� ��������������� ����

$path = $dir.$file; // ������ ���� � ����� (������� � ��� ���)

if (($w_src > $w_param)||($h_src > $h_param))  // ��������� �� ����� �� ���
                         //�������� ������ ����������� ���
  {
   if ((float)$w_src/(float)$h_src>(float)$w_param/(float)$h_param)
     {
      /*$ratio = $w_src/$w; 
      $w_dest = $w; 
      $h_dest = round($h_src/$ratio); */
      
      $w=$w_param;
        $ratio = $w_src/$w; 
      $w_dest = $w; 
       $h_dest= round($h_src/$ratio); 
      
     }
   elseif ((float)$w_src/(float)$h_src<(float)$w_param/(float)$h_param) 
     {
         
         $w=$h_param;
      $ratio = $h_src/$w; 
      $h_dest = $w; 
      $w_dest = round($w_src/$ratio); 
         
      
     }
   else // ���� ����������� ����������
     {
      $w_dest = $w; // ����������� ������������ ��������
      $h_dest = $w;
     }
   $dest = imagecreatetruecolor($w_dest,$h_dest); // ������� ������ �����������
   imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
   // ����������� �������� ����������� � �������� (� ������ ���������).
   // ������� ���������� ����������, ������� ����������� ����� �������
   // ��������, ��� ���� ������������ ������ imagecopyresampled �������
   // imagecopyresized
   
  // $texts_gateway=new texts();
  // $texts_gateway->Add(array("NULL","2","222"));
  
   
   
   
  }
else
  {
      
      switch($ext)
    {
    case "jpg":
     $dest = imagecreatefromjpeg($temp_path);   
    break;
    case "png":
     $dest = imagecreatefrompng($temp_path);   
    break;
    case "gif":
     $dest = imagecreatefromgif($temp_path);   
    break;
    }

      
      
   
   // ���� �������� ������ ����� ������ ��� �� ������, �� ������
   // ������� ��������� ���� ��� ��������������
  }
 
 
 
  if($watermark)
   {
       //getDocumentRoot().
     $water = imagecreatefrompng("../img/watermark.png");
     $dest=watermark( $dest, $water, 70);
   }
 
 
 
 
 
 
 
   
     switch($ext)
{
case "jpg":
   $res = imagejpeg($dest,$path,$q); // �������� ���� � �����
break;
case "png":
   $res = imagepng($dest,$path,9); // �������� ���� � �����
break;
case "gif":
   $res = imagegif($dest,$path,$q); // �������� ���� � �����
break;
}
   
   
   
   
   if (!$res)
     {
     
     
      imagedestroy($dest); // ����������� ������
   imagedestroy($src);
   
      return false;
     }
     
    imagedestroy($dest);
   imagedestroy($src);
   
    
   return true;
}





function ProcessImage($field_name,&$params,$shop_login,$table)
{

$index=array_search("files_".$table->getName()."_".$field_name, $params);
    if(!($index===false))
    {
    //shop_bg_color ��� �����������
        if($_FILES["img_".$table->getName()."_".$field_name]['name'])
            {
                  //��, ����������� ���������
                  $ext=getImgExt($_FILES["img_".$table->getName()."_".$field_name]['type']);
                  if($ext==false)
                  {
                      $table->setError("�������� ��� �����! ����� ����������� ������ ����� ���������� .jpg, .png, .gif");
                      return false;
                  }
                  
                  if(file_exists("../shops/".$shop_login."/".$field_name.".".$ext))
                  {
                    unlink ("../shops/".$shop_login."/".$field_name.".".$ext);
                  }
                  
                  move_uploaded_file($_FILES["img_".$table->getName()."_".$field_name]['tmp_name'], "../shops/".$shop_login."/".$field_name.".".$ext);
                  
                  $params[$index]="shops/".$shop_login."/".$field_name.".".$ext;
                  
                  
            }
            else
            {$params[$index]="NOT_SET";}
    }
    
    
    return true;


}



function InsertAtags($text1)
{
$text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)', 
   '<noindex><a href="\\1" rel="nofollow">\\1</a></noindex>', $text1); 
$text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)', 
   '<noindex>\\1<a rel="nofollow" href="http://\\2">\\2</a></noindex>', $text); 
$text = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})', 
   '<a href="mailto:\\1">\\1</a>', $text);
 return $text;
}




function ProcessSimpleImage($field_name,$old_field_value,&$params,$shop_login,$table,$root,$root_dest)
{
$index=array_search($field_name, $table->getFields());
    //shop_bg_color ��� �����������
    
        if($_FILES[$table->getName()."_".$field_name]['name'])
            {
                  //��, ����������� ���������
                  
                  
                  $ext=getImgExt($_FILES[$table->getName()."_".$field_name]['type']);
                  if($ext==false)
                  {
                      $table->setError("�������� ��� �����! ����� ����������� ������ ����� ���������� .jpg, .png, .gif");
                      return false;
                  }
                  /*$ar=array("gif","jpg","png");
                  for($i=0;$i<count($ar);$i++)
                  { */
                      if(file_exists($old_field_value))
                      {
                        unlink ($old_field_value);
                      }
                  //}
                  $new_name=tempname($ext);
                  move_uploaded_file($_FILES[$table->getName()."_".$field_name]['tmp_name'], $root.$shop_login."/".$new_name);
                  
                  $params[$index]=$root_dest.$shop_login."/".$new_name;
                  
                  
            }
            else
            {$params[$index]="NOT_SET";}
    
    
    
    return true;


}




function getImgExt($imgtype)
{
if(strstr($imgtype,"image"))
    {
        if($imgtype=="image/jpeg")
        {       
            return "jpg";
        }
        if($imgtype=="image/gif")
        {       
            return "gif";
        }
        if($imgtype=="image/png")
        {       
            return "png";
        }
        return "jpg";
    }
    else
    {
       return false;
    } 

}


function removeDirRec($dir)
{
    if ($objs = glob($dir."/*")) {
        foreach($objs as $obj) {
            is_dir($obj) ? removeDirRec($obj) : unlink($obj);
        }
    }
    rmdir($dir);
}



//���� ��� �����������
function ColorOrImg($name,$valueType,$value)
{

echo "<table cellpadding='3' cellspacing='0' border='0'>
<tr>
<td  valign='top' align='left'>
";
if($valueType=="img")
{echo "<input type='radio' onchange=\"Set_Img_Color".$name."('img');\" id='id_radio_img".$name."' name='radio_".$name."' value='img' checked><font>�����������: </font>";}
else
{echo "<input type='radio' onchange=\"Set_Img_Color".$name."('img');\" id='id_radio_img".$name."' name='radio_".$name."' value='img'><font>�����������: </font>";}

echo "</td><td valign='top' align='left'>";

if($valueType=="img")
{
    EchoFile("","img_".$name,20);
}
else
{   
    EchoFile("","img_".$name,20);
    echo "<script>document.getElementById('id_img_".$name."').disabled = true;</script>";
}

echo "</td>
</tr>

<tr>
<td  valign='top' align='left'>
";




if($valueType=="color")
{echo "<input type='radio' onchange=\"Set_Img_Color".$name."('color');\" id='id_radio_color".$name."' name='radio_".$name."' value='color' checked><font>����: </font>";}
else
{echo "<input type='radio' onchange=\"Set_Img_Color".$name."('color');\" id='id_radio_color".$name."' name='radio_".$name."' value='color'><font>����: </font>";}

echo "</td>
<td valign='top' align='left'>";


if($valueType=="color")
{EchoColorExt("","id_color".$name,"color".$name,$value,"Set_Img_Color".$name."('color');");}
else
{
EchoColorExt("","id_color".$name,"color".$name,"","Set_Img_Color".$name."('color');");
echo "<script>document.getElementById('id_color".$name."').disabled = true;</script>";
}

echo "</td></tr></table>";

if($valueType=="color")
{echo "<input type='hidden' name='".$name."' id='id_".$name."' size='20' value='".$value."' class='good_text'/>";}
else
{echo "<input type='hidden' name='".$name."' id='id_".$name."' size='20' value='files_".$name."' class='good_text'/>";}


echo "<script language='Javascript'>


function Set_Img_Color".$name."(value)
{
if(value=='img')
{
document.getElementById('id_color".$name."').disabled = true; 
document.getElementById('id_img_".$name."').disabled = false;
document.getElementById('id_radio_img".$name."').checked=true;
}
else
{
document.getElementById('id_color".$name."').disabled = false; 
document.getElementById('id_img_".$name."').disabled = true;
document.getElementById('id_radio_color".$name."').checked=true;
}

}

function OnSubmit_id_".$name."()
{
var value;


if(document.getElementById('id_radio_img".$name."').checked)
{value=document.getElementById('id_img_".$name."').value;
document.getElementById('id_".$name."').value=\"files_".$name."\";
}
else
{value=document.getElementById('id_color".$name."').value;
document.getElementById('id_".$name."').value=value;
}

}

</script>";
}







//����� �����
function EchoColor($text,$id,$name,$value)
{
echo "
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='right' valign='top'>
<font>".$text."</font>
</td>

<td align='left' valign='top'>
<div style='padding-left:3px; padding-right:3px;'>
<div id='preview_color".$name."' style='border:1px solid silver; background-color:".$value."; width:20px; height: 20px;'>&nbsp;&nbsp;</div>
</div>
</td>

<td align='left' valign='top'>
<input type='text'  id='".$id."' value='".$value."' name='".$name."' onkeyup='OnKeyUp".$name."();' size='10' class='good_text'>&nbsp;&nbsp;
</td>";


echo "
<td align='left' valign='top'>
<div id='colors_".$id."'>
        <div onclick=\"return collapseBox('colors_".$id."', this, 0.65, 0.30)\" >
            <div>
				<a href='' onclick='return false;'>�������� ����</a>
            </div>
        </div>
        <div class='c' id='c".$id."' >     
           
		   <script language='Javascript'>
function FormatColor".$name."(color)
{
document.getElementById('".$id."').value=color;
document.getElementById('preview_color".$name."').style.backgroundColor =color;
var div1=document.getElementById('c".$id."');
div1.style.display='none';
}

function OnKeyUp".$name."()
{
document.getElementById('preview_color".$name."').style.backgroundColor =document.getElementById('".$id."').value;
}


</script>

<table border='1' width='180' height='50'>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#FF9999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF9999')\"></td>
    <td width='12%' bgcolor='#FFFF99'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FFFF99')\"></td>
    <td width='12%' bgcolor='#99FF99'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#99FF99')\"></td>
    <td width='12%' bgcolor='#00FF99'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FF99')\"></td>
    <td width='13%' bgcolor='#99FFFF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#99FFFF')\"></td>
    <td width='13%' bgcolor='#0099FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#0099FF')\"></td>
    <td width='13%' bgcolor='#FF99CC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF99CC')\"></td>
    <td width='13%' bgcolor='#FF99FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF99FF')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#FF0000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF0000')\"></td>
    <td width='12%' bgcolor='#FFFF00'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FFFF00')\"></td>
    <td width='12%' bgcolor='#99FF00'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#99FF00')\"></td>
    <td width='12%' bgcolor='#00FF33'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FF33')\"></td>
    <td width='13%' bgcolor='#00FFFF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FFFF')\"></td>
    <td width='13%' bgcolor='#0099CC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#0099CC')\"></td>
    <td width='13%' bgcolor='#9999CC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#9999CC')\"></td>
    <td width='13%' bgcolor='#FF00FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF00FF')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#993333'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#993333')\"></td>
    <td width='12%' bgcolor='#FF9933'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF9933')\"></td>
    <td width='12%' bgcolor='#00FF00'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FF00')\"></td>
    <td width='12%' bgcolor='#009999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#009999')\"></td>
    <td width='13%' bgcolor='#003399'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#003399')\"></td>
    <td width='13%' bgcolor='#9999FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#9999FF')\"></td>
    <td width='13%' bgcolor='#990033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#990033')\"></td>
    <td width='13%' bgcolor='#FF0099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF0099')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#990000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#990000')\"></td>
    <td width='12%' bgcolor='#FF9900'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF9900')\"></td>
    <td width='12%' bgcolor='#009900'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#009900')\"></td>
    <td width='12%' bgcolor='#009933'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#009933')\"></td>
    <td width='13%' bgcolor='#0000FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#0000FF')\"></td>
    <td width='13%' bgcolor='#000099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000099')\"></td>
    <td width='13%' bgcolor='#990099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#990099')\"></td>
    <td width='13%' bgcolor='#9900FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#9900FF')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#330000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330000')\"></td>
    <td width='12%' bgcolor='#993300'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#993300')\"></td>
    <td width='12%' bgcolor='#003300'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#003300')\"></td>
    <td width='12%' bgcolor='#003333'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#003333')\"></td>
    <td width='13%' bgcolor='#000099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000099')\"></td>
    <td width='13%' bgcolor='#000033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000033')\"></td>
    <td width='13%' bgcolor='#330033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330033')\"></td>
    <td width='13%' bgcolor='#330099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330099')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#000000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000000')\"></td>
    <td width='12%' bgcolor='#999900'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#999900')\"></td>
    <td width='12%' bgcolor='#999933'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#999933')\"></td>
    <td width='12%' bgcolor='#999999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#999999')\"></td>
    <td width='13%' bgcolor='#339999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#339999')\"></td>
    <td width='13%' bgcolor='#CCCCCC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#CCCCCC')\"></td>
    <td width='13%' bgcolor='#330033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330033')\"></td>
    <td width='13%' bgcolor='#FFFFFF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FFFFFF')\"></td>
  </tr>
</table>

        </div>
    </div>
    
    </td>
  </tr>
</table>
        <script language='javascript'>
var div1=document.getElementById('c".$id."');
div1.style.display='none';
</script>";

}






function EchoColorExt($text,$id,$name,$value,$onclick)
{
echo "
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='right' valign='top'>
<font>".$text."</font>&nbsp;&nbsp;
</td>

<td align='left' valign='top'>
<div style='padding-left:3px; padding-right:3px;'>
<div id='preview_color".$name."' style='border:1px solid silver; background-color:".$value."; width:20px; height: 20px;'>&nbsp;&nbsp;</div>
</div>
</td>


<td align='left' valign='top'>
<input type='text'  id='".$id."' value='".$value."' onkeyup='OnKeyUp".$name."();' name='".$name."' size='10' class='good_text'>&nbsp;&nbsp;
</td>";



echo "
<td align='left' valign='top'>
<div id='colors_".$id."'>
        <div onclick=\"return collapseBox('colors_".$id."', this, 0.65, 0.30)\" >
            <div>
				<a href='' onclick='return false;'>�������� ����</a>
            </div>
        </div>
        <div class='c' id='c".$id."' >     
           
		   <script language='Javascript'>
		   
function OnKeyUp".$name."()
{
document.getElementById('preview_color".$name."').style.backgroundColor =document.getElementById('".$id."').value;
".$onclick."
}
		   

function FormatColor".$name."(color)
{
document.getElementById('".$id."').value=color;
".$onclick."

document.getElementById('preview_color".$name."').style.backgroundColor =color;
var div1=document.getElementById('c".$id."');
div1.style.display='none';
}
</script>

<table border='1' width='180' height='50'>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#FF9999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF9999')\"></td>
    <td width='12%' bgcolor='#FFFF99'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FFFF99')\"></td>
    <td width='12%' bgcolor='#99FF99'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#99FF99')\"></td>
    <td width='12%' bgcolor='#00FF99'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FF99')\"></td>
    <td width='13%' bgcolor='#99FFFF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#99FFFF')\"></td>
    <td width='13%' bgcolor='#0099FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#0099FF')\"></td>
    <td width='13%' bgcolor='#FF99CC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF99CC')\"></td>
    <td width='13%' bgcolor='#FF99FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF99FF')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#FF0000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF0000')\"></td>
    <td width='12%' bgcolor='#FFFF00'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FFFF00')\"></td>
    <td width='12%' bgcolor='#99FF00'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#99FF00')\"></td>
    <td width='12%' bgcolor='#00FF33'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FF33')\"></td>
    <td width='13%' bgcolor='#00FFFF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FFFF')\"></td>
    <td width='13%' bgcolor='#0099CC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#0099CC')\"></td>
    <td width='13%' bgcolor='#9999CC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#9999CC')\"></td>
    <td width='13%' bgcolor='#FF00FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF00FF')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#993333'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#993333')\"></td>
    <td width='12%' bgcolor='#FF9933'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF9933')\"></td>
    <td width='12%' bgcolor='#00FF00'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#00FF00')\"></td>
    <td width='12%' bgcolor='#009999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#009999')\"></td>
    <td width='13%' bgcolor='#003399'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#003399')\"></td>
    <td width='13%' bgcolor='#9999FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#9999FF')\"></td>
    <td width='13%' bgcolor='#990033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#990033')\"></td>
    <td width='13%' bgcolor='#FF0099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF0099')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#990000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#990000')\"></td>
    <td width='12%' bgcolor='#FF9900'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FF9900')\"></td>
    <td width='12%' bgcolor='#009900'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#009900')\"></td>
    <td width='12%' bgcolor='#009933'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#009933')\"></td>
    <td width='13%' bgcolor='#0000FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#0000FF')\"></td>
    <td width='13%' bgcolor='#000099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000099')\"></td>
    <td width='13%' bgcolor='#990099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#990099')\"></td>
    <td width='13%' bgcolor='#9900FF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#9900FF')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#330000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330000')\"></td>
    <td width='12%' bgcolor='#993300'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#993300')\"></td>
    <td width='12%' bgcolor='#003300'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#003300')\"></td>
    <td width='12%' bgcolor='#003333'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#003333')\"></td>
    <td width='13%' bgcolor='#000099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000099')\"></td>
    <td width='13%' bgcolor='#000033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000033')\"></td>
    <td width='13%' bgcolor='#330033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330033')\"></td>
    <td width='13%' bgcolor='#330099'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330099')\"></td>
  </tr>
  <tr style='cursor: hand;'>
    <td width='12%' bgcolor='#000000'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#000000')\"></td>
    <td width='12%' bgcolor='#999900'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#999900')\"></td>
    <td width='12%' bgcolor='#999933'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#999933')\"></td>
    <td width='12%' bgcolor='#999999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#999999')\"></td>
    <td width='13%' bgcolor='#339999'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#339999')\"></td>
    <td width='13%' bgcolor='#CCCCCC'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#CCCCCC')\"></td>
    <td width='13%' bgcolor='#330033'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#330033')\"></td>
    <td width='13%' bgcolor='#FFFFFF'><img src='img/blank.gif' border='0'	onClick=\"FormatColor".$name."('#FFFFFF')\"></td>
  </tr>
</table>
        </div>
    </div>
    
    </td>
  </tr>
</table>
        <script language='javascript'>
var div1=document.getElementById('c".$id."');
div1.style.display='none';
</script>";

}


function Echo2Fields($name,$value,$text1,$text2)
{
printHidden($name,$value,30);

echo "<script>
function OnSubmit_id_".$name."()
{
    document.getElementById('id_".$name."').value=document.getElementById('id_value1_".$name."').value+';'+document.getElementById('id_value2_".$name."').value;
}


</script>";
 
 $values=split(";",$value);

echo "<div style='padding-bottom:5px;'><font>".$text1.":</font></div>";
echo "<div style='padding-bottom:10px;'>";
printTextLine("value1_".$name,$values[0],20);
echo "</div>";

echo "<div style='padding-bottom:5px;'><font>".$text2.":</font></div>";
echo "<div style='padding-bottom:10px;'>";
printTextLine("value2_".$name,$values[1],20);
echo "</div>";


}


function EchoVariants($name,$value)
{   
printHidden($name,$value,30);

echo "<script>
function AddValue".$name."()
{

           newtext=document.getElementById('id_new_text_".$name."').value;
           document.getElementById('id_new_text_".$name."').value='';
           
           document.getElementById('id_".$name."t').options[document.getElementById('id_".$name."t').options.length] = new Option(newtext, newtext);
      
}



  function Remove_".$name."()
        {
            document.getElementById('id_".$name."t').remove(document.getElementById('id_".$name."t').selectedIndex);
            document.getElementById('id_".$name."t').selectedIndex=0;
        }
        
          



function OnSubmit_id_".$name."()
{
   listbox=document.getElementById('id_".$name."t');
        length1=listbox.options.length;
        first=true;
        res='';
            for(i=0;i<length1;i++)
            {
            if(first)
            {
            res+=document.getElementById('id_".$name."t').options[i].value;
            first=false;
            }
            else
            {
            res+=';'+document.getElementById('id_".$name."t').options[i].value;
            }
            }
            
         //   alert(res);
        document.getElementById('id_".$name."').value=res;
}


</script>";
 echo "<div style='padding-bottom:5px;'><font>������� ����� ��������:</font></div>";
 echo "<div style='padding-bottom:10px;'>";
       printTextLine("new_text_".$name,"",20);
 echo "&nbsp;&nbsp;";
 echo "<input type='button' class='good_button' value='�������� ��������' onclick='AddValue".$name."()'>";
 echo "</div>";
 
 echo "<div style='padding-bottom:5px;'><font>������ ��������:</font></div>";
 
 echo "<div style='padding-bottom:5px;'>";
 
 
 echo "<select size='8' style='width:200px' name='".$name."t' id='id_".$name."t'>";
 
 $values=split(";",$value);
    if(count($values)>0)
    {
        if(count($values)!=1||$values[0]!="")
        {
        for($i=0;$i<count($values);$i++)
        {
            echo "<option value='".$values[$i]."'>".$values[$i]."</option>";
        }
        }
    }
    echo "</select>";
 echo "</div>";
 
 echo "<div style='padding-bottom:5px;'><input type='button' class='good_button' value='������� ��������' onclick='Remove_".$name."()'></div>";
 
 
}



//����� ������
function EchoFont($name,$valuefont,$valuesize,$valuecolor)
{
echo "
<div class='adm_container'>
<table cellpadding='3' cellspacing='0' border='0'>
<tr>
<td align='left' valign='top' width='400px'>
<div style='small_container'>
<font>�����:</font>&nbsp;&nbsp;
";
$fonts=array("Arial","Courier New","Times New Roman","Verdana","Tahoma");
echo "<select class='good_text' name='temp1_".$name."' id='id_temp1".$name."' onChange='OnChange_id_".$name."();'>";
for($i=0;$i<count($fonts);$i++)
{
if($fonts[$i]==$valuefont)
	{echo "<option value='".$fonts[$i]."' selected>".$fonts[$i]."</option>";}
else
	{echo "<option value='".$fonts[$i]."'>".$fonts[$i]."</option>";}
}
echo "</select></div>";

echo "<div class='small_container'><font>������: </font>";
echo "<input type='text' name='temp2_".$name."' id='id_temp2".$name."' size='4' value='".$valuesize."' class='good_text' onkeyup='OnChange_id_".$name."();'/>";

echo "</div>";

echo "<div class='small_container'>";
EchoColorExt("���� ������: ","id_temp3".$name,"temp3_".$name,$valuecolor,"OnChange_id_".$name."();");
echo "</div>
</td>
";

echo "<td align='center' valign='top'>
<div id='font_result_id".$name."'></div>
</td></tr>
</table></div>";

echo "<input type='hidden' name='".$name."' id='id_".$name."' size='40' value='".$valuefont.";".$valuesize.";".$valuecolor."' class='good_text'/>";


echo "<script language='Javascript'>

document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� �����:</font><br><font style='color: ".$valuecolor."; font-family: ".$valuefont."; font-size: ".$valuesize."px;'>�������� �����</font>\";


function OnSubmit_id_".$name."()
{
value1=document.getElementById('id_temp1".$name."').value;
value2=document.getElementById('id_temp2".$name."').value;
value3=document.getElementById('id_temp3".$name."').value;
document.getElementById('id_".$name."').value=value1+\";\"+value2+\";\"+value3;
}



function OnChange_id_".$name."()
{
document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� �����:</font><br><font style='color: \"+document.getElementById('id_temp3".$name."').value+\"; font-family: \"+document.getElementById('id_temp1".$name."').value+\"; font-size: \"+document.getElementById('id_temp2".$name."').value+\"px;'>�������� �����</font>\";
}
</script>";
}


function EchoFont2($name,$valuefont,$valuesize,$valuecolor)
{
echo "
<div style='border:1px Solid silver;'>
<table cellpadding='3' cellspacing='0' border='0'>
<tr>
<td align='left' valign='top' width='430px'>
<div style='small_container'>
<font>�����:</font>&nbsp;&nbsp;
";
$fonts=array("Arial","Courier New","Times New Roman","Verdana","Tahoma");
echo "<select class='good_text' name='temp1_".$name."' id='id_temp1".$name."' onChange='OnChange_id_".$name."();'>";
for($i=0;$i<count($fonts);$i++)
{
if($fonts[$i]==$valuefont)
    {echo "<option value='".$fonts[$i]."' selected>".$fonts[$i]."</option>";}
else
    {echo "<option value='".$fonts[$i]."'>".$fonts[$i]."</option>";}
}
echo "</select></div>";

echo "<div class='small_container'><font>������: </font>";
echo "<input type='text' name='temp2_".$name."' id='id_temp2".$name."' size='4' value='".$valuesize."' class='good_text' onkeyup='OnChange_id_".$name."();'/>";

echo "</div>";

echo "<div class='small_container'>";
EchoColorExt("���� ������: ","id_temp3".$name,"temp3_".$name,$valuecolor,"OnChange_id_".$name."();");
echo "</div>
</td>
";

echo "<td align='center' valign='top' width='140px'>
<div id='font_result_id".$name."'></div>
</td></tr>
</table></div>";

echo "<input type='hidden' name='".$name."' id='id_".$name."' size='40' value='".$valuefont.";".$valuesize.";".$valuecolor."' class='good_text'/>";


echo "<script language='Javascript'>

document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� �����:</font><br><font style='color: ".$valuecolor."; font-family: ".$valuefont."; font-size: ".$valuesize."px;'>�������� �����</font>\";


function OnSubmit_id_".$name."()
{
 
value1=document.getElementById('id_temp1".$name."').value;
value2=document.getElementById('id_temp2".$name."').value;
value3=document.getElementById('id_temp3".$name."').value;
document.getElementById('id_".$name."').value=value1+\";\"+value2+\";\"+value3;

}



function OnChange_id_".$name."()
{
document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� �����:</font><br><font style='color: \"+document.getElementById('id_temp3".$name."').value+\"; font-family: \"+document.getElementById('id_temp1".$name."').value+\"; font-size: \"+document.getElementById('id_temp2".$name."').value+\"px;'>�������� �����</font>\";
}
</script>";
}




//���������� ��������

function EchoRedactor($text,$id,$name,$value,$path="../")
{
echo "<script type=\"text/javascript\" src=\"{$path}ckeditor/ckeditor.js\"></script>";

echo "<textarea id='{$id}' name='{$name}'>{$value}</textarea>";



echo "<script type=\"text/javascript\">
       CKEDITOR.replace( '{$id}',
    {
        toolbar :
            [
                ['Source','Bold', 'Italic','Underline', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', 'Image','-','TextColor','BGColor','-','JustifyLeft','JustifyCenter','JustifyRight'],
                ['Font','FontSize','Undo','Redo','-','Paste','-','Youtube']
            ]
    });
    </script>";

}


function EchoCalendar($name,$size,$value,$params="")
{
    echo "<script src='js/jquery.ui.datepicker-ru.js'></script>";
    printTextLine($name,$value,$size,$params);
    echo "
    <script>
     \$(document).ready(function() {
    \$('#id_{$name}').datepicker(\$.datepicker.regional['ru']);
  });
    </script>";
                                     


}


function EchoOnePhotoInLoader($photo_id,$new_photo,$path,$checked=false)
{
$photo=getPic("images/photos/",$path,2);
if($checked) $checked_str="checked";
 return "
 <div class='one_img_cont_left' id='one_img_cont{$photo_id}'>
 <div class='one_img_cont'>
        <div class='one_img'>
            <img src='{$photo}'>            
        </div>
        <a href='#' id='{$photo_id}' onclick='return RemovePhoto({$photo_id})'><img src='img/close_red.png'></a> &nbsp;<input type='radio' name='main_pic_in_loader' value='{$photo_id}' {$checked_str}>�������
        <input type='hidden' id='photos_ids{$photo_id}' name='photos_ids[]' value='{$photo_id}'>
        <input type='hidden' id='new_photo{$photo_id}' name='new_photo[]' value='{$new_photo}'>
    </div>
 </div>";
}

function EchoPickMacroCategory($cur_directory,$cont_res_name,$select_func,$admin=false)
{
    global $project_name;
echo "<script src='/js/PickMacroCategory.js' type='text/javascript'></script>";
echo "<script src='/js/modal.js' type='text/javascript'></script>";

    
    $categories_gateway=new categories();
    
    $mag_cat_id=0;
    $mag_cat_obj_name="�� �������";
    
    if($cur_directory)
        {
            $mag_cat_id=$cur_directory;                                    
            $mag_cat_obj=$categories_gateway->getRecFieldsById($mag_cat_id,array("name"));
            $mag_cat_obj_name=$mag_cat_obj['name'];
        
        }
        if(!$admin)                                              
            echo "<b><a href='#' id='{$cont_res_name}_link' onclick='return SelectMagCategory()'>{$mag_cat_obj_name}</a></b>";
        echo " <a href='#' class='prof' onclick='return SelectMagCategory()'>�������� ���������</a>";
        printHidden($cont_res_name,$mag_cat_id,20);
                                                      
    
    echo "<div id='PopUpWindowMagCategory' class='popup_block'>
            <div id='popupheader'></div>
            <div id='popupcontent'>";
    echo "<span>����� ��������� �������� {$project_name}</span>
    <div class='clr'></div><br><br>";
                                                                
    echo "<div class='city_select'>";

echo "<div><select id='id_main_cat' onchange=\"LoadCats('id_main_cat','id_sub_cat','id_result_cat');\">";
echo "<option value='-1' selected>**�������� ���������**</option>";

$categories=$categories_gateway->GetMainCats();
while($category=mysql_fetch_array($categories))
{
    echo "<option value='{$category['id']}'>{$category['name']}</option>";
} 
echo "</select></div>";

echo "<div><select id='id_sub_cat' onchange=\"LoadCats('id_sub_cat','id_result_cat','');\" disabled>";

echo "</select></div>";

echo "<div><select id='id_result_cat' onchange=\"CheckResultCat('id_result_cat');\" disabled>";
echo "<option value='0' selected></option>";
echo "</select></div>";
echo "<div><button id='select_city_but' onclick=\"SelectCatBut('{$cont_res_name}','{$select_func}');\" type='button' disabled>�������</button></div>";


echo "<div class='clr'></div>";
echo "</div>";
echo "<br><br>����� ����� ��������� ��� ������ ������ ���������� ��������� �������� {$project_name}. ��� �������� ������������� ����������� ������ �������� ��� ����� �� ������ ����� ��� ����, �� � ����� ������� ������� {$project_name}<br>";
echo "</div></div>";

}






function NowDate()
{
	return date("Y-m-d");
}


function NowTime()
{
	
	return date("H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")));
}


   



function DateSmaller($date1,$date2)
{
list($year1,$month1,$day1)=split("-",$date1);
list($year2,$month2,$day2)=split("-",$date2);
    
    $date1_=mktime(0,0,0,$month1,$day1,$year1);
    $date2_=mktime(0,0,0,$month2,$day2,$year2);
    
    if($date1_<$date2_)
    {return true;}
    else
    {return false;}
}

function NowDateMin($delta_Min)
{
    return date("Y-m-j H:i:s",mktime(date("H"),date("i")+$delta_Min,0, date("m"), date("j"), date("Y")));
}

function explodeDate($date)
{
return explode("-",$date);
}

function NowDateMonth($delta_month)
{
    return date("Y-m-j",mktime(0, 0,0, date("m")+$delta_month, date("j"), date("Y")));
}

function NowDateDay($delta_day)
{
	return date("Y-m-j",mktime(0, 0,0, date("m"), date("j")+$delta_day, date("Y")));
}


function DateDay($date,$delta_day)//date=dd-mm-YY
{
    
    return date("Y-m-j",mktime(0, 0,0, date("m"), date("j")+$delta_day, date("Y")));
}



function NowDateYear($delta_year)
{
    return date("Y-m-j",mktime(0, 0,0, date("m"), date("j"), date("Y")+$delta_year));
}


function NowDateHour($delta_hour)
{
    return date("Y-m-j H:i:s", mktime(date("H")+$delta_hour, date("i"), date("s"), date("m"), date("j"), date("Y")));
}


function DateAddDay($date,$delta_day)
{
    list($year,$month,$day)=split("-",$date);
    return date("Y-m-j",mktime(0, 0,0, $month, $day+$delta_day, $year));
}




function NowDateTime()
{
	return date("Y-m-j H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")));
}


function TimeInNHours($hours_num)
{
    return date("Y-m-j H:i:s", mktime(date("H")+$hours_num, date("i"), date("s"), date("m"), date("j"), date("Y")));
}


function OnViewUid($value)
{    
   return "<a href='/account.php?account={$value}'>{$value}</a>";  
}



 

function ParseTextSimple($text)
{
return strip_tags($text);
}




class DBDataViewerStd
{
    function MapField($field_name,$field_value)
    {
        switch($field_name)
        {
        case "datetime":
        return FormatDateTimeForView($field_value);
        break;
        
        default:
        return $field_value;
        }
    }
}






function SetSuperAdmin($value)
{
     
        $_SESSION['super_admin']=$value;
     
}


function getPermissionAdmin()
{
if(isset($_SESSION['admin_login']))
{return true;}
else
{return false;}
}

function SetAdminLogin($value)
{
     $_SESSION['admin_login']=$value;
}

function GetAdminLogin()
{
     return $_SESSION['admin_login'];
}




function Print1Page($beg,$end,$page_num,$href)
{
    
for($i=$beg;$i<=$end;$i++)
{
if($i==$page_num)
{
    echo "<font>".$i."&nbsp;</font>";
}
else
{
    $href1=str_replace(array("%param%","%param_value%"),array("&page_num=".$i,$i),$href);
    echo "<a href='{$href1}'>".$i."</a>&nbsp;";
}
}
    
}

function getUnReadMessNum($user_id)
{
$users_gateway=new users();
$temp=$users_gateway->getRecFieldsById($user_id,array("unread_mess_num"));
return $temp['unread_mess_num'];
}


function PrintPagesList($page_all_number,$page_num,$href)
{

    if($page_all_number>10)
    {
$max_printed;
if($page_num<=3||$page_num>=$page_all_number-3)
{

    Print1Page(1,4,$page_num,$href);
    echo "&nbsp;<font>...</font>&nbsp;";
    Print1Page($page_all_number-4,$page_all_number,$page_num,$href);

}
else
{
    Print1Page(1,3,$page_num,$href);
    if($page_num>5&&$page_num<$page_all_number-5)
    {
    echo "&nbsp;<font>...</font>&nbsp;";
    Print1Page($page_num-1,$page_num+1,$page_num,$href);
    echo "&nbsp;<font>...</font>&nbsp;";
    }
    else
    {
        if($page_num<=5)
        {
        Print1Page(4,$page_num+1,$page_num,$href);
        echo "&nbsp;<font>...</font>&nbsp;";
        }
    }
    
    
    
    
    
    if($page_num>=$page_all_number-5)
    {
        echo "&nbsp;<font>...</font>&nbsp;";
        Print1Page($page_num-1,$page_all_number-4,$page_num,$href);
    }
    
    
    
    Print1Page($page_all_number-3,$page_all_number,$page_num,$href);
}




    }
    else
    {
    
        Print1Page(1,$page_all_number,$page_num,$href);
    
    }
    
    

}


function PrintPagesBlock($page_num,$goods_num,$recs_per_page,$href)
 {
$page_all_number=getPageNumber($goods_num,$recs_per_page);;
    
if($page_all_number!=0)
{
//echo "<div style='float:right; padding-top:5px; padding-bottom:5px;'><font>�������� ��������: ".$page_num." �� ".$page_all_number."</font></div>";


echo "<div style='float:left; padding-top:5px; padding-bottom:5px;'>��������: ";
PrintPagesList($page_all_number,$page_num,$href);
echo "</div>";
}

}



function PrintPagesBlockBlog($page_num,$goods_num,$recs_per_page,$href)
 {
$page_all_number=getPageNumber($goods_num,$recs_per_page);;
    
if($page_all_number!=0)
{

echo "<div style='padding-top:5px; padding-bottom:5px;'><font>��������: </font>";

PrintPagesList($page_all_number,$page_num,$href);

echo "</div>";
}

}

function PrintPagesTeam($page_num,$goods_num,$recs_per_page,$href)
 {
$page_all_number=getPageNumber($goods_num,$recs_per_page);;
    
if($page_all_number!=0)
{

echo "<div><font>��������: </font>";

PrintPagesList($page_all_number,$page_num,$href);

echo "</div>";
}

}



   


function DidYearPass($date)
{
    list($add_year,$add_month,$add_day)=split("-",$date);
    $end_period=mktime(0,0,0,$add_month,$add_day,$add_year+1);
    
    $now=mktime(0,0,0,date('m'),date('j'),date('Y'));
    
    
    if($end_period>$now)
    {return false;}
    else
    {return true;}
    
}






function IsSuperAdmin()
{
    if(isset($_SESSION['super_admin']))
    {
        return $_SESSION['super_admin'];
    }
    else
    {
        return false;
    }
}



function FormatDateTimeForViewWithSec($str_date)
{
list ($date, $time)=split(" ",$str_date);
$sep="[/.-]";
list ($year, $month, $day)=split($sep,$date);


list ($h, $m,$s)=split(":",$time);

return $day."/".$month."/".$year." ".$h.":".$m.":".$s;
}


function FormatDateTimeForDBWithSec($str_date)
{
$ar=split(" ",$str_date);

if(count($ar)==2)
{
$date=$ar[0]; 
$time=$ar[1]; 
}
else
{
    $data=$ar[0];
    if(strpos($data,":")!==false)
    {
    //��� �����
    return $data;
    }
    else if(strpos($data,"/")!==false)
    {
    //��� ����
    $sep="[/.-]";
list ($day, $month, $year)=split($sep,$data);

return $year."-".$month."-".$day;

    }
    else
    {
    return  $data;
    }
    
}

//list ($date, $time)
$sep="[/.-]";
list ($day, $month, $year)=split($sep,$date);

return $year."-".$month."-".$day." ".$time;
}









  


function tempname($ext)
{
      $name  = time().'_';
      for ($i=0;$i<8;$i++){
          $name .= chr(rand(97,121));
      }
      $name .= '.'.$ext;
      return $name;
}

function tempvalue()
{
      $name  = time().'_';
      for ($i=0;$i<8;$i++){
          $name .= chr(rand(97,121));
      }
      return $name;
}


function replaceRussians($str)
{
$rus=array(' ','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�');
$eng=array('_','a','b','v','g','d','e','e','zh','z','i','i','k','l','m','n','o','p','r','s','t','u','f','kh','ts','ch','sh','shc','','y','','e','yu','ya');
for($i=0;$i<count($rus);$i++)
{
$str=str_replace($rus[$i], $eng[$i], $str);
}

return $str;
}

function EchoPhotosLoader($text,$value,$base_dir)
{
    
    echo "
    <script src='js/JsHttpRequest/lib/JsHttpRequest/JsHttpRequest.js' type='text/javascript'></script>
    <script>
    var loaded_photos=0;
 var imgcontainer_num=1;
 
 
 
 function LoadPic2()
 {
    
    JsHttpRequest.query('ajax/loadpic.php',
    {'picture':document.getElementById('id_picture')},
    function(result, errors) 
    {
    
   // document.getElementById('imgcontainer'+imgcontainer_num).innerHTML += errors;
     //'picture':document.getElementById('id_picture')
      
        if(result['result']==1)
            {
             AddCount();   
            document.getElementById('imgcontainer'+imgcontainer_num).innerHTML += errors;
            document.getElementById('reportresult').innerHTML =\"<font class='green'>�������� ���������� ���������!</font>\";
            }
            else
            {
            document.getElementById('reportresult').innerHTML = errors;
            }
        
    },
    false
    );
    
    //alert('LoadCategoryInfo12');
    document.getElementById('reportresult').innerHTML = '<font>�������� ����������... ����������, ���������!</font>';
    //alert('LoadCategoryInfo2');
    
 
 
//alert('LoadCategoryInfoEND');
 return false;
 
 }
 
 
 
 
 
 function RemoveElem(id)
 {
 var elem = document.getElementById(id);
 elem.parentNode.removeChild(elem);
 
 MinusCount();
 
 }
 
 
 function AddCount()
 {
 loaded_photos++;    
 if(loaded_photos/6>1)
 {imgcontainer_num=2;}
 
 document.getElementById('count').innerHTML=loaded_photos;
 }
 
 function MinusCount()
 {
 loaded_photos--;    
 if(loaded_photos/6>1)
 {imgcontainer_num=2;}
 else
 {imgcontainer_num=1;}
 
 document.getElementById('count').innerHTML=loaded_photos;
 }
 </script>
 ";
    
    
    
    
    global $photo_limit_size;
    
    
     $def_photo=$value;
        $pic=explode(";",$def_photo);
        
        $default_imgcontainer1_content="";
        $default_imgcontainer2_content="";
        if($def_photo!="")
        {
        for($i=0;$i<count($pic);$i++)
        {
            if($i==0)
            {$main_pic="checked";}
            else
            {$main_pic="";}
            
            $pic_name=$pic[$i];
             $ar=split("[.]",$pic_name);
            $pic_name_small=$ar[0]."_small.".$ar[1];
            
            $temp_id=tempvalue();
            
             $add_string="<div style='float:left; text-align:center; padding:5px;' id='".$temp_id."'>
     <img src='".$base_dir.$pic_name_small."'>   <br>
     <a href='#' onclick=\"RemoveElem('".$temp_id."'); return false;\">�������</a><br>
     <input class='p' id='good_picture_".$temp_id."' name='loaded_good_picture_".$temp_id."' type='hidden' value='".$pic_name."'> 
     <font>�������:</font> <input type='radio' name='radio_main_pic' value='".$pic_name."' ".$main_pic.">
</div>";
            if($i<6)
            {$default_imgcontainer1_content.=$add_string;}
            else
            {$default_imgcontainer2_content.=$add_string;}
            
        }
        }
    
    
    
echo "

 
";

   echo "<fieldset style='border:1px solid silver;'>
                                                <legend><font class='mess_table_header'>".$text."</font>
                                                </legend>";
                                                
                                           
                                                      
                                                
                                                              echo "<center><strong><font>��������� �����������: <span id='count'>0</span>";
                                                              
                                                              
                                                              echo "</font></strong></center>&nbsp;
                                                              
                                                    
                                                    <table cellpadding='0' cellspacing='0' align='center' class='m_tab' width='100%'>
                                                    <tr><td class='td_greybox' align='center'>
                                                    
                                                           <font>
                                                           ������� '�����', ����� ������� ����������
                                                           </font>                                                        
                                                    
                                                           
                                                     <div id='imgcontainer1'>       
                                                     ".$default_imgcontainer1_content."
                                                     </div>
                                                     <div id='imgcontainer2'>                                                                                                                              
                                                     ".$default_imgcontainer2_content."
                                                     </div>";          

                                                    echo "</td></tr></table>";    
echo "<br><div style='clear:both;'>";
//echo "<form id='f1' method='post' enctype='multipart/form-data'>";
EchoFile("�������� ��������(������ �� ����� ".$photo_limit_size." ��):","picture",20);
echo "&nbsp;&nbsp;&nbsp;<input type='button' onclick='LoadPic2();' class='good_button' value='�������� ����'>";
//echo "&nbsp;&nbsp;&nbsp;<input type='submit' class='good_button' value='�������� ����'>";
//echo "</form>";

echo "</div>
<br>
<center>
<span id='reportresult'>
</span></center>";

  
               
               
                                                
                                                
                                                echo "</fieldset>";

}



function EchoFile($text,$name,$size)
{
echo "<font>".$text." </font><input type='file' class='good_text' name='".$name."' id='id_".$name."'/>";
}



function EchoLinks($name,$valuefont1,$valuesize1,$valuecolor1,$underline1)
{
echo "
<div class='adm_container'>
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='left' valign='top' width='400px'>
<div style='padding:4px;'>
<font>�����: </font>&nbsp;&nbsp;
";
$fonts=array("Arial","Courier New","Times New Roman","Verdana","Tahoma");
echo "<select class='good_text' name='temp1_".$name."' id='id_temp1".$name."' onChange='OnChange_id_".$name."();'>";
for($i=0;$i<count($fonts);$i++)
{
if($fonts[$i]==$valuefont1)
	{echo "<option value='".$fonts[$i]."' selected>".$fonts[$i]."</option>";}
else
	{echo "<option value='".$fonts[$i]."'>".$fonts[$i]."</option>";}
}
echo "</select></div>";

echo "<div style='padding:4px;'>
<font>������: </font>";
echo "<input type='text' name='temp2_".$name."' id='id_temp2".$name."' size='4' value='".$valuesize1."' class='good_text' onkeyup='OnChange_id_".$name."();'/></div>";
$checked="";
if($underline1=="true")
{$checked="checked";}

echo "<div style='padding:4px;'><INPUT TYPE=CHECKBOX NAME='checkbox_".$name."' id='id_checkbox_".$name."' ".$checked." onchange='OnChange_id_".$name."();'><font> ����������&nbsp;&nbsp;&nbsp;&nbsp;</font></div>

<div style='padding:4px;'>
";
EchoColorExt("���� ������: ","id_temp3".$name,"temp3_".$name,$valuecolor1,"OnChange_id_".$name."();");

echo "</div></td>

<td valign='top' align='center'>";

echo "
<div id='font_result_id".$name."'></div>
</td></tr>
</table>
</div>";

echo "<input type='hidden' name='".$name."' id='id_".$name."' size='40' value='".$valuefont1.";".$valuesize1.";".$valuecolor1.";".$underline1."' class='good_text'/>";


echo "<script language='Javascript'>

var u=''; var u1='';
if(document.getElementById('id_checkbox_".$name."').checked)
{u='<u>'; u1='</u>';}

document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� ������:</font><br><font style='color: \"+document.getElementById('id_temp3".$name."').value+\"; font-family: \"+document.getElementById('id_temp1".$name."').value+\"; font-size: \"+document.getElementById('id_temp2".$name."').value+\"px;'>\"+u+\"�������� �����\"+u1+\"</font>\";

function OnSubmit_id_".$name."()
{
var u='false';
if(document.getElementById('id_checkbox_".$name."').checked)
{u='true';}

document.getElementById('id_".$name."').value=document.getElementById('id_temp1".$name."').value+\";\"+document.getElementById('id_temp2".$name."').value+\";\"+document.getElementById('id_temp3".$name."').value+ \";\"+u;
}



function OnChange_id_".$name."()
{
var u=''; var u1='';
if(document.getElementById('id_checkbox_".$name."').checked)
{u='<u>'; u1='</u>';}

document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� ������:</font><br><font style='color: \"+document.getElementById('id_temp3".$name."').value+\"; font-family: \"+document.getElementById('id_temp1".$name."').value+\"; font-size: \"+document.getElementById('id_temp2".$name."').value+\"px;'>\"+u+\"�������� �����\"+u1+\"</font>\";
}

</script>";
}



function EchoLinks2($name,$valuefont1,$valuesize1,$valuecolor1,$underline1)
{
echo "
<div style='border:1px solid silver;'>
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td align='left' valign='top' width='450px'>
<div style='padding:4px;'>
<font>�����: </font>&nbsp;&nbsp;
";
$fonts=array("Arial","Courier New","Times New Roman","Verdana","Tahoma");
echo "<select class='good_text' name='temp1_".$name."' id='id_temp1".$name."' onChange='OnChange_id_".$name."();'>";
for($i=0;$i<count($fonts);$i++)
{
if($fonts[$i]==$valuefont1)
    {echo "<option value='".$fonts[$i]."' selected>".$fonts[$i]."</option>";}
else
    {echo "<option value='".$fonts[$i]."'>".$fonts[$i]."</option>";}
}
echo "</select></div>";

echo "<div style='padding:4px;'>
<font>������: </font>";
echo "<input type='text' name='temp2_".$name."' id='id_temp2".$name."' size='4' value='".$valuesize1."' class='good_text' onkeyup='OnChange_id_".$name."();'/></div>";
$checked="";
if($underline1=="true")
{$checked="checked";}

echo "<div style='padding:4px;'><INPUT TYPE=CHECKBOX NAME='checkbox_".$name."' id='id_checkbox_".$name."' ".$checked." onchange='OnChange_id_".$name."();'><font> ����������&nbsp;&nbsp;&nbsp;&nbsp;</font></div>

<div style='padding:4px;'>
";
EchoColorExt("���� ������: ","id_temp3".$name,"temp3_".$name,$valuecolor1,"OnChange_id_".$name."();");

echo "</div></td>

<td valign='top' align='center'>";

echo "
<div id='font_result_id".$name."'></div>
</td></tr>
</table>
</div>";

echo "<input type='hidden' name='".$name."' id='id_".$name."' size='40' value='".$valuefont1.";".$valuesize1.";".$valuecolor1.";".$underline1."' class='good_text'/>";


echo "<script language='Javascript'>

var u=''; var u1='';
if(document.getElementById('id_checkbox_".$name."').checked)
{u='<u>'; u1='</u>';}

document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� ������:</font><br><font style='color: \"+document.getElementById('id_temp3".$name."').value+\"; font-family: \"+document.getElementById('id_temp1".$name."').value+\"; font-size: \"+document.getElementById('id_temp2".$name."').value+\"px;'>\"+u+\"�������� �����\"+u1+\"</font>\";

function OnSubmit_id_".$name."()
{
var u='false';
if(document.getElementById('id_checkbox_".$name."').checked)
{u='true';}

document.getElementById('id_".$name."').value=document.getElementById('id_temp1".$name."').value+\";\"+document.getElementById('id_temp2".$name."').value+\";\"+document.getElementById('id_temp3".$name."').value+ \";\"+u;
}



function OnChange_id_".$name."()
{
var u=''; var u1='';
if(document.getElementById('id_checkbox_".$name."').checked)
{u='<u>'; u1='</u>';}

document.getElementById('font_result_id".$name."').innerHTML=\"<font>��� ����� ��������� ������:</font><br><font style='color: \"+document.getElementById('id_temp3".$name."').value+\"; font-family: \"+document.getElementById('id_temp1".$name."').value+\"; font-size: \"+document.getElementById('id_temp2".$name."').value+\"px;'>\"+u+\"�������� �����\"+u1+\"</font>\";
}

</script>";
}




function translite($string)
{
  $trans = array("�"=>"i","�"=>"I","�"=>"a","�"=>"b","�"=>"v","�"=>"g","�"=>"d","�"=>"e", "�"=>"yo","�"=>"j","�"=>"z","�"=>"i","�"=>"i","�"=>"k","�"=>"l", "�"=>"m","�"=>"n","�"=>"o","�"=>"p","�"=>"r","�"=>"s","�"=>"t", "�"=>"y","�"=>"f","�"=>"h","�"=>"c","�"=>"ch", "�"=>"sh","�"=>"sh","�"=>"i","�"=>"e","�"=>"u","�"=>"ya"," "=>"_",
  "�"=>"A","�"=>"B","�"=>"V","�"=>"G","�"=>"D","�"=>"E", "�"=>"Yo","�"=>"J","�"=>"Z","�"=>"I","�"=>"I","�"=>"K", "�"=>"L","�"=>"M","�"=>"N","�"=>"O","�"=>"P", "�"=>"R","�"=>"S","�"=>"T","�"=>"Y","�"=>"F", "�"=>"H","�"=>"C","�"=>"Ch","�"=>"Sh","�"=>"Sh", "�"=>"I","�"=>"E","�"=>"U","�"=>"Ya",
  "�"=>"","�"=>"","�"=>"","�"=>"","-"=>"_","/"=>"","\\"=>"","."=>"",":"=>"","\""=>"",","=>"",";"=>"","?"=>"","%"=>"");
  return iconv('cp1251', 'utf-8',strtr($string, $trans));
}

   


?>