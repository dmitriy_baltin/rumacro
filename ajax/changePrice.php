<?
header('Content-Type: text/html; charset=windows-1251');
session_start();
include("../config.php");
include("../core/functions.php");
include("../core/database.php");
include("../core/main_functions.php");

$db=new DataBase();


if(getPermission())
{

    $item_id=$_REQUEST['item_id'];
    $price=$_REQUEST['price'];

    if(!isNum($item_id))
    {
        echo json_encode(array("result"=>"error","contents"=>"�������� �������� item_id"));
        exit();               
    }
    
    if(!isNum($price))
    {
        echo json_encode(array("result"=>"error","contents"=>"�������� �������� price"));
        exit();               
    }
    
    
    
    $goods_gateway=new goods();
    $good=$goods_gateway->getRecFieldsById($item_id,array("user_id","price"));
    if(!$good)
    {
        echo json_encode(array("result"=>"error","contents"=>"����� �� ������"));
        exit();               
    }
    
    if($good['user_id']!=getUserID())
    {
        echo json_encode(array("result"=>"error","contents"=>"� ��� ��� ���� �� ������ �����"));
        exit();               
    }
    
    if($good["price"]==$price)
    {
        echo json_encode(array("result"=>"success","contents"=>$price));
        exit();
    }
    
    $result = $goods_gateway->UpdateExt($item_id,array("price"),array($price));
    if($result)
        echo json_encode(array("result"=>"success","contents"=>$price));
    else
        echo json_encode(array("result"=>"error","contents"=>"������ ������� � ���� ������"));
}
else
{
    echo json_encode(array("result"=>"error","contents"=>"��� ����"));
    exit();               
}



?>