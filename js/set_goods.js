$(document).ready(function(){
    
    
    
        $(".good_holder .price span").live("click",function(){
        var val = $(this).html();
        if (val.match(/^[-\+]?\d+/) !== null) {
            $(this).html("<input type='text' value='"+val+"' size='4'>");
        }
    });
    
    
    $(".good_holder .price span input").live("keypress",function(e){
        if ((e.keyCode || e.which) == 13) 
        {
             var span = $(this).parent("span");
             var item_id = $(span).parent("div").data("item_id");
             
             $.get('/ajax/changePrice.php', {'price':$(this).val(), 'item_id':item_id}, 
               function(data1) {
                    if(data1.result=="success")
                        {    
                            $(span).html(data1.contents);
                        }
                    else if(data1.result=="error")
                       {
                         alert(data1.contents);   
                       }
                   },'json'
                   );              
        }
    });
    
    

$("#g_cat_id").chosen();
$("#new_cat_parent_id").chosen();

$("#g_cat_id").change(function(){
    if($("#g_cat_id :selected").val()!=-1)
        LoadGoods();
    else
        AddCatFormGroup();
    
});


$("#id_good_status, #id_good_avaliable, #id_good_sale_type").change(function(){
        LoadGoods();
        return false;
});

$("#id_search_str").keypress(function(e){
    if ((e.keyCode || e.which) == 13) 
            {
                LoadGoods();
            }
});

$(".cl_buts a").live("click",function(){
    if(confirm('�� �������, ��� ������ ������� ������ �����?'))
        RemoveGood($(this).attr('id'));
    return false;
});




$(".change_status_buts").live("click",function(){
            ChangeStatus($(this).data('good_id'),100);
            return false;  
});


$(".change_selected_status").live("click",function(){
    
    var new_status=$(this).data('new_status');
    if(!checkIfItemsSelected())
        alert('��� ���������� �������� ��������, ������ ������� �� ������ ��������');
    else
        {
                    $(".ck input[type=checkbox]").each(function(){
                        if($(this).attr('checked')=="checked")
                              ChangeStatus($(this).attr('id'),new_status);
                        });
        }   
        
});


$("#move_to_group_all_sel").live("click",function(){
    if(!checkIfItemsSelected())
        alert('��� ���������� �������� ��������, ������� �� ������ ���������');
    else
        {
            if($('#g_cat_dest_id :selected').val()==0)
            {
                alert('��� ���������� ������� ���������, � ������� ����� ���������� ������');
            }
        
            if(confirm('�� �������, ��� ������ ��������� ��� ���������� ������ � ��������� ������?'))    
                {
                    var ids=new Array();
                    $(".ck input[type=checkbox]").each(function(){
                        if($(this).attr('checked')=="checked")
                            ids.push($(this).attr('id'));
                            
                        });
                    MoveToGroup(ids,$('#g_cat_dest_id :selected').val());
                }
        }   
});





$(".remove_cat").live("click",function(){

if(confirm('�� �������, ��� ������ ������� ������ ������?'))    
    window.location.href='index.php?page=set_goods&remove_cat='+$(this).attr('rel');
                
return false;
});


$("#add_group_but").click(function(){
    AddCatFormGroup();
});

$("#confirm_items_but").click(function(){
    $('#confirm_items_process').show();
    setTimeout(function(){ 
        $('#confirm_items_process').html("<div class='success_cont'>������� ������������</div>"); 
        setTimeout(function(){ $('#confirm_items_cont').hide('slow');  },2000);
    },1500);
});




$("#add_group_form").submit(function(){

if($('#id_group_name').val()=="")
{
    alert('������� �������� ������');
    return false;
}

return true;
});









});


function LoadGoods()
{
    var good_status=$("#id_good_status :selected").val();
    var good_avaliable=$("#id_good_avaliable :selected").val();
    var good_sale_type=$("#id_good_sale_type :selected").val();   
    var temp_cat_id=$("#g_cat_id :selected").val();
    var temp_search_val=$("#id_search_str").val();
    $('#goods_holder_ajax').html("<p align='center'><img src='img/progress_bar_small.gif'></p>");
    $('#goods_holder_ajax').load('ajax/loadgoods.php', 
    {'cat_id': temp_cat_id,'search_val': temp_search_val,'good_status':good_status,'good_avaliable':good_avaliable,'good_sale_type':good_sale_type});
}



function RemoveGood(id)
{
    if(id>0)
    {
        $('#rem_but_'+id).html("<img src='img/progress_bar_small.gif'>");
        $('#good_'+id).load('ajax/removegood.php', {'id': id});
    }
}

function ChangeStatus(good_id,new_status)
{    //���������� ���.
    $('#change_status_but'+good_id).html("<img src='img/progress_bar_small.gif'>");
    $('#change_status_but'+good_id).load('/ajax/changeGoodStatus.php', {'good_id': good_id,'new_status':new_status});
}



function MoveToGroup(ids,new_group_id)
{
        $.get('/ajax/moveItemsToGroup.php', {'ids':ids, 'new_group_id':new_group_id}, 
                           function(data1) {
                           //alert(data1.result);
                            if(data1.result=="success")
                                {    
                                    LoadGoods();
                                }
                            else if(data1.result=="error")
                               {
                                    $('#move_to_group_process').html("<div class='error_cont'>"+data1.contents+"</div>");
                                    setTimeOut(function(){ $('#move_to_group_process').html(""); },1000);
                               }
                               
                           },'json'); 
                           
        $('#move_to_group_process').html("<img src='img/progress_bar_small.gif'>");       
}






