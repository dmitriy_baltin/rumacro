$(document).ready(function(){
    
    $('.order-button').bind('click',function(){
        
        var is_context = "";
        if($('#is_context_'+$(this).data("pocketid")).prop('checked')){
            is_context = "&context=1";    
        }
        else{
            is_context = "&context=0";
        }    
        window.location.href = "/?page=pay&type=service&data="+$(this).data("pocketid")+is_context;
    });
    
    $('.is_context').bind('click',function(){
        if($('#is_context_'+$(this).data("pocketid")).prop('checked')){
            $('.price_' + $(this).data("pocketid")).html($(this).data("withcontext"));
        }
        else{
            $('.price_' + $(this).data("pocketid")).html($(this).data("nocontext"));
        }
    });
    
});