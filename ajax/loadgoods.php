<?
session_start();
header('Content-Type: text/html; charset=windows-1251');


include("../config.php");
include("../core/functions.php");
include("../core/database.php");
include("../core/main_functions.php");

$db=new DataBase();

$cat_id=$_REQUEST['cat_id'];
$good_status=$_REQUEST['good_status'];
$good_avaliable=$_REQUEST['good_avaliable'];
$good_sale_type=$_REQUEST['good_sale_type'];

if(getPermission())
{

    if(!isNum($cat_id)||!isNum($good_status)||!isNum($good_avaliable)||!isNum($good_sale_type))
        exit();
            
        
    $name=iconv('UTF-8', 'windows-1251', stringBeforeDBInputWithStripTags($_REQUEST['search_val']));

    $recs_per_page=20;
    $page_num=1;


    $goods_gateway=new goods(); 
    $goods_info=$goods_gateway->getUsersGoodsInfo(getUserID(),$cat_id,$name,$good_status,$good_avaliable,$good_sale_type,$page_num,$recs_per_page);

    $goods=$goods_info[0];
    $goods_num=$goods_info[1];

    $user_categories_gateway=new user_categories();
    $main_cats22=$user_categories_gateway->getUsersGoodsCategories(getUserID(),"=");
    $main_cats_num=mysql_numrows($main_cats22);
    $sub_cats22=$user_categories_gateway->getUsersGoodsCategories(getUserID(),"<>");


    $param_line="index.php?page=set_goods";


    if($cat_id)
        $param_line.="&cat_id=".$cat_id;
        

    if($name)
        $param_line.="&name=".$name;
        
    if($good_status!=100)
        $param_line.="&good_status=".$good_status;
        
    if($good_avaliable!=100)
        $param_line.="&good_avaliable=".$good_avaliable;
        
    if($good_sale_type!=100)
        $param_line.="&good_sale_type=".$good_sale_type;



    include("../templates/partial/part_set_goods_good.html");    

}
?>