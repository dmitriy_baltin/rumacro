<?

include("../config.php");
include("../const.php");
include("../core/functions.php");
include("../core/database.php");
include("../core/main_functions.php");

$db=new DataBase();

$search_str = trim(strip_tags($_GET['term']));
$param = trim(strip_tags($_GET['param']));
if(strlen($search_str)>=3)
{
   $search_str_utf8=$search_str;
   $search_str = iconv("UTF-8", "CP1251", $search_str); 
    
    
    if( $param == "items" ){
    
        $goods=new goods();
        
        $ids=$goods->getGoodsIdsByName($search_str_utf8);
        
        if($ids)
            $search_cond ='`goods`.`id` IN ('.$ids.')';
        else $search_cond ='false';

        //$search_cond = "MATCH (`goods`.`name`) AGAINST ('\"{$search_str}\"' IN BOOLEAN MODE)";
        // $search_cond = "`goods`.`name` LIKE '%".$search_str."%'";

        
        
        $sql =  "
        SELECT
            `goods`.`name` as `value`,
            `categories`.`name` as `cat_name`
        FROM 
            `goods` LEFT JOIN `categories` ON `goods`.`cat_id3` = `categories`.`id`   
        WHERE
            `goods`.`checked` = 1 AND `goods`.`view` = 1 AND {$search_cond}  
        LIMIT 
            0,100";
                       
    }
    elseif( $param == "companies" )
    {
        $sql =  "
        SELECT
            `users`.`name` as `value`
        FROM 
            `users`
        WHERE
            `users`.`name` LIKE '%".$search_str."%' 
        LIMIT 
            0,100";
    }
    
    
    //echo $sql;
    $result_mysql = mysql_query($sql);
    $results = array();
    while($rec = mysql_fetch_array($result_mysql))
    {
        $val = iconv("CP1251", "UTF-8", hsc(strip_tags($rec['value']))); 
        $cat = iconv("CP1251", "UTF-8", hsc(strip_tags($rec['cat_name'])));
        $results[] = array("id"=>$val,"value"=>$val." (".$cat.")"); 
    }
    
    echo json_encode($results);
    
}

     

?>