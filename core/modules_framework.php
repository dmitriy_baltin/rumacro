<?

   /*
abstract class BaseModule
{
var $_name;


var $_view_objects=array();



function __construct($name="")
{
    $this->_name=$name;
    $this->_view_objects=array();
}

function AddView($label,$obj)
    {$this->_view_objects[$label]=$obj;}
    
function ModuleHasLabel($label)
{
if($this->_view_objects[$label])
{return true;}
else
{return false;}
}



public function Show($label)
{
    
if($this->_view_objects[$label])
{$this->_view_objects[$label]->Show();}
else
{$this->StdViewShow($label);}
}


public function StdViewShow($label)
{
        
if(file_exists(getDocumentRoot()."modules/{$this->_name}/{$label}view.css"))
{
    include(getDocumentRoot()."modules/{$this->_name}/{$label}view.css");
}

    
if(file_exists(getDocumentRoot()."modules/{$this->_name}/{$label}view.php"))
{
    include(getDocumentRoot()."modules/{$this->_name}/{$label}view.php");
}
if(file_exists(getDocumentRoot()."modules/{$this->_name}/{$label}view.html"))
{
    include(getDocumentRoot()."modules/{$this->_name}/{$label}view.html");
}
}
}




   */





abstract class Showable
{
var $_sql_select="";
var $_sql_from="";
var $_sql_where="";
var $_sql_order="";
var $_get_count=false;
var $_recs_per_page=10;
var $_page_num=1;

var $_view_php_file="list_std";
 var $_view_css_file="list";


var $_directory_files;

 var $_HTMLResult; 
 var $_use_buffer;

//var $_name="";


function __construct($sql_select,$sql_from,$page_num,$recs_per_page,$sql_where="",$sql_order="",$get_count=false)
{
    $this->_name=$name;
    $this->_sql_select=$sql_select;
    $this->_sql_from=$sql_from;
    $this->_sql_where=$sql_where;
    $this->_sql_order=$sql_order;
    $this->_get_count=$get_count;
    $this->_page_num=$page_num;
    $this->_recs_per_page=$recs_per_page;
    $this->_use_buffer=true;
    
}


function getHTMLResult()
{return $this->_HTMLResult;}

function setViewPhpFile($value)
{$this->_view_php_file=$value;}

function setViewCssFile($value)
{$this->_view_css_file=$value;}

function DisableBuffer()
{
$this->_use_buffer=false;
}




public function getDataFromDatabase()
{
  $sql="SELECT ";
  if($this->_get_count)
    {$sql.="SQL_CALC_FOUND_ROWS ";}
    
  if($this->_sql_select)
    {$sql.="{$this->_sql_select} ";}
  else
    {$sql.="* ";}
    
  if($this->_sql_from)
    {$sql.="FROM {$this->_sql_from} ";}
  else
    {return false;}
  
  if($this->_sql_where)
    {$sql.="WHERE {$this->_sql_where} ";}
    
  if($this->_sql_order)
    {$sql.="ORDER BY {$this->_sql_order} ";}
    
  //if($this->_sql_limit)
    $sql.="LIMIT ".($this->_page_num-1)*$this->_recs_per_page." , ".$this->_recs_per_page;
  
  
  //echo $sql;
  
if($this->_get_count)
    $sql_count="SELECT FOUND_ROWS() as 'num'";

    
    //echo $sql;


$result=mysql_query($sql);
if($this->_get_count)
    $result_count=mysql_query($sql_count);

if($result)
{    
if($this->_get_count)
    {$row=mysql_fetch_array($result_count);
    $num=$row['num'];}
else
    $num=-1;
    
return array(0=>$result,1=>$num);
}
else
{
$this->setError(mysql_error());
return false;
}



}

public function Show()
{
    

  $result=$this->getDataFromDatabase();

if($result)
{
    
$items=$result[0];
$items_num=$result[1];
    
if($items)
{   
    
   $this->ShowData($items,$items_num);
   

   
}
else
{
    throw new Exception("Wrong sql query");
}

   
   
}
else
{
    throw new Exception("Wrong sql query");
}
  
}


public function ShowData($items,$items_num)
{
if($this->_use_buffer)
    ob_start();    

        
  if(file_exists("modules/{$this->_directory_files}/".$this->_view_css_file.".css"))
        echo "<link rel=STYLESHEET href=\"modules/{$this->_directory_files}/".$this->_view_css_file.".css\" type=\"text/css\">";
    
  if(file_exists("modules/{$this->_directory_files}/".$this->_view_php_file.".php"))
        include("modules/{$this->_directory_files}/".$this->_view_php_file.".php");
  else
        throw new Exception("No Listing file: {$this->_view_php_file}");
        
  if($this->_use_buffer)
   $this->_HTMLResult=ob_get_clean();
}



}









abstract class BaseList extends Showable
{
 var $_items_per_row=1;
 var $_up_pagenation=false;
 var $_down_pagenation=false;
 var $_header=false;
 var $_alllink=false;
 var $_alllink_text=false;
 var $_one_item_view_obj;
 //��� ��� ������������ ��������:
 var $_link_params="href='index.php?page=%page_name%&page_num=%param%'";
 
 
 var $_no_data_text="�� ������ ������ ��� ������";
 
 
 var $_list_ancor=false;
 
 
 
function __construct($one_item_view_obj,$sql_select,$sql_from,$sql_where="",$sql_order="",$get_count=false,$page_num=false,$recs_per_page=false)
{
    if(!$recs_per_page)
    {$recs_per_page=$this->getStdRecsPerPage();}
    
    if(!$page_num)
    {$page_num=$this->getStdPageFromGET();}
    
    $this->_view_php_file="list_std";
     $this->_view_css_file="list";
     $this->_directory_files="_listing";
     
     $this->_one_item_view_obj=$one_item_view_obj;

    
    parent::__construct($sql_select,$sql_from,$page_num,$recs_per_page,$sql_where,$sql_order,$get_count);   
}


function setListAncor($value)
{$this->_list_ancor=$value;}


function setItemsPerRow($value)
{$this->_items_per_row=$value;}

function setNoDataText($value)
{$this->_no_data_text=$value;}


function setAllLink($href,$text)
{$this->_alllink=$href; $this->_alllink_text=$text;}



function setPaginationLinkParams($value)
{$this->_link_params=$value;}

function setHeader($value)
{$this->_header=$value;}



function setUpPaginationTrue()
{$this->_up_pagenation=true;}

function setDownPaginationTrue()
{$this->_down_pagenation=true;}


public function getStdPageFromGET()
{
if(isset($_GET['page_num'])&&$_GET['page_num']>0&&is_numeric($_GET['page_num']))
{
return StringBeforeDB($_GET['page_num']);
}
else
{
    return 1;
}

}

public function getStdRecsPerPage()
{
    return 10;
}

 



public function ShowPagination($items_num)
{ 
    if($items_num>$this->_recs_per_page)
    {
  echo "<div class='page_cont'>";
  echo "��������: ";
//PrintPages($items_num,$this->_page_num,$this->_link_params);
    PrintPagesBlockAjax($this->_page_num,$items_num,$this->_recs_per_page,$this->_link_params);
  echo "</div>";  
    }
}


    


}





abstract class BaseItem extends Showable
{
var $_header_field;
var $_id_field;
var $_text_field=false;
var $_picture_field=false;
var $_picture_prefix="";
var $_picture_folder="";
var $_datetime_field=false;

var $_add_blocks=array();

var $_nl2br=true;

var $_text_template=false;
var $_header_template=false;
var $_date_template=false;  





public function AddAddBlock($field_name,$text_template)
{
  $this->_add_blocks[]=array('field_name'=>$field_name,'template'=>$text_template);
}




public function setDataTemplates($text_template,$header_template,$date_template)
{
  $this->_text_template=$text_template;
  $this->_header_template=$header_template;
  $this->_date_template=$date_template;
}


public function setPictureParams($picture_field,$picture_prefix,$picture_folder)
{
  $this->_picture_field=$picture_field;
  $this->_picture_prefix=$picture_prefix;
  $this->_picture_folder=$picture_folder;
}





public function TextWithoutBr()
{
$this->_nl2br=false;
}



public function EchoPicture($value)
{
    if($value!="")
    {
    $ar=explode(";",$value);
    $ar1=split("[.]",$ar[0]);
    
    echo "{$this->_picture_folder}/".$ar1[0]."{$this->_picture_prefix}.".$ar1[1];
    }
    else
    {
        echo "";
    }
}

public function EchoText($value)
{
    if($this->_text_template)
    {
        $value=str_replace("%value%",$value,$this->_text_template);
    }
    
    if($this->_nl2br)
        echo $value;
    else
        echo strip_tags($value);
}

public function EchoHeader($value)
{
    
    if($this->_header_template)
    {
        $value=str_replace("%value%",$value,$this->_header_template);
    }
    echo $value;   
}


public function EchoDateTime($value)
{
    $temp=FormatDateForView($value);
    
    if($this->_date_template)
    {
        $temp=str_replace("%value%",$temp,$this->_date_template);
    }
    
    echo $temp;   
}


public function ShowItem($item)
{ 
  if(file_exists("modules/{$this->_directory_files}/".$this->_view_php_file.".php"))
        include("modules/{$this->_directory_files}/".$this->_view_php_file.".php");
  else
        throw new Exception("No item file: {$this->_view_php_file}");
}    


}







class OneItemView extends BaseItem
{
var $_view_php_file; 
//var $_header_field=false;
//var $_id_field=false;
var $_href="p-%param1%%param2%";
var $_additional_field=false;
//var $_text_field=false;
//var $_picture_field=false;
//var $_picture_prefix="";
//var $_picture_folder="";
//var $_datetime_field=false;

//var $_add_blocks=array();



//var $_nl2br=true;
//var $_text_template=false;
//var $_header_template=false;
//var $_date_template=false;

function __construct($view_php_file,$header_field,$id_field,$href,$additional_field=false,$text_field=false,$datetime_field=false)
{
    if($view_php_file)
        $this->_view_php_file=$view_php_file;
    else
        $this->_view_php_file="item_std";
        
    $this->_directory_files="_listing";
    
  $this->_header_field=$header_field;
  $this->_id_field=$id_field;
  $this->_href=$href;
  $this->_additional_field=$additional_field;
  $this->_text_field=$text_field;

  $this->_datetime_field=$datetime_field;
  
}



public function EchoHref($id,$add_field_value)
{
    if($this->_additional_field&&$add_field_value)
    {
        $add_field_value=translite($add_field_value);
        $res=str_replace(array("%param1%","%param2%"),array($id,"-".$add_field_value),$this->_href);
    }
    else
        $res=str_replace(array("%param1%","%param2%"),array($id,""),$this->_href);
        
    return $res;
    
}






}







class ActicleBase  extends BaseItem 
{
 var $_id_value;   
 var $_has_photos=false;   

 var $_social_buttons_up=false;
 var $_social_buttons_down=false;
 var $_vkontakte_app_id=false;
 var $_twitter_name=false;
 
 
 var $_photo_sign_field="";
 var $_photo_sign_field_template="";
 
 var $_main_photo=true;
 
 var $_by_ref_field="";
 var $_by_ref_link_field="";
 var $_by_ref_template="";
 
 
 
 
 var $_gallery_prefix="";
 
 
 
 var $_HTMLTitle;
 var $_HTMLDescription;
 var $_HTMLKeywords;

 
 
 
 function SetPhotoSign($photo_sign_field,$photo_sign_field_template)
 {
     $this->_photo_sign_field=$photo_sign_field;
     $this->_photo_sign_field_template=$photo_sign_field_template;
 }
 
 function MianPhotoDisable()
 {
 $this->_main_photo=false;
 }
 
 
 function SetByRef($by_ref_field,$by_ref_link_field,$by_ref_template)
 {
     $this->_by_ref_field=$by_ref_field;
     $this->_by_ref_link_field=$by_ref_link_field;
     $this->_by_ref_template=$by_ref_template;
 }
 
 
 function setGalleryPrefix($value)
 {
    $this->_gallery_prefix=$value;
 }
 
 public function Show()
{
$result=$this->getDataFromDatabase();

$items=$result[0];
if($result&&$items)
{
    


if(mysql_numrows($items)>0)
{
$item=mysql_fetch_array($items);   

//������ ������������ title,keywords,description
$this->_HTMLTitle=$item[$this->_header_field];
$this->_HTMLKeywords=$item[$this->_header_field];
$this->_HTMLDescription=substr(strip_tags($item[$this->_text_field]),0,200);

$this->ShowData($item,false);
   
}
else
{
    throw new Exception("������ �� �������. ������ ������ ������� ���� �� ����������.");
}
   
   
}
else
{
    throw new Exception("Wrong sql query");
}
  
}

 
 
 
    
    

function __construct($id_value,$view_php_file,$sql_select,$sql_from,$header_field,$id_field,$text_field,$datetime_field=false)
{
$this->_directory_files="_articles";

if($view_php_file)
        $this->_view_php_file=$view_php_file;
    else
        $this->_view_php_file="article_std";
        
$this->_view_css_file="article";

$this->_id_value=$id_value;
$this->_id_field=$id_field;


 $this->_header_field=$header_field;
 $this->_text_field=$text_field;
 $this->_datetime_field=$datetime_field;           
       
parent::__construct($sql_select,$sql_from,1,1,"`{$this->_id_field}`={$this->_id_value}",false);
}


function getHTMLTitle()
{
return $this->_HTMLTitle;
}

function getHTMLKeyWords()
{
return $this->_HTMLKeywords;
}

function getHTMLDescription()
{
return $this->_HTMLDescription;
}


function ActivateSocialButtons($up,$down,$vkontakte_app_id,$twitter_name)
{
    $this->_social_buttons_up=$up;
    $this->_social_buttons_down=$down;
    $this->_vkontakte_app_id=$vkontakte_app_id;
    $this->_twitter_name=$twitter_name;
}



function GetDigitalParamFromGET($name)
{
    if(isset($_GET[$name])&&is_numeric($_GET[$name])&&$_GET[$name]>0)
    {
    return StringBeforeDB($_GET[$name]);
    }
    else
    {
    throw new Exception("�������� ��������");
    }
    

}





public function ArticleHasAdditionalPhotos()
{
  $this->_has_photos=true;
}



public function AddAddBlock($field_name,$text_template)
{
  $this->_add_blocks[]=array('field_name'=>$field_name,'template'=>$text_template);
}




public function setDataTemplates($text_template,$header_template,$date_template)
{
  $this->_text_template=$text_template;
  $this->_header_template=$header_template;
  $this->_date_template=$date_template;
}


public function setPictureParams($picture_field,$picture_prefix,$picture_folder)
{
  $this->_picture_field=$picture_field;
  $this->_picture_prefix=$picture_prefix;
  $this->_picture_folder=$picture_folder;
}






 
} 












    


?>