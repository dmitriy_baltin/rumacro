<?php
session_start();
header('Content-Type: text/html; charset=windows-1251');

include("../../config.php");
include("../../core/functions.php");
include("../../core/database.php");
include("../../core/main_functions.php");

$db=new DataBase();

if(isset($_SESSION['adm_login'])){
    if(!isset($_REQUEST['id'])||!is_numeric($_REQUEST['id'])){
        echo "bad id";
        exit(); 
    }
    if(!isset($_REQUEST['value'])){
        echo "bad value";
        exit(); 
    }
    $id = (int) $_REQUEST['id'];
    $value = 1; 
    if( $_REQUEST['value']=="false" || $_REQUEST['value']=="" ){
        $value = 0;    
    }  
    $macro_news_gateway = new macro_news();
    $macro_news_gateway->UpdateFields($id,array("on_main"),array($value));
    echo "ok";
}
?>
