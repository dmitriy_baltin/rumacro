<?

class PageFactory
{
function GetPage($aPage)
    {
   switch($aPage)
   { 
       
case "main": return new Page($aPage.".html",$aPage);     
case "agreement": return new Page($aPage.".html",$aPage);     
case "set_callbacks": return new AdminPage($aPage.".html",$aPage);
case "oferta": return new Page($aPage.".html",$aPage);     
case "macronews": return new Page($aPage.".html",$aPage);     
case "macronewsitem": return new Page($aPage.".html",$aPage);
case "rules": return new Page($aPage.".html",$aPage);
case "exit": return new Page($aPage.".html",$aPage);     
case "login": return new Page($aPage.".html",$aPage);     
case "items": return new Page($aPage.".html",$aPage);     
case "companies": return new Page($aPage.".html",$aPage);    
case "verifytestimonial": return new Page($aPage.".html",$aPage);
 
case "item": return new Page($aPage.".html",$aPage);     
case "services": return new Page($aPage.".html",$aPage);     
case "context": return new Page($aPage.".html",$aPage);     
case "pay": return new Page($aPage.".html",$aPage);     
case "pay_info": return new Page($aPage.".html",$aPage);     
case "register": return new Page($aPage.".html",$aPage); 
case "map": return new Page($aPage.".html",$aPage); 
case "set_main_info": return new AdminPage($aPage.".html",$aPage); 
case "set_ext_info": return new AdminPage($aPage.".html",$aPage); 
case "set_auth": return new AdminPage($aPage.".html",$aPage); 
case "query": return new AdminPage($aPage.".html",$aPage); 
case "set_verific": return new AdminPage($aPage.".html",$aPage); 
case "set_goods": return new AdminPage($aPage.".html",$aPage); 

case "edit_cat": return new AdminPage($aPage.".html",$aPage); 
case "set_categories": return new AdminPage($aPage.".html",$aPage);

case "add_good": return new AdminPage($aPage.".html",$aPage); 
case "edit_good": return new AdminPage($aPage.".html",$aPage); 
case "set_site_info": return new AdminPage($aPage.".html",$aPage); 
case "set_site_design": return new AdminPage($aPage.".html",$aPage); 

case "set_filials": return new AdminPage($aPage.".html",$aPage); 
case "add_filial": return new AdminPage($aPage.".html",$aPage); 
case "edit_filial": return new AdminPage($aPage.".html",$aPage); 

case "set_articles": return new AdminPage($aPage.".html",$aPage); 
case "add_article": return new AdminPage($aPage.".html",$aPage); 
case "edit_article": return new AdminPage($aPage.".html",$aPage); 

case "set_prices": return new AdminPage($aPage.".html",$aPage); 
case "add_price": return new AdminPage($aPage.".html",$aPage); 
case "edit_price": return new AdminPage($aPage.".html",$aPage);

case "set_prototypes": return new AdminPage($aPage.".html",$aPage); 
case "add_prototype": return new AdminPage($aPage.".html",$aPage); 
case "edit_prototype": return new AdminPage($aPage.".html",$aPage);
case "set_prototype_fields": return new AdminPage($aPage.".html",$aPage); 
case "add_prototype_field": return new AdminPage($aPage.".html",$aPage); 
case "edit_prototype_field": return new AdminPage($aPage.".html",$aPage);  
  

case "set_news": return new AdminPage($aPage.".html",$aPage); 
case "add_news": return new AdminPage($aPage.".html",$aPage); 
case "edit_news": return new AdminPage($aPage.".html",$aPage);

case "set_managers": return new AdminPage($aPage.".html",$aPage); 
case "add_manager": return new AdminPage($aPage.".html",$aPage); 
case "edit_manager": return new AdminPage($aPage.".html",$aPage);

case "set_favorites": return new AdminPage($aPage.".html",$aPage);

case "set_messages": return new AdminPage($aPage.".html",$aPage);
case "view_message": return new AdminPage($aPage.".html",$aPage);

case "set_orders": return new AdminPage($aPage.".html",$aPage);  
case "set_testimonials": return new AdminPage($aPage.".html",$aPage);
case "set_pay_delivery": return new AdminPage($aPage.".html",$aPage);  

case "site": return new SitePage($aPage.".html",$aPage);  
case "sabout": return new SitePage($aPage.".html",$aPage);  
case "scontacts": return new SitePage($aPage.".html",$aPage);  
case "stestimonials": return new SitePage($aPage.".html",$aPage);
case "saddtestimonial": return new SitePage($aPage.".html",$aPage);
case "spaydelivery": return new SitePage($aPage.".html",$aPage);  

case "sarticles": return new SitePage($aPage.".html",$aPage);  
case "sarticle": return new SitePage($aPage.".html",$aPage);  

case "snews": return new SitePage($aPage.".html",$aPage);  
case "snew": return new SitePage($aPage.".html",$aPage);  

case "sitems": return new SitePage($aPage.".html",$aPage);  
case "404": return new Page($aPage.".html",$aPage);  

    default:
        return new Page("404.html","404"); 
    
   }
    }
}

class Page
{
var $_page_main_content;
var $_link_label;

var $_page_before_main_content;
var $_page_after_main_content;

    function __construct($page_main_content, $link_label)
    {
        $this->_page_main_content=$page_main_content;
        $this->_link_label=$link_label;
		
		$this->_page_before_main_content=false;
        $this->_page_after_main_content=false;
    }
}


class AdminPage extends Page
{
    function __construct($page_main_content, $link_label)
    {
        $this->_link_label=$link_label;
        $this->_page_main_content="admin/".$page_main_content;
        
        $this->_page_before_main_content="admin/beforemain.html";
        $this->_page_after_main_content="admin/aftermain.html";
    }
}


class SitePage extends Page
{
    function __construct($page_main_content, $link_label)
    {
        $this->_link_label=$link_label;
        $this->_page_main_content=$page_main_content;
        
        $this->_page_before_main_content="beforemain.html";
        $this->_page_after_main_content="aftermain.html";
    }
}










?>