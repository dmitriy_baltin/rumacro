<?php
  $item_name = TextHelper::Out($item['name']);
  $item_text = nl2br(TextHelper::Out($item['text']));
  $item_comment = nl2br(TextHelper::Out($item['owner_comment']));
  $item_date = FormatDateForView(TextHelper::Out($item['date']));
  $item_mark = TextHelper::Out($item['mark']);
  $item_id = TextHelper::Out($item['id']);
  global $arTestimonialsParamDesc;
  global $arTestimonialsParamDelivery;
  global $arTestimonialsParamAnswer;  
?>
