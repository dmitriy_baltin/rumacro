<?php
session_start();
date_default_timezone_set('Europe/Kiev');

$page=null;



//phpIDS core



//config
include("../config.php");  



//core functions        
include("../core/database.php");
include("../class.phpmailer.php");
include("../core/functions.php");
include("../core/main_functions.php");
include("../rassilka_func.php");


$db=DataBaseSingleton::GetDB($db);


include("../const.php");  


$admin_flag=true;

include("page_class.php");


$page_factory=new PageFactory();


if(!isset($_SESSION['adm_login'])){
	$page=$page_factory->GetPage("login");
}
else{
    $admins_gateway = new admins();
    $user_admin = $admins_gateway->getRecById((int)$_SESSION['adm_login']);
    if($user_admin&&$user_admin['group']){
        $admin_groups_gateway = new admin_groups();
        $user_admin_rights = $admin_groups_gateway->getRecById($user_admin['group']);
    }
}



  

if(!$page)
{
	if($_GET['page'])
	    {$page=$page_factory->GetPage($_GET['page']);}
	else
	    {$page=$page_factory->GetPage("main");}
}


include("templates/index.html");




?>