<?

class SiteMapWriter
{
public $file;
public $cur_file;
public $counter;
public $counter_all=0;
public $max_recs_in_one_file;
public $eos;

public $write_to_file;



function OutString($string){
  if($this->write_to_file==true){
    fputs( $this->file, $string);
  }
  else{
    echo $string;
  }
}

function __construct($write_file=true)
{
  $this->write_to_file=$write_file;
  if($this->write_to_file){
   $this->file = fopen($_SERVER['DOCUMENT_ROOT']."/sitemap.xml","w");
    if(!$this->file)
    {
      echo("������ �������� ����� sitemap.xml");
    }
  }
  else{
    header("Content-Type:application/xml");
  }
  $this->cur_file="";
  $this->counter=0;
  $this->max_recs_in_one_file=50000;
  $this->eos="\r\n"; 
}



function BeginWrite()
{
    $this->OutString( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".$this->eos);
    $this->OutString( "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"".$this->eos);
    $this->OutString( "xmlns:image=\"http://www.sitemaps.org/schemas/sitemap-image/1.1\"".$this->eos);
    $this->OutString( "xmlns:video=\"http://www.sitemaps.org/schemas/sitemap-video/1.1\">".$this->eos);
}

function EndWrite()
{

    $this->OutString( "</urlset>".$this->eos);
    if($this->write_to_file){
      fclose($this->file);
    }

}



function Write($str,$freq="daily",$prio="0.8")
  {
    if($this->counter<$this->max_recs_in_one_file)
    {
        
     $str=str_replace("&","&amp;",$str);
     $this->OutString( "<url>".$this->eos);   
     $this->OutString( "<loc>".$str."</loc>".$this->eos);
     $this->OutString( "<changefreq>".$freq."</changefreq>".$this->eos);
     $this->OutString( "<priority>".$prio."</priority>".$this->eos);
     $this->OutString( "</url>".$this->eos);
     $this->counter++;
     $this->counter_all++;
    }
    else if($this->write_to_file)
    {                 
       if($this->cur_file=="")
       {$this->cur_file=1;}
       else
       {$this->cur_file++;}
       
       fclose($this->file);
       $this->file = fopen("sitemap{$this->cur_file}.xml","w");
       if(!$this->file)
        {
          echo("������ �������� ����� sitemap{$this->cur_file}.xml");
        }
        
        $this->counter=0;  
        
    }
  
  }

}

?>
