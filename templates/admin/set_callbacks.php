<?php
$table_gateway=new user_callbacks();
$recs_per_page=20;
if(isset($_GET['page_num'])&&is_numeric($_GET['page_num'])&&$_GET['page_num']>0)
    $page_num=stringBeforeDBInputWithStripTags($_GET['page_num']);
else
    $page_num=1;
list($items, $items_num)=$table_gateway->getAllRecsByPageExt("id","DESC",$page_num,$recs_per_page,array(new SearchField("user_id",getUserID(),"=")));

$table_gateway->nullNewCallbacks(getUserID());
?>
