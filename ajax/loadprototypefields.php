<?
session_start();
header('Content-Type: text/html; charset=windows-1251');

include("../config.php");
include("../core/functions.php");
include("../core/database.php");
include("../core/main_functions.php");

$db=new DataBase();

$cat_id=stringBeforeDBInputWithStripTags($_REQUEST['id']);
$good_id=stringBeforeDBInputWithStripTags($_REQUEST['good_id']);

if(!isNum($cat_id)||!getPermission())
    exit();

if($good_id&&!isNum($good_id))
    exit();

        
$user_categories_gateway=new user_categories();
$category=$user_categories_gateway->getRecFieldsById($cat_id,array("user_id","prototype_id"));

if($category['user_id']!=getUserID())
    exit();
    
$prototype_id=$category['prototype_id'];
?>
    
<?if($prototype_id):?>

    <?
    $user_prototypes_fields_gateway=new user_prototypes_fields();
    if(!$good_id)
        $prototypes_fields=$user_prototypes_fields_gateway->getWhereExt("prototype_id={$prototype_id}",false,"id","ASC");
    else    
        $prototypes_fields=$user_prototypes_fields_gateway->getPrototypeFieldsAndValues($prototype_id,$good_id);    
    ?>

    <?while($prototypes_field=mysql_fetch_array($prototypes_fields)):?>
        <?
        $prototypes_field_id=Out($prototypes_field['id']);
        $prototypes_field_name=Out($prototypes_field['name']);
        $prototypes_field_value=Out($prototypes_field['value']);
        ?>
        <div class='d_punkt'>
            <div class='label'><?echo $prototypes_field_name?></div>
            <div class='input'>
                <?printTextExtParams("prototype_fields[".$prototypes_field_id."]",$prototypes_field_value,2,40,"style='width:350px;'");?>
            </div>
            <div class='clr'></div>
        </div>
    <?endwhile;?>

<?else:?>
    <strong>��� ������ ������ ������� ��� �������������� �����.</strong><br>
    ����� �������� �������������� ���� ��� ���������� <a href='<?echo getPageLink("add_prototype")?>'>������� ��������</a>, �������� � ���� ���� � <a href='/index.php?page=edit_cat&id=<?echo $cat_id?>'>��������� �������� � ������ ������ �������</a>
<?endif;?>
