


function ClosePopUp()
{ //When clicking on the close or fade layer...
    $('#fade , .popup_block').fadeOut(function() {
        $('#fade, a.close').remove();  //fade them both out
    });
    return false;
}



function OpenExt(divid,params,funcOnClose)
{
    
	var popID = divid; //Get Popup Name
    var popURL = params; //Get Popup href to define size
 
    //Pull Query & Variables from href URL
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1]; //Gets the first query string value
 
    //Fade in the Popup and add close button
    $('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" onclick="return '+funcOnClose+'" class="close"><img src="/img/close.png" alt="" /></a>');
 
    //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
    //var popMargTop = 50;//($('#' + popID).height()+80) / 2;123
    var popMargLeft = ($('#' + popID).width()) / 2;
    var top = $(document).scrollTop() + 50; 
    //Apply Margin to Popup
    $('#' + popID).css({
        'margin-left' : -popMargLeft,
        'top': top
    });
    
    //Fade in Background
    $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
    $('#fade').css({'filter' : 'alpha(opacity=60)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 
    
    return false;
}