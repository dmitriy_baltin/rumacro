<? 
session_start();
header('Content-Type: text/html; charset=windows-1251');


include("../config.php");
include("../core/functions.php");
include("../core/database.php");
include("../core/main_functions.php");

$db=new DataBase();

if(isNum($_REQUEST['good']))    
{
    $good_id=stringBeforeDBInputWithStripTags($_REQUEST['good']);

    $goods_gateway=new goods();
    $good=$goods_gateway->getGoodForOrder($good_id);
    if($good)
    {
        $good_name=hsc($good['good_name']);
        $good_discount=hsc($good['good_discount']);
        $good_price=hsc($good['good_price']);
        if($good_price>0&&$good_discount)
        {
            $good_price = $good_price - $good_price * $good_discount / 100.0; 
        }
        $good_currency=hsc($good['currency']);
        $good_user_id=hsc($good['good_user_id']);
        $good_user_name=hsc($good['good_user_name']);
        $good_user_site_address=hsc($good['good_user_site_address']);
        $good_user_shop_name=hsc($good['good_user_shop_name']);
        $goods_photos=getPhotosForGroup(array($good_id));        
        $good_photo=getPic("images/photos/",$goods_photos[$good_id],3);
        $good_all_price=getPriceFormat($good_price,$good_currency);
        
        
        if($good_price==0)
           $temp="<span id='all_price'>{$good_all_price}</span>";
        else
            $temp="<span id='all_price'>{$good_price}</span> {$good_currency}";
        
        $delivery_regions=hsc($good['delivery_regions']);
        $delivery_dop_info=nl2br(hsc($good['delivery_dop_info']));
        
        $user_payways_gateway=new user_payways();
        $user_deliveries_gateway=new user_deliveries();
        
        $deliveries=$user_deliveries_gateway->getWhereExt("user_id=".$good_user_id." AND active=1",false,"id","ASC");
        $payways=$user_payways_gateway->getWhereExt("user_id=".$good_user_id." AND active=1",false,"id","ASC");
        if(mysql_numrows($payways))
            $has_payways=true;
            
        if(mysql_numrows($deliveries))
            $has_deliveries=true;

        if($has_payways&&$has_deliveries)
            $pay_and_delivery_label="�������� � ������";
        elseif($has_payways)
            $pay_and_delivery_label="������";
        elseif($has_deliveries)
            $pay_and_delivery_label="��������";
                
        $cur_user_fio="";
        $cur_user_email="";
        $cur_user_phone="";
        $cur_user_city="";
        $cur_user_address="";
        
        if(getPermission())
        {
            $user_orders_gateway=new user_orders();
            $orders=$user_orders_gateway->getWhereExt("sender_user_id=".getUserID(),false,"id","DESC");
            if($orders&&mysql_numrows($orders))
            {
                $order=mysql_fetch_array($orders);
                $cur_user_fio=hsc($order['fio']);
                $cur_user_email=hsc($order['email']);
                $cur_user_phone=hsc($order['phone']);
                $cur_user_city=hsc($order['city']);
                $cur_user_address=hsc($order['address']);
            }
        }
        
            
        
        
        
        
        echo "<div id='order'>";
                
          echo "<div class='order_items'>";
                    
          
              echo "<div class='order_item'>";
                echo "<div class='label'>���������</div>";
                echo "<div class='data'>";
                    echo "<a href='".getUserSiteUrl($good_user_id,$good_user_site_address)."'>{$good_user_name}</a> 
                        &nbsp;<span><a href='".getSitePageUrl("contacts",$good_user_id,$good_user_site_address)."'>&rarr; �������� ����������</a></span>";
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
                    
              
              
              echo "<div class='order_item'>";
                echo "<div class='label'>{$pay_and_delivery_label}</div>";
                echo "<div class='data'>";
                        if($has_deliveries)
                            {
                                echo "<select id='id_delivery' name='delivery' style='width:150px;'  class='good_text'>";
                                echo "<option value='0' rel='1'>**�������� ������ ��������**</option>";
                                while($delivery=mysql_fetch_array($deliveries))
                                {
                                    $delivery_name=hsc($delivery['name']);
                                    $delivery_is_address=$delivery['is_address'];
                                    echo "<option value='{$delivery['id']}' rel='{$delivery_is_address}'>{$delivery_name}</option>";
                                }
                                echo "</select>&nbsp;";
                            }
                            
                            if($has_payways)
                            {
                                echo "<select id='id_payway' name='payway' style='width:150px;' class='good_text'>";
                                echo "<option value='0'>**�������� ������ ������**</option>";
                                while($payway=mysql_fetch_array($payways))
                                {
                                    $payway_name=hsc($payway['name']);
                                    echo "<option value='{$payway['id']}'>{$payway_name}</option>";
                                }
                                echo "</select>";
                            }
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
              
              
              echo "<div class='order_item'>";
                echo "<span>������������ ����������</span><hr>";
              echo "</div>";
              
              echo "<div class='order_item'>";
                echo "<div class='label'>�������, ���</div>";
                echo "<div class='data'>";
                    printTextLine("fio",$cur_user_fio,10,"style='width:300px;'");
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
              
              echo "<div class='order_item'>";
                echo "<div class='label'>E-mail</div>";
                echo "<div class='data'>";
                    printTextLine("email",$cur_user_email,10,"style='width:300px;'");
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
              
              echo "<div class='order_item'>";
                echo "<div class='label'>�������</div>";
                echo "<div class='data'>";
                    printTextLine("phone",$cur_user_phone,10,"style='width:300px;'");
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
             
             
             if($has_deliveries)
                            {
              
              echo "<div id='delivery_form'>";
              
              echo "<div class='order_item'>";
                echo "<span>����� ��������</span><hr>";
              echo "</div>";
              
              echo "<div class='order_item'>";
                echo "<div class='label'>�����</div>";
                echo "<div class='data'>";
                    printTextLine("city",$cur_user_city,10,"style='width:300px;'");
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
              
              echo "<div class='order_item'>";
                echo "<div class='label'>�����</div>";
                echo "<div class='data'>";
                    printTextLine("address",$cur_user_address,10,"style='width:300px;'");
                    printHidden("has_address",1,10);
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
                          
              
              
              echo "</div>";
              
                } 
              
              
              echo "<div class='order_item'>";
                echo "<div class='label'>�����������</div>";
                echo "<div class='data'>";
                    printTextExtParams("comment","",2,45,"style='width:300px'");
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
              
              
              
              
              if(!getPermission())
              {
                echo "<div class='order_item'>";
                    echo "<div class='label'>��� ������</div>";
                    echo "<div class='data'>";
                        echo "<img src='../captcha.php?".session_name()."=".session_id()."'>";
                    echo "</div>";
                    echo "<div class='clr'></div>";
                  echo "</div>";
                  
                echo "<div class='order_item'>";
                echo "<div class='label'>������� ���</div>";
                echo "<div class='data'>";
                    printTextLine("code","",20,"style='width:80px;'");
                echo "</div>";
                echo "<div class='clr'></div>";
              echo "</div>";
              }
              else
                printHidden("code",3,10);
              
              
              
              printHidden("price_hidden",$good_price,10);
              printHidden("good_id",$good_id,10);
              
              
              echo "<div class='order_item'>";
                echo "<button class='submit' id='sendorder'>������� �����</button>";    
              echo "</div>";
              
              echo "<div class='order_item red' id='order_result'>";    
              echo "</div>";
              
              
          echo "</div>"; 
          
          echo "
          <div class='right'>
                <div class='good_cont_bord'>
                    <div class='good_cont1'>
                        <div class='photo'><a href='".getGoodUrl($good_id,$good_name)."'><img src='{$good_photo}'></a></div>
                        <div class='name'><a href='".getGoodUrl($good_id,$good_name)."'>{$good_name}</a></div>
                        <div class='clr'></div>
                        <div class='price'>��������� 1 ��.: <strong>{$good_all_price}</strong></div>
                        
                        <hr>
                        <div class='all_price'>����� ���������: {$temp}</div>";
                        
                        echo "<div class='order_item'>";
                            echo "<div class='label2 left'>����� ������</div>";
                            echo "<div class='data2 left'>";
                                printTextLine("count","1",10,"style='width:30px;'");
                            echo "</div>";
                            echo "<div class='clr'></div>";
                        echo "</div>";
                        
              
                echo "</div>
                </div>";
                
          if($has_deliveries&&($delivery_dop_info||$delivery_regions))
              {
              echo "<div class='order_item'>";
                echo "<span>�������������� ���������� � ��������</span><hr>";
              echo "</div>";
              
              if($delivery_dop_info)
              {
                  echo "<div class='order_item'>";
                    echo "<div class='label2'>�������������� ����������</div>";
                    echo "<div class='data2'>";
                        echo $delivery_dop_info;
                    echo "</div>";
                  echo "</div>";
              }
              
              if($delivery_regions)
              {
                 echo "<div class='order_item'>";
                    echo "<div class='label2'>������� ��������</div>";
                    echo "<div class='data2'>";
                        echo $delivery_regions;
                    echo "</div>";
                    echo "<div class='clr'></div>";
                  echo "</div>";
              
              }
              
              }
                
        echo "</div>";
                
        echo "<div class='clr'></div>";   
        echo "</div>";
        
    }
    else
        echo "����� �� ������";
}
else
    echo "�������� ��������";

?>
